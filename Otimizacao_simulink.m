function [A1,A2,A3,C1,C2,C3] = runtracklsq
% RUNTRACKLSQ demonstrates using LSQNONLIN with Simulink.

parameter_estimation                       % Load the model
A10=0.1813;
A20=-1.3575;
A30=2.1761;
C10=0.0397;
C20=0.0468;
C30=0.0478;
par0 = [A10,A20,A30,C10,C20,C30]; % Set initial values
Ts=0.01;
M3=evalin('base','M3');
m3_HW=evalin('base','m3_HW');
% b=evalin('base','b');
% J=evalin('base','J');
% c=evalin('base','c');
% k=evalin('base','k');
% K=evalin('base','K');
options = optimoptions(@lsqnonlin,'Algorithm','levenberg-marquardt',...
   'Display','iter-detailed','MaxIterations',500,'UseParallel',0);
% 'StepTolerance',1e-8,'OptimalityTolerance',1e-8, 'ScaleProblem', 'jacobian'
par = lsqnonlin(@tracklsq, par0, [], [], options);
b = par(1);
J = par(2);
c = par(3);
k = par(4);
% K = par(5);
% R = par(6);
% L = par(7);
% figure, compare(iddata(exp1.out,exp1.in,Ts),iddata(evalin('base','Modelo(:,1)'),exp1.in,Ts))
% figure, compare(iddata(exp2.out,exp2.in,Ts),iddata(evalin('base','Modelo(:,2)'),exp2.in,Ts))
% figure, compare(iddata(exp3.out,exp3.in,Ts),iddata(evalin('base','Modelo(:,3)'),exp3.in,Ts))
% figure, compare(iddata(exp4.out,exp4.in,Ts),iddata(evalin('base','Modelo(:,4)'),exp4.in,Ts))
% figure, compare(iddata(exp5.out,exp5.in,Ts),iddata(evalin('base','Modelo(:,5)'),exp5.in,Ts))
figure, compare(iddata(exp6.out,exp6.in,Ts),iddata(evalin('base','Modelo(:,1)'),exp6.in,Ts))
figure, compare(iddata(exp7.out,exp7.in,Ts),iddata(evalin('base','Modelo(:,2)'),exp7.in,Ts))
% figure, compare(iddata(exp8.out,exp8.in,Ts),iddata(evalin('base','Modelo'),exp8.in,Ts))
% assignin('base','R',R);
% assignin('base','L',L);
assignin('base','b',b);
assignin('base','J',J);
assignin('base','c',c);
assignin('base','k',k);
% assignin('base','K',K);

    function F = tracklsq(par)
      % Track the output of optsim to a signal of 1
        
      % Variables a1 and a2 are needed by the model optsim.
      % They are shared with RUNTRACKLSQ so do not need to be
      % redefined here.
%       R = par(1);
%       L = par(2);
      b = par(1);
      J = par(2);
      c = par(3);
      k = par(4);
%       K = par(5);
%       R = par(6);
%       L = par(7);
      % Set sim options and compute function value
      myobj = sim('parameter_estimation','SrcWorkspace','Current', ...
          'StopTime','10','SaveOutput','on','OutputSaveName','yout');
      F1 = myobj.get('yout');
      F=F1{1}.Values.Data;
      assignin('base','Modelo',F1{2}.Values.Data);      
    end
end