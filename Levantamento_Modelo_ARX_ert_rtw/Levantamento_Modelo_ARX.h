/*
 * File: Levantamento_Modelo_ARX.h
 *
 * Code generated for Simulink model 'Levantamento_Modelo_ARX'.
 *
 * Model version                  : 1.3
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Wed Aug 26 14:44:52 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Levantamento_Modelo_ARX_h_
#define RTW_HEADER_Levantamento_Modelo_ARX_h_
#include <math.h>
#include <float.h>
#include <string.h>
#include <stddef.h>
#ifndef Levantamento_Modelo_ARX_COMMON_INCLUDES_
# define Levantamento_Modelo_ARX_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "MW_arduinoI2C.h"
#include "arduino_analogoutput_lct.h"
#endif                                 /* Levantamento_Modelo_ARX_COMMON_INCLUDES_ */

#include "Levantamento_Modelo_ARX_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "MW_target_hardware_resources.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T RateTransition;               /* '<Root>/Rate Transition' */
  real_T Switch;                       /* '<S2>/Switch' */
  real_T Saturation2;                  /* '<Root>/Saturation2' */
  real_T DigitalClock;                 /* '<Root>/Digital Clock' */
} B_Levantamento_Modelo_ARX_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Integrator_DSTATE;            /* '<S13>/Integrator' */
  real_T UD_DSTATE;                    /* '<S14>/UD' */
  real_T RateTransition_Buffer0;       /* '<Root>/Rate Transition' */
  struct {
    void *LoggedData;
  } ToWorkspace2_PWORK;                /* '<Root>/To Workspace2' */

  struct {
    void *LoggedData;
  } ToWorkspace3_PWORK;                /* '<Root>/To Workspace3' */

  struct {
    void *LoggedData;
  } ToWorkspace4_PWORK;                /* '<Root>/To Workspace4' */

  struct {
    void *LoggedData;
  } ToWorkspace5_PWORK;                /* '<Root>/To Workspace5' */

  codertarget_arduinobase_inter_T obj; /* '<Root>/I2C LSB1' */
  uint8_T Output_DSTATE;               /* '<S7>/Output' */
} DW_Levantamento_Modelo_ARX_T;

/* Parameters (auto storage) */
struct P_Levantamento_Modelo_ARX_T_ {
  real_T I;                            /* Variable: I
                                        * Referenced by: '<S13>/Integral Gain'
                                        */
  real_T P;                            /* Variable: P
                                        * Referenced by: '<S13>/Proportional Gain'
                                        */
  real_T DiscretePIDController_D;      /* Mask Parameter: DiscretePIDController_D
                                        * Referenced by: '<S13>/Derivative Gain'
                                        */
  real_T Levantamento_OutValues[20];   /* Mask Parameter: Levantamento_OutValues
                                        * Referenced by: '<S1>/Vector'
                                        */
  real_T CompareToConstant1_const;     /* Mask Parameter: CompareToConstant1_const
                                        * Referenced by: '<S11>/Constant'
                                        */
  real_T CompareToConstant2_const;     /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S12>/Constant'
                                        */
  real_T CompareToConstant_const;      /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S10>/Constant'
                                        */
  uint32_T PWM_pinNumber;              /* Mask Parameter: PWM_pinNumber
                                        * Referenced by: '<S4>/PWM'
                                        */
  uint8_T LimitedCounter_uplimit;      /* Mask Parameter: LimitedCounter_uplimit
                                        * Referenced by: '<S9>/FixPt Switch'
                                        */
  real_T Constant_Value;               /* Expression: 120
                                        * Referenced by: '<S2>/Constant'
                                        */
  real_T RateTransition_InitialCondition;/* Expression: 0
                                          * Referenced by: '<Root>/Rate Transition'
                                          */
  real_T Integrator_gainval;           /* Computed Parameter: Integrator_gainval
                                        * Referenced by: '<S13>/Integrator'
                                        */
  real_T Integrator_IC;                /* Expression: InitialConditionForIntegrator
                                        * Referenced by: '<S13>/Integrator'
                                        */
  real_T TSamp_WtEt;                   /* Computed Parameter: TSamp_WtEt
                                        * Referenced by: '<S14>/TSamp'
                                        */
  real_T UD_InitialCondition;          /* Expression: DifferentiatorICPrevScaledInput
                                        * Referenced by: '<S14>/UD'
                                        */
  real_T Saturation2_UpperSat;         /* Expression: 100
                                        * Referenced by: '<Root>/Saturation2'
                                        */
  real_T Saturation2_LowerSat;         /* Expression: 0
                                        * Referenced by: '<Root>/Saturation2'
                                        */
  int16_T Bias1_Bias;                  /* Computed Parameter: Bias1_Bias
                                        * Referenced by: '<Root>/Bias1'
                                        */
  uint16_T UD_DelayLength;             /* Computed Parameter: UD_DelayLength
                                        * Referenced by: '<S14>/UD'
                                        */
  int16_T Gain3_Gain;                  /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<Root>/Gain3'
                                        */
  uint8_T Constant_Value_n;            /* Computed Parameter: Constant_Value_n
                                        * Referenced by: '<S9>/Constant'
                                        */
  uint8_T Saturation_UpperSat;         /* Computed Parameter: Saturation_UpperSat
                                        * Referenced by: '<S3>/Saturation'
                                        */
  uint8_T Saturation_LowerSat;         /* Computed Parameter: Saturation_LowerSat
                                        * Referenced by: '<S3>/Saturation'
                                        */
  uint8_T FixPtConstant_Value;         /* Computed Parameter: FixPtConstant_Value
                                        * Referenced by: '<S8>/FixPt Constant'
                                        */
  uint8_T Output_InitialCondition;     /* Computed Parameter: Output_InitialCondition
                                        * Referenced by: '<S7>/Output'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_Levantamento_Modelo_A_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    uint16_T clockTick1;
    struct {
      uint16_T TID[2];
    } TaskCounters;

    struct {
      boolean_T TID0_1;
    } RateInteraction;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_Levantamento_Modelo_ARX_T Levantamento_Modelo_ARX_P;

/* Block signals (auto storage) */
extern B_Levantamento_Modelo_ARX_T Levantamento_Modelo_ARX_B;

/* Block states (auto storage) */
extern DW_Levantamento_Modelo_ARX_T Levantamento_Modelo_ARX_DW;

/* External function called from main */
extern void Levantamento_Modelo_ARX_SetEventsForThisBaseStep(boolean_T
  *eventFlags);

/* Model entry point functions */
extern void Levantamento_Modelo_ARX_SetEventsForThisBaseStep(boolean_T
  *eventFlags);
extern void Levantamento_Modelo_ARX_initialize(void);
extern void Levantamento_Modelo_ARX_step0(void);
extern void Levantamento_Modelo_ARX_step1(void);
extern void Levantamento_Modelo_ARX_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Levantamento_Modelo__T *const Levantamento_Modelo_ARX_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S7>/Data Type Propagation' : Unused code path elimination
 * Block '<S8>/FixPt Data Type Duplicate' : Unused code path elimination
 * Block '<S9>/FixPt Data Type Duplicate1' : Unused code path elimination
 * Block '<S2>/Logical Operator1' : Unused code path elimination
 * Block '<S2>/Logical Operator2' : Unused code path elimination
 * Block '<S14>/DTDup' : Unused code path elimination
 * Block '<S1>/Out' : Eliminate redundant signal conversion block
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'Levantamento_Modelo_ARX'
 * '<S1>'   : 'Levantamento_Modelo_ARX/Levantamento'
 * '<S2>'   : 'Levantamento_Modelo_ARX/Limites'
 * '<S3>'   : 'Levantamento_Modelo_ARX/PID'
 * '<S4>'   : 'Levantamento_Modelo_ARX/PWM2'
 * '<S5>'   : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1'
 * '<S6>'   : 'Levantamento_Modelo_ARX/Regressors'
 * '<S7>'   : 'Levantamento_Modelo_ARX/Levantamento/LimitedCounter'
 * '<S8>'   : 'Levantamento_Modelo_ARX/Levantamento/LimitedCounter/Increment Real World'
 * '<S9>'   : 'Levantamento_Modelo_ARX/Levantamento/LimitedCounter/Wrap To Zero'
 * '<S10>'  : 'Levantamento_Modelo_ARX/Limites/Compare To Constant'
 * '<S11>'  : 'Levantamento_Modelo_ARX/Limites/Compare To Constant1'
 * '<S12>'  : 'Levantamento_Modelo_ARX/Limites/Compare To Constant2'
 * '<S13>'  : 'Levantamento_Modelo_ARX/PID/Discrete PID Controller'
 * '<S14>'  : 'Levantamento_Modelo_ARX/PID/Discrete PID Controller/Differentiator'
 * '<S15>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/Check Enable Signal'
 * '<S16>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/Check Initial Covariance'
 * '<S17>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/Check Initial Parameters'
 * '<S18>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/Check Reset Signal'
 * '<S19>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/Check Signals'
 * '<S20>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/Data Type Conversion - AdaptationParameter1'
 * '<S21>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/Data Type Conversion - InitialCovariance'
 * '<S22>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/Data Type Conversion - InitialParameters'
 * '<S23>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/MultiplyWithTranspose'
 * '<S24>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/ProcessInitialCovariance'
 * '<S25>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/ProcessInitialParameters'
 * '<S26>'  : 'Levantamento_Modelo_ARX/Recursive Least Squares Estimator1/RLS'
 */
#endif                                 /* RTW_HEADER_Levantamento_Modelo_ARX_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
