/*
 * File: Levantamento_Modelo_ARX.c
 *
 * Code generated for Simulink model 'Levantamento_Modelo_ARX'.
 *
 * Model version                  : 1.3
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Wed Aug 26 14:44:52 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Levantamento_Modelo_ARX.h"
#include "Levantamento_Modelo_ARX_private.h"
#include "Levantamento_Modelo_ARX_dt.h"
#define Levantamento_M_RegisterAddress_ (12.0)
#define Levantamento_Mode_SlaveAddress_ (54.0)

/* Block signals (auto storage) */
B_Levantamento_Modelo_ARX_T Levantamento_Modelo_ARX_B;

/* Block states (auto storage) */
DW_Levantamento_Modelo_ARX_T Levantamento_Modelo_ARX_DW;

/* Real-time model */
RT_MODEL_Levantamento_Modelo__T Levantamento_Modelo_ARX_M_;
RT_MODEL_Levantamento_Modelo__T *const Levantamento_Modelo_ARX_M =
  &Levantamento_Modelo_ARX_M_;
static void rate_monotonic_scheduler(void);

/*
 * Set which subrates need to run this base step (base rate always runs).
 * This function must be called prior to calling the model step function
 * in order to "remember" which rates need to run this base step.  The
 * buffering of events allows for overlapping preemption.
 */
void Levantamento_Modelo_ARX_SetEventsForThisBaseStep(boolean_T *eventFlags)
{
  /* Task runs when its counter is zero, computed via rtmStepTask macro */
  eventFlags[1] = ((boolean_T)rtmStepTask(Levantamento_Modelo_ARX_M, 1));
}

/*
 *   This function updates active task flag for each subrate
 * and rate transition flags for tasks that exchange data.
 * The function assumes rate-monotonic multitasking scheduler.
 * The function must be called at model base rate so that
 * the generated code self-manages all its subrates and rate
 * transition flags.
 */
static void rate_monotonic_scheduler(void)
{
  /* To ensure a deterministic data transfer between two rates,
   * data is transferred at the priority of a fast task and the frequency
   * of the slow task.  The following flags indicate when the data transfer
   * happens.  That is, a rate interaction flag is set true when both rates
   * will run, and false otherwise.
   */

  /* tid 0 shares data with slower tid rate: 1 */
  Levantamento_Modelo_ARX_M->Timing.RateInteraction.TID0_1 =
    (Levantamento_Modelo_ARX_M->Timing.TaskCounters.TID[1] == 0);

  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (Levantamento_Modelo_ARX_M->Timing.TaskCounters.TID[1])++;
  if ((Levantamento_Modelo_ARX_M->Timing.TaskCounters.TID[1]) > 399) {/* Sample time: [4.0s, 0.0s] */
    Levantamento_Modelo_ARX_M->Timing.TaskCounters.TID[1] = 0;
  }
}

/* Model step function for TID0 */
void Levantamento_Modelo_ARX_step0(void) /* Sample time: [0.01s, 0.0s] */
{
  uint8_T rawData[2];
  real_T rtb_Add;
  real_T rtb_TSamp;
  real_T rtb_Gain3;
  uint16_T q0;
  uint16_T qY;
  real_T u0;
  uint8_T tmp;

  {                                    /* Sample time: [0.01s, 0.0s] */
    rate_monotonic_scheduler();
  }

  /* RateTransition: '<Root>/Rate Transition' */
  if (Levantamento_Modelo_ARX_M->Timing.RateInteraction.TID0_1) {
    Levantamento_Modelo_ARX_B.RateTransition =
      Levantamento_Modelo_ARX_DW.RateTransition_Buffer0;
  }

  /* End of RateTransition: '<Root>/Rate Transition' */

  /* MATLABSystem: '<Root>/I2C LSB1' */
  MW_i2cReadLoop((uint8_T)Levantamento_Mode_SlaveAddress_, 1.0,
                 Levantamento_M_RegisterAddress_, 2U, rawData);
  q0 = (uint16_T)rawData[0] << 8U;
  qY = q0 + /*MW:OvSatOk*/ rawData[1];
  if (qY < q0) {
    qY = MAX_uint16_T;
  }

  /* Gain: '<Root>/Gain3' incorporates:
   *  Bias: '<Root>/Bias1'
   *  DataTypeConversion: '<Root>/Data Type Conversion3'
   *  MATLABSystem: '<Root>/I2C LSB1'
   */
  rtb_Gain3 = (real_T)Levantamento_Modelo_ARX_P.Gain3_Gain * 7.62939453125E-6 *
    (real_T)((int16_T)qY + Levantamento_Modelo_ARX_P.Bias1_Bias);

  /* Sum: '<S3>/Add' */
  rtb_Add = Levantamento_Modelo_ARX_B.RateTransition - rtb_Gain3;

  /* SampleTimeMath: '<S14>/TSamp' incorporates:
   *  Gain: '<S13>/Derivative Gain'
   *
   * About '<S14>/TSamp':
   *  y = u * K where K = 1 / ( w * Ts )
   */
  rtb_TSamp = Levantamento_Modelo_ARX_P.DiscretePIDController_D * rtb_Add *
    Levantamento_Modelo_ARX_P.TSamp_WtEt;

  /* Switch: '<S2>/Switch' incorporates:
   *  Constant: '<S10>/Constant'
   *  Constant: '<S11>/Constant'
   *  Constant: '<S12>/Constant'
   *  Constant: '<S2>/Constant'
   *  Logic: '<S2>/Logical Operator'
   *  Product: '<S2>/Product'
   *  RelationalOperator: '<S10>/Compare'
   *  RelationalOperator: '<S11>/Compare'
   *  RelationalOperator: '<S12>/Compare'
   *  Saturate: '<S3>/Saturation'
   */
  if ((Levantamento_Modelo_ARX_B.RateTransition >=
       Levantamento_Modelo_ARX_P.CompareToConstant1_const) && (rtb_Gain3 >=
       Levantamento_Modelo_ARX_P.CompareToConstant2_const)) {
    Levantamento_Modelo_ARX_B.Switch = Levantamento_Modelo_ARX_P.Constant_Value;
  } else {
    /* Sum: '<S13>/Sum' incorporates:
     *  Delay: '<S14>/UD'
     *  DiscreteIntegrator: '<S13>/Integrator'
     *  Gain: '<S13>/Proportional Gain'
     *  Sum: '<S14>/Diff'
     */
    u0 = (Levantamento_Modelo_ARX_P.P * rtb_Add +
          Levantamento_Modelo_ARX_DW.Integrator_DSTATE) + (rtb_TSamp -
      Levantamento_Modelo_ARX_DW.UD_DSTATE);

    /* Saturate: '<S3>/Saturation' */
    if (u0 > Levantamento_Modelo_ARX_P.Saturation_UpperSat) {
      u0 = Levantamento_Modelo_ARX_P.Saturation_UpperSat;
    } else {
      if (u0 < Levantamento_Modelo_ARX_P.Saturation_LowerSat) {
        u0 = Levantamento_Modelo_ARX_P.Saturation_LowerSat;
      }
    }

    u0 = floor(u0);
    if (rtIsNaN(u0)) {
      u0 = 0.0;
    } else {
      u0 = fmod(u0, 256.0);
    }

    Levantamento_Modelo_ARX_B.Switch = Levantamento_Modelo_ARX_B.RateTransition >=
      Levantamento_Modelo_ARX_P.CompareToConstant_const ? (int16_T)(uint8_T)u0 :
      0;
  }

  /* End of Switch: '<S2>/Switch' */

  /* DataTypeConversion: '<S4>/Data Type Conversion' */
  if (Levantamento_Modelo_ARX_B.Switch < 256.0) {
    if (Levantamento_Modelo_ARX_B.Switch >= 0.0) {
      tmp = (uint8_T)Levantamento_Modelo_ARX_B.Switch;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint8_T;
  }

  /* End of DataTypeConversion: '<S4>/Data Type Conversion' */

  /* S-Function (arduinoanalogoutput_sfcn): '<S4>/PWM' */
  MW_analogWrite(Levantamento_Modelo_ARX_P.PWM_pinNumber, tmp);

  /* Saturate: '<Root>/Saturation2' */
  if (rtb_Gain3 > Levantamento_Modelo_ARX_P.Saturation2_UpperSat) {
    Levantamento_Modelo_ARX_B.Saturation2 =
      Levantamento_Modelo_ARX_P.Saturation2_UpperSat;
  } else if (rtb_Gain3 < Levantamento_Modelo_ARX_P.Saturation2_LowerSat) {
    Levantamento_Modelo_ARX_B.Saturation2 =
      Levantamento_Modelo_ARX_P.Saturation2_LowerSat;
  } else {
    Levantamento_Modelo_ARX_B.Saturation2 = rtb_Gain3;
  }

  /* End of Saturate: '<Root>/Saturation2' */
  /* DigitalClock: '<Root>/Digital Clock' */
  Levantamento_Modelo_ARX_B.DigitalClock =
    Levantamento_Modelo_ARX_M->Timing.taskTime0;

  /* Update for DiscreteIntegrator: '<S13>/Integrator' incorporates:
   *  Gain: '<S13>/Integral Gain'
   */
  Levantamento_Modelo_ARX_DW.Integrator_DSTATE += Levantamento_Modelo_ARX_P.I *
    rtb_Add * Levantamento_Modelo_ARX_P.Integrator_gainval;

  /* Update for Delay: '<S14>/UD' */
  Levantamento_Modelo_ARX_DW.UD_DSTATE = rtb_TSamp;

  /* External mode */
  rtExtModeUploadCheckTrigger(2);
  rtExtModeUpload(0, Levantamento_Modelo_ARX_M->Timing.taskTime0);

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.01s, 0.0s] */
    if ((rtmGetTFinal(Levantamento_Modelo_ARX_M)!=-1) &&
        !((rtmGetTFinal(Levantamento_Modelo_ARX_M)-
           Levantamento_Modelo_ARX_M->Timing.taskTime0) >
          Levantamento_Modelo_ARX_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(Levantamento_Modelo_ARX_M, "Simulation finished");
    }

    if (rtmGetStopRequested(Levantamento_Modelo_ARX_M)) {
      rtmSetErrorStatus(Levantamento_Modelo_ARX_M, "Simulation finished");
    }
  }

  /* Update absolute time */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  Levantamento_Modelo_ARX_M->Timing.taskTime0 =
    (++Levantamento_Modelo_ARX_M->Timing.clockTick0) *
    Levantamento_Modelo_ARX_M->Timing.stepSize0;
}

/* Model step function for TID1 */
void Levantamento_Modelo_ARX_step1(void) /* Sample time: [4.0s, 0.0s] */
{
  real_T rtb_Output;
  uint8_T rtb_FixPtSum1;

  /* Sum: '<S8>/FixPt Sum1' incorporates:
   *  Constant: '<S8>/FixPt Constant'
   *  UnitDelay: '<S7>/Output'
   */
  rtb_FixPtSum1 = (uint8_T)((uint16_T)Levantamento_Modelo_ARX_DW.Output_DSTATE +
    Levantamento_Modelo_ARX_P.FixPtConstant_Value);

  /* MultiPortSwitch: '<S1>/Output' incorporates:
   *  Constant: '<S1>/Vector'
   *  UnitDelay: '<S7>/Output'
   */
  rtb_Output =
    Levantamento_Modelo_ARX_P.Levantamento_OutValues[Levantamento_Modelo_ARX_DW.Output_DSTATE];

  /* Update for RateTransition: '<Root>/Rate Transition' */
  Levantamento_Modelo_ARX_DW.RateTransition_Buffer0 = rtb_Output;

  /* Switch: '<S9>/FixPt Switch' */
  if (rtb_FixPtSum1 > Levantamento_Modelo_ARX_P.LimitedCounter_uplimit) {
    /* Update for UnitDelay: '<S7>/Output' incorporates:
     *  Constant: '<S9>/Constant'
     */
    Levantamento_Modelo_ARX_DW.Output_DSTATE =
      Levantamento_Modelo_ARX_P.Constant_Value_n;
  } else {
    /* Update for UnitDelay: '<S7>/Output' */
    Levantamento_Modelo_ARX_DW.Output_DSTATE = rtb_FixPtSum1;
  }

  /* End of Switch: '<S9>/FixPt Switch' */
  rtExtModeUpload(1, ((Levantamento_Modelo_ARX_M->Timing.clockTick1) * 4.0));

  /* Update absolute time */
  /* The "clockTick1" counts the number of times the code of this task has
   * been executed. The resolution of this integer timer is 4.0, which is the step size
   * of the task. Size of "clockTick1" ensures timer will not overflow during the
   * application lifespan selected.
   */
  Levantamento_Modelo_ARX_M->Timing.clockTick1++;
}

/* Model initialize function */
void Levantamento_Modelo_ARX_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)Levantamento_Modelo_ARX_M, 0,
                sizeof(RT_MODEL_Levantamento_Modelo__T));
  rtmSetTFinal(Levantamento_Modelo_ARX_M, 168.0);
  Levantamento_Modelo_ARX_M->Timing.stepSize0 = 0.01;

  /* External mode info */
  Levantamento_Modelo_ARX_M->Sizes.checksums[0] = (4169007513U);
  Levantamento_Modelo_ARX_M->Sizes.checksums[1] = (3009422530U);
  Levantamento_Modelo_ARX_M->Sizes.checksums[2] = (3391723674U);
  Levantamento_Modelo_ARX_M->Sizes.checksums[3] = (3586332899U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[5];
    Levantamento_Modelo_ARX_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(Levantamento_Modelo_ARX_M->extModeInfo,
      &Levantamento_Modelo_ARX_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(Levantamento_Modelo_ARX_M->extModeInfo,
                        Levantamento_Modelo_ARX_M->Sizes.checksums);
    rteiSetTPtr(Levantamento_Modelo_ARX_M->extModeInfo, rtmGetTPtr
                (Levantamento_Modelo_ARX_M));
  }

  /* block I/O */
  (void) memset(((void *) &Levantamento_Modelo_ARX_B), 0,
                sizeof(B_Levantamento_Modelo_ARX_T));

  /* states (dwork) */
  (void) memset((void *)&Levantamento_Modelo_ARX_DW, 0,
                sizeof(DW_Levantamento_Modelo_ARX_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    Levantamento_Modelo_ARX_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Start for RateTransition: '<Root>/Rate Transition' */
  Levantamento_Modelo_ARX_B.RateTransition =
    Levantamento_Modelo_ARX_P.RateTransition_InitialCondition;

  /* Start for MATLABSystem: '<Root>/I2C LSB1' */
  Levantamento_Modelo_ARX_DW.obj.isInitialized = 0L;
  Levantamento_Modelo_ARX_DW.obj.isInitialized = 1L;

  /* Start for S-Function (arduinoanalogoutput_sfcn): '<S4>/PWM' */
  MW_pinModeOutput(Levantamento_Modelo_ARX_P.PWM_pinNumber);

  /* InitializeConditions for RateTransition: '<Root>/Rate Transition' */
  Levantamento_Modelo_ARX_DW.RateTransition_Buffer0 =
    Levantamento_Modelo_ARX_P.RateTransition_InitialCondition;

  /* InitializeConditions for DiscreteIntegrator: '<S13>/Integrator' */
  Levantamento_Modelo_ARX_DW.Integrator_DSTATE =
    Levantamento_Modelo_ARX_P.Integrator_IC;

  /* InitializeConditions for Delay: '<S14>/UD' */
  Levantamento_Modelo_ARX_DW.UD_DSTATE =
    Levantamento_Modelo_ARX_P.UD_InitialCondition;

  /* InitializeConditions for UnitDelay: '<S7>/Output' */
  Levantamento_Modelo_ARX_DW.Output_DSTATE =
    Levantamento_Modelo_ARX_P.Output_InitialCondition;
}

/* Model terminate function */
void Levantamento_Modelo_ARX_terminate(void)
{
  /* Terminate for MATLABSystem: '<Root>/I2C LSB1' */
  if (Levantamento_Modelo_ARX_DW.obj.isInitialized == 1L) {
    Levantamento_Modelo_ARX_DW.obj.isInitialized = 2L;
  }

  /* End of Terminate for MATLABSystem: '<Root>/I2C LSB1' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
