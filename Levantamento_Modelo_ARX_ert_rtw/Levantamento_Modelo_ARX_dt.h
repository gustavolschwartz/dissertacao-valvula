/*
 * Levantamento_Modelo_ARX_dt.h
 *
 * Code generation for model "Levantamento_Modelo_ARX".
 *
 * Model version              : 1.3
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Wed Aug 26 14:44:52 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(int16_T),
  sizeof(codertarget_arduinobase_inter_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "int16_T",
  "codertarget_arduinobase_inter_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&Levantamento_Modelo_ARX_B.RateTransition), 0, 0, 4 }
  ,

  { (char_T *)(&Levantamento_Modelo_ARX_DW.Integrator_DSTATE), 0, 0, 3 },

  { (char_T *)(&Levantamento_Modelo_ARX_DW.ToWorkspace2_PWORK.LoggedData), 11, 0,
    4 },

  { (char_T *)(&Levantamento_Modelo_ARX_DW.obj), 15, 0, 1 },

  { (char_T *)(&Levantamento_Modelo_ARX_DW.Output_DSTATE), 3, 0, 1 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  5U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&Levantamento_Modelo_ARX_P.I), 0, 0, 26 },

  { (char_T *)(&Levantamento_Modelo_ARX_P.PWM_pinNumber), 7, 0, 1 },

  { (char_T *)(&Levantamento_Modelo_ARX_P.LimitedCounter_uplimit), 3, 0, 1 },

  { (char_T *)(&Levantamento_Modelo_ARX_P.Constant_Value), 0, 0, 8 },

  { (char_T *)(&Levantamento_Modelo_ARX_P.Bias1_Bias), 4, 0, 1 },

  { (char_T *)(&Levantamento_Modelo_ARX_P.UD_DelayLength), 5, 0, 1 },

  { (char_T *)(&Levantamento_Modelo_ARX_P.Gain3_Gain), 14, 0, 1 },

  { (char_T *)(&Levantamento_Modelo_ARX_P.Constant_Value_n), 3, 0, 5 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  8U,
  rtPTransitions
};

/* [EOF] Levantamento_Modelo_ARX_dt.h */
