/*
 * File: Levantamento_Modelo_ARX_data.c
 *
 * Code generated for Simulink model 'Levantamento_Modelo_ARX'.
 *
 * Model version                  : 1.3
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Wed Aug 26 14:44:52 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Levantamento_Modelo_ARX.h"
#include "Levantamento_Modelo_ARX_private.h"

/* Block parameters (auto storage) */
P_Levantamento_Modelo_ARX_T Levantamento_Modelo_ARX_P = {
  /* Variable: I
   * Referenced by: '<S13>/Integral Gain'
   */
  7.9987995196070329,

  /* Variable: P
   * Referenced by: '<S13>/Proportional Gain'
   */
  5.2526640061651442,

  /* Mask Parameter: DiscretePIDController_D
   * Referenced by: '<S13>/Derivative Gain'
   */
  0.0,

  /* Mask Parameter: Levantamento_OutValues
   * Referenced by: '<S1>/Vector'
   */
  { 5.0, 10.0, 25.0, 40.0, 50.0, 75.0, 90.0, 95.0, 100.0, 95.0, 90.0, 75.0, 50.0,
    40.0, 25.0, 10.0, 5.0, 95.0, 5.0, 20.0 },

  /* Mask Parameter: CompareToConstant1_const
   * Referenced by: '<S11>/Constant'
   */
  98.0,

  /* Mask Parameter: CompareToConstant2_const
   * Referenced by: '<S12>/Constant'
   */
  98.0,

  /* Mask Parameter: CompareToConstant_const
   * Referenced by: '<S10>/Constant'
   */
  1.0,

  /* Mask Parameter: PWM_pinNumber
   * Referenced by: '<S4>/PWM'
   */
  3U,

  /* Mask Parameter: LimitedCounter_uplimit
   * Referenced by: '<S9>/FixPt Switch'
   */
  19U,

  /* Expression: 120
   * Referenced by: '<S2>/Constant'
   */
  120.0,

  /* Expression: 0
   * Referenced by: '<Root>/Rate Transition'
   */
  0.0,

  /* Computed Parameter: Integrator_gainval
   * Referenced by: '<S13>/Integrator'
   */
  0.01,

  /* Expression: InitialConditionForIntegrator
   * Referenced by: '<S13>/Integrator'
   */
  0.0,

  /* Computed Parameter: TSamp_WtEt
   * Referenced by: '<S14>/TSamp'
   */
  100.0,

  /* Expression: DifferentiatorICPrevScaledInput
   * Referenced by: '<S14>/UD'
   */
  0.0,

  /* Expression: 100
   * Referenced by: '<Root>/Saturation2'
   */
  100.0,

  /* Expression: 0
   * Referenced by: '<Root>/Saturation2'
   */
  0.0,

  /* Computed Parameter: Bias1_Bias
   * Referenced by: '<Root>/Bias1'
   */
  -1610,

  /* Computed Parameter: UD_DelayLength
   * Referenced by: '<S14>/UD'
   */
  1U,

  /* Computed Parameter: Gain3_Gain
   * Referenced by: '<Root>/Gain3'
   */
  16487,

  /* Computed Parameter: Constant_Value_n
   * Referenced by: '<S9>/Constant'
   */
  0U,

  /* Computed Parameter: Saturation_UpperSat
   * Referenced by: '<S3>/Saturation'
   */
  255U,

  /* Computed Parameter: Saturation_LowerSat
   * Referenced by: '<S3>/Saturation'
   */
  0U,

  /* Computed Parameter: FixPtConstant_Value
   * Referenced by: '<S8>/FixPt Constant'
   */
  1U,

  /* Computed Parameter: Output_InitialCondition
   * Referenced by: '<S7>/Output'
   */
  0U
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
