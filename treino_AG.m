function [P,I,D] = treino_AG

Ts=0.01;

A1=[eye(3); -eye(3)];
A2=[10;10;1;0;0;0];
options=optimoptions(@ga,'Display','iter','MaxGenerations',20);
par = ga(@tracklsq, 3, A1, A2, [], [], [], [], [], options);
P = par(1);
I = par(2);
D = par(3);

% figure, compare(iddata(M8.VP,M8.VM,Ts),iddata(evalin('base','Modelo(:,1)'),M8.VM,Ts))
% assignin('base','P8',P);
% assignin('base','I8',I);
% assignin('base','D8',D);
assignin('base','P',P);
assignin('base','I',I);
assignin('base','D',D);
assignin('base','ts',ts);
assignin('base','mp',mp);
    function F = tracklsq(par)
      % Track the output of optsim to a signal of 1
        
        P = par(1);
        I = par(2);
        D = par(3);
        
        [P I D]

      % Set sim options and compute function value
      myobj = sim('PID_Tuning_ARXModel','SrcWorkspace','Current', ...
          'StopTime','70','SaveOutput','on','OutputSaveName','yout');
      F1 = myobj.get('yout');
      %F=F1{1}.Values.Data;
      VP=F1{2}.Values.Data; %saida
      SP=F1{3}.Values.Data; %sp
      %assignin('base','Modelo',F1{2}.Values.Data); 
      % Custo
      k=size(VP);
      cont=1000;
      cont_0=501;
      ts=ones(round(k(1,1)/500)-1,1)*0;
      ts=[5 5 5 5 5 5 0 0 0 0 0 0 0];
      mp=zeros(round(k(1,1)/500)-1,1);
      for h=3:1:round(k(1,1)/500)/1
          
        while (cont>cont_0)
            if abs(SP(cont)-VP(cont))<0.02*abs(SP(cont)-SP(cont_0-1))
               ts(h-2)=(-500*(h-1)+cont)*Ts; 
            end
            if (SP(cont)> SP(cont_0-1)) && ([SP(cont)-VP(cont)]>mp(h-1))
                mp(h-2)=[SP(cont)-VP(cont)]/abs(SP(cont)-SP(cont_0-1));
            end
            if (SP(cont)< SP(cont_0-1)) && ([SP(cont)-VP(cont)]<mp(h-1))
                mp(h-2)=[SP(cont)-VP(cont)]/abs(SP(cont)-SP(cont_0-1));
            end
          cont=cont-1;
        end
        
        cont_0=h*500+1;
        cont=(h+1)*500;
      end
      TS=0.2;
      MP=0.05;
%       F= sum((ts/TS).^3)/h + sum((abs(mp)/MP).^3)/h;  P:5.2583        I:5.654      D:0.36247
      F= sum((ts/TS).^3)/h; %                           P:5.2527        I:7.9988     D:0.1686
      if (P<0)|| (I<0)
          F=abs(F)+10^20;
      end
      
    end
end