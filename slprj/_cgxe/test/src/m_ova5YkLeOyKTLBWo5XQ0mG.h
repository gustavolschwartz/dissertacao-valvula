#ifndef __ova5YkLeOyKTLBWo5XQ0mG_h__
#define __ova5YkLeOyKTLBWo5XQ0mG_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"
#include "slexec_vm_zc_functions.h"

/* Type Definitions */
#ifndef struct_tag_sMXuVOqiR8VrXITXlaWuyyE
#define struct_tag_sMXuVOqiR8VrXITXlaWuyyE

struct tag_sMXuVOqiR8VrXITXlaWuyyE
{
  real_T uint8;
  real_T uint16;
};

#endif                                 /*struct_tag_sMXuVOqiR8VrXITXlaWuyyE*/

#ifndef typedef_sMXuVOqiR8VrXITXlaWuyyE
#define typedef_sMXuVOqiR8VrXITXlaWuyyE

typedef struct tag_sMXuVOqiR8VrXITXlaWuyyE sMXuVOqiR8VrXITXlaWuyyE;

#endif                                 /*typedef_sMXuVOqiR8VrXITXlaWuyyE*/

#ifndef typedef_codertarget_arduinobase_internal_arduinoI2CRead
#define typedef_codertarget_arduinobase_internal_arduinoI2CRead

typedef struct {
  int32_T isInitialized;
} codertarget_arduinobase_internal_arduinoI2CRead;

#endif                                 /*typedef_codertarget_arduinobase_internal_arduinoI2CRead*/

#ifndef typedef_InstanceStruct_ova5YkLeOyKTLBWo5XQ0mG
#define typedef_InstanceStruct_ova5YkLeOyKTLBWo5XQ0mG

typedef struct {
  SimStruct *S;
  codertarget_arduinobase_internal_arduinoI2CRead sysobj;
  boolean_T sysobj_not_empty;
  void *emlrtRootTLSGlobal;
  uint16_T *b_y0;
} InstanceStruct_ova5YkLeOyKTLBWo5XQ0mG;

#endif                                 /*typedef_InstanceStruct_ova5YkLeOyKTLBWo5XQ0mG*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_ova5YkLeOyKTLBWo5XQ0mG(SimStruct *S, int_T method,
  void* data);

#endif
