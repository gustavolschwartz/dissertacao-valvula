/* Include files */

#include "test_cgxe.h"
#include "m_ova5YkLeOyKTLBWo5XQ0mG.h"

unsigned int cgxe_test_method_dispatcher(SimStruct* S, int_T method, void* data)
{
  if (ssGetChecksum0(S) == 1328577961 &&
      ssGetChecksum1(S) == 3589512255 &&
      ssGetChecksum2(S) == 249559452 &&
      ssGetChecksum3(S) == 3910467545) {
    method_dispatcher_ova5YkLeOyKTLBWo5XQ0mG(S, method, data);
    return 1;
  }

  return 0;
}
