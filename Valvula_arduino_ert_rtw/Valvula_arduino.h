/*
 * Valvula_arduino.h
 *
 * Code generation for model "Valvula_arduino".
 *
 * Model version              : 1.21
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Mon Sep 14 14:46:08 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Valvula_arduino_h_
#define RTW_HEADER_Valvula_arduino_h_
#include <math.h>
#include <string.h>
#include <stddef.h>
#ifndef Valvula_arduino_COMMON_INCLUDES_
# define Valvula_arduino_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "zero_crossing_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "MW_arduinoI2C.h"
#include "MW_SerialRead.h"
#include "MW_SerialWrite.h"
#include "arduino_analogoutput_lct.h"
#include "MW_digitalio.h"
#endif                                 /* Valvula_arduino_COMMON_INCLUDES_ */

#include "Valvula_arduino_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "MW_target_hardware_resources.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals (auto storage) */
typedef struct {
  int128m_T r0;
  int128m_T r1;
  char_T cv0[9];
  real_T Switch;                       /* '<S13>/Switch' */
  real_T sampleTime;
  int64m_T Sum;                        /* '<S12>/Sum' */
  int64m_T DeadZone;                   /* '<S14>/DeadZone' */
  int64m_T rtb_Sum_m;
  int64m_T rtb_DeadZone_c;
  int64m_T r2;
  uint8_T data[8];
  uint32_T In1;                        /* '<S37>/In1' */
  uint32_T In1_b;                      /* '<S36>/In1' */
  boolean_T In1_d;                     /* '<S38>/In1' */
  boolean_T LogicalOperator2;          /* '<S27>/Logical Operator2' */
} B_Valvula_arduino_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  codertarget_arduinobase_in_ij_T obj; /* '<S7>/Serial Transmit' */
  codertarget_arduinobase_block_T obj_m;/* '<S35>/Digital Input' */
  real_T Delay_DSTATE;                 /* '<S10>/Delay' */
  real_T Delay_DSTATE_l;               /* '<S27>/Delay' */
  codertarget_arduinobase_int_i_T obj_k;/* '<S1>/Serial Receive' */
  uint32_T UnitDelay1_DSTATE;          /* '<S6>/Unit Delay1' */
  uint32_T UnitDelay_DSTATE;           /* '<S6>/Unit Delay' */
  int32_T Integrator_DSTATE;           /* '<S12>/Integrator' */
  uint32_T Counter_Count;              /* '<S6>/Counter' */
  uint32_T Counter1_Count;             /* '<S6>/Counter1' */
  uint32_T AlgorithmSubsystem_PREV_T[2];/* '<S1>/Algorithm Subsystem' */
  codertarget_arduinobase_inter_T obj_n;/* '<S1>/Sensor_Pos' */
  uint8_T Output_DSTATE;               /* '<S3>/Output' */
  uint8_T Delay1_DSTATE;               /* '<S10>/Delay1' */
  uint8_T Delay2_DSTATE;               /* '<S10>/Delay2' */
  boolean_T Delay1_DSTATE_d;           /* '<S27>/Delay1' */
  boolean_T Delay2_DSTATE_l;           /* '<S27>/Delay2' */
  uint8_T Counter_Count_n;             /* '<S10>/Counter' */
  uint8_T Counter_Count_nv;            /* '<S27>/Counter' */
} DW_Valvula_arduino_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState EnabledSubsystem2_Trig_ZCE;/* '<S6>/Enabled Subsystem2' */
  ZCSigState EnabledSubsystem1_Trig_ZCE;/* '<S6>/Enabled Subsystem1' */
  ZCSigState EnabledSubsystem_Trig_ZCE;/* '<S6>/Enabled Subsystem' */
  ZCSigState Algorithm_Trig_ZCE;       /* '<Root>/Algorithm' */
} PrevZCX_Valvula_arduino_T;

/* Parameters (auto storage) */
struct P_Valvula_arduino_T_ {
  int32_T P;                           /* Variable: P
                                        * Referenced by: '<S12>/Proportional Gain'
                                        */
  int16_T B;                           /* Variable: B
                                        * Referenced by: '<S12>/Setpoint Weighting (Proportional)'
                                        */
  int16_T I;                           /* Variable: I
                                        * Referenced by: '<S12>/Integral Gain'
                                        */
  real_T CompareToConstant1_const;     /* Mask Parameter: CompareToConstant1_const
                                        * Referenced by: '<S30>/Constant'
                                        */
  real_T CompareToConstant1_const_b;   /* Mask Parameter: CompareToConstant1_const_b
                                        * Referenced by: '<S31>/Constant'
                                        */
  int64m_T DiscreteVarying2DOFPID2_LowerSa;/* Mask Parameter: DiscreteVarying2DOFPID2_LowerSa
                                            * Referenced by:
                                            *   '<S12>/Saturate'
                                            *   '<S14>/DeadZone'
                                            */
  int64m_T DiscreteVarying2DOFPID2_UpperSa;/* Mask Parameter: DiscreteVarying2DOFPID2_UpperSa
                                            * Referenced by:
                                            *   '<S12>/Saturate'
                                            *   '<S14>/DeadZone'
                                            */
  uint32_T Counter_InitialCount;       /* Mask Parameter: Counter_InitialCount
                                        * Referenced by: '<S6>/Counter'
                                        */
  uint32_T Counter1_InitialCount;      /* Mask Parameter: Counter1_InitialCount
                                        * Referenced by: '<S6>/Counter1'
                                        */
  uint32_T CompareToConstant_const;    /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S34>/Constant'
                                        */
  uint32_T PWM_pinNumber;              /* Mask Parameter: PWM_pinNumber
                                        * Referenced by: '<S9>/PWM'
                                        */
  int16_T CompareToConstant_const_c;   /* Mask Parameter: CompareToConstant_const_c
                                        * Referenced by: '<S15>/Constant'
                                        */
  int16_T CompareToConstant1_const_f;  /* Mask Parameter: CompareToConstant1_const_f
                                        * Referenced by: '<S16>/Constant'
                                        */
  int16_T CompareToConstant2_const;    /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S17>/Constant'
                                        */
  uint8_T Counter_InitialCount_l;      /* Mask Parameter: Counter_InitialCount_l
                                        * Referenced by: '<S27>/Counter'
                                        */
  uint8_T Counter_InitialCount_m;      /* Mask Parameter: Counter_InitialCount_m
                                        * Referenced by: '<S10>/Counter'
                                        */
  uint8_T CompareToConstant_const_j;   /* Mask Parameter: CompareToConstant_const_j
                                        * Referenced by: '<S19>/Constant'
                                        */
  uint8_T CompareToConstant1_const_e;  /* Mask Parameter: CompareToConstant1_const_e
                                        * Referenced by: '<S20>/Constant'
                                        */
  uint8_T CompareToConstant2_const_m;  /* Mask Parameter: CompareToConstant2_const_m
                                        * Referenced by: '<S21>/Constant'
                                        */
  uint8_T CompareToConstant3_const;    /* Mask Parameter: CompareToConstant3_const
                                        * Referenced by: '<S22>/Constant'
                                        */
  uint8_T CompareToConstant4_const;    /* Mask Parameter: CompareToConstant4_const
                                        * Referenced by: '<S23>/Constant'
                                        */
  uint8_T CompareToConstant5_const;    /* Mask Parameter: CompareToConstant5_const
                                        * Referenced by: '<S24>/Constant'
                                        */
  uint8_T CompareToConstant6_const;    /* Mask Parameter: CompareToConstant6_const
                                        * Referenced by: '<S25>/Constant'
                                        */
  uint8_T CompareToConstant7_const;    /* Mask Parameter: CompareToConstant7_const
                                        * Referenced by: '<S26>/Constant'
                                        */
  uint8_T CompareToConstant_const_p;   /* Mask Parameter: CompareToConstant_const_p
                                        * Referenced by: '<S2>/Constant'
                                        */
  uint8_T CounterLimited_uplimit;      /* Mask Parameter: CounterLimited_uplimit
                                        * Referenced by: '<S33>/FixPt Switch'
                                        */
  real_T Constant_Value;               /* Expression: 120
                                        * Referenced by: '<S13>/Constant'
                                        */
  real_T Gain2_Gain;                   /* Expression: 100
                                        * Referenced by: '<S7>/Gain2'
                                        */
  real_T Delay_InitialCondition;       /* Expression: 0.0
                                        * Referenced by: '<S27>/Delay'
                                        */
  real_T Constant_Value_i;             /* Expression: 1
                                        * Referenced by: '<S27>/Constant'
                                        */
  real_T Delay_InitialCondition_c;     /* Expression: 0.0
                                        * Referenced by: '<S10>/Delay'
                                        */
  real_T Constant_Value_l;             /* Expression: 1
                                        * Referenced by: '<S10>/Constant'
                                        */
  real_T DigitalInput_SampleTime;      /* Expression: 1/100000
                                        * Referenced by: '<S35>/Digital Input'
                                        */
  int64m_T ZeroGain_Gain;              /* Computed Parameter: ZeroGain_Gain
                                        * Referenced by: '<S14>/ZeroGain'
                                        */
  uint32_T Out1_Y0;                    /* Computed Parameter: Out1_Y0
                                        * Referenced by: '<S36>/Out1'
                                        */
  uint32_T Out1_Y0_a;                  /* Computed Parameter: Out1_Y0_a
                                        * Referenced by: '<S37>/Out1'
                                        */
  uint32_T UnitDelay1_InitialCondition;/* Computed Parameter: UnitDelay1_InitialCondition
                                        * Referenced by: '<S6>/Unit Delay1'
                                        */
  uint32_T UnitDelay_InitialCondition; /* Computed Parameter: UnitDelay_InitialCondition
                                        * Referenced by: '<S6>/Unit Delay'
                                        */
  int32_T Constant_Value_j;            /* Computed Parameter: Constant_Value_j
                                        * Referenced by: '<S12>/Constant'
                                        */
  int32_T Integrator_IC;               /* Computed Parameter: Integrator_IC
                                        * Referenced by: '<S12>/Integrator'
                                        */
  int16_T Saturation3_UpperSat;        /* Computed Parameter: Saturation3_UpperSat
                                        * Referenced by: '<S7>/Saturation3'
                                        */
  int16_T Saturation3_LowerSat;        /* Computed Parameter: Saturation3_LowerSat
                                        * Referenced by: '<S7>/Saturation3'
                                        */
  int16_T Bias1_Bias;                  /* Computed Parameter: Bias1_Bias
                                        * Referenced by: '<S1>/Bias1'
                                        */
  uint16_T Delay_DelayLength;          /* Computed Parameter: Delay_DelayLength
                                        * Referenced by: '<S27>/Delay'
                                        */
  uint16_T Delay1_DelayLength;         /* Computed Parameter: Delay1_DelayLength
                                        * Referenced by: '<S27>/Delay1'
                                        */
  uint16_T Delay2_DelayLength;         /* Computed Parameter: Delay2_DelayLength
                                        * Referenced by: '<S27>/Delay2'
                                        */
  uint16_T Delay1_DelayLength_b;       /* Computed Parameter: Delay1_DelayLength_b
                                        * Referenced by: '<S10>/Delay1'
                                        */
  uint16_T Delay2_DelayLength_m;       /* Computed Parameter: Delay2_DelayLength_m
                                        * Referenced by: '<S10>/Delay2'
                                        */
  uint16_T Delay_DelayLength_g;        /* Computed Parameter: Delay_DelayLength_g
                                        * Referenced by: '<S10>/Delay'
                                        */
  int16_T Gain3_Gain;                  /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S1>/Gain3'
                                        */
  uint8_T Delay1_InitialCondition;     /* Computed Parameter: Delay1_InitialCondition
                                        * Referenced by: '<S10>/Delay1'
                                        */
  uint8_T Delay2_InitialCondition;     /* Computed Parameter: Delay2_InitialCondition
                                        * Referenced by: '<S10>/Delay2'
                                        */
  uint8_T Constant_Value_ib;           /* Computed Parameter: Constant_Value_ib
                                        * Referenced by: '<S33>/Constant'
                                        */
  uint8_T Output_InitialCondition;     /* Computed Parameter: Output_InitialCondition
                                        * Referenced by: '<S3>/Output'
                                        */
  uint8_T FixPtConstant_Value;         /* Computed Parameter: FixPtConstant_Value
                                        * Referenced by: '<S32>/FixPt Constant'
                                        */
  boolean_T Delay1_InitialCondition_m; /* Computed Parameter: Delay1_InitialCondition_m
                                        * Referenced by: '<S27>/Delay1'
                                        */
  boolean_T Delay2_InitialCondition_j; /* Computed Parameter: Delay2_InitialCondition_j
                                        * Referenced by: '<S27>/Delay2'
                                        */
  boolean_T Out1_Y0_p;                 /* Computed Parameter: Out1_Y0_p
                                        * Referenced by: '<S38>/Out1'
                                        */
  uint8_T Gain1_Gain;                  /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S7>/Gain1'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_Valvula_arduino_T {
  const char_T *errorStatus;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick1;
    uint32_T clockTickH1;
    struct {
      uint16_T TID[2];
    } TaskCounters;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_Valvula_arduino_T Valvula_arduino_P;

/* Block signals (auto storage) */
extern B_Valvula_arduino_T Valvula_arduino_B;

/* Block states (auto storage) */
extern DW_Valvula_arduino_T Valvula_arduino_DW;

/* Model entry point functions */
extern void Valvula_arduino_initialize(void);
extern void Valvula_arduino_step(void);
extern void Valvula_arduino_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Valvula_arduino_T *const Valvula_arduino_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S13>/Logical Operator1' : Unused code path elimination
 * Block '<S13>/Logical Operator2' : Unused code path elimination
 * Block '<S3>/Data Type Propagation' : Unused code path elimination
 * Block '<S32>/FixPt Data Type Duplicate' : Unused code path elimination
 * Block '<S33>/FixPt Data Type Duplicate1' : Unused code path elimination
 * Block '<S9>/Data Type Conversion' : Eliminate redundant data type conversion
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'Valvula_arduino'
 * '<S1>'   : 'Valvula_arduino/Algorithm'
 * '<S2>'   : 'Valvula_arduino/Compare To Constant'
 * '<S3>'   : 'Valvula_arduino/Counter Limited'
 * '<S4>'   : 'Valvula_arduino/PWM1'
 * '<S5>'   : 'Valvula_arduino/Repeating Sequence Stair'
 * '<S6>'   : 'Valvula_arduino/SP_PWM__Counter'
 * '<S7>'   : 'Valvula_arduino/Algorithm/Algorithm Subsystem'
 * '<S8>'   : 'Valvula_arduino/Algorithm/Diagnosis'
 * '<S9>'   : 'Valvula_arduino/Algorithm/PWM'
 * '<S10>'  : 'Valvula_arduino/Algorithm/TOFF'
 * '<S11>'  : 'Valvula_arduino/Algorithm/Algorithm Subsystem/Convers�o'
 * '<S12>'  : 'Valvula_arduino/Algorithm/Algorithm Subsystem/Discrete Varying 2DOF PID2'
 * '<S13>'  : 'Valvula_arduino/Algorithm/Algorithm Subsystem/Limites'
 * '<S14>'  : 'Valvula_arduino/Algorithm/Algorithm Subsystem/Discrete Varying 2DOF PID2/Clamping circuit'
 * '<S15>'  : 'Valvula_arduino/Algorithm/Algorithm Subsystem/Limites/Compare To Constant'
 * '<S16>'  : 'Valvula_arduino/Algorithm/Algorithm Subsystem/Limites/Compare To Constant1'
 * '<S17>'  : 'Valvula_arduino/Algorithm/Algorithm Subsystem/Limites/Compare To Constant2'
 * '<S18>'  : 'Valvula_arduino/Algorithm/Diagnosis/Calibration'
 * '<S19>'  : 'Valvula_arduino/Algorithm/Diagnosis/Compare To Constant'
 * '<S20>'  : 'Valvula_arduino/Algorithm/Diagnosis/Compare To Constant1'
 * '<S21>'  : 'Valvula_arduino/Algorithm/Diagnosis/Compare To Constant2'
 * '<S22>'  : 'Valvula_arduino/Algorithm/Diagnosis/Compare To Constant3'
 * '<S23>'  : 'Valvula_arduino/Algorithm/Diagnosis/Compare To Constant4'
 * '<S24>'  : 'Valvula_arduino/Algorithm/Diagnosis/Compare To Constant5'
 * '<S25>'  : 'Valvula_arduino/Algorithm/Diagnosis/Compare To Constant6'
 * '<S26>'  : 'Valvula_arduino/Algorithm/Diagnosis/Compare To Constant7'
 * '<S27>'  : 'Valvula_arduino/Algorithm/Diagnosis/TOFF'
 * '<S28>'  : 'Valvula_arduino/Algorithm/Diagnosis/Calibration/Triggered Subsystem'
 * '<S29>'  : 'Valvula_arduino/Algorithm/Diagnosis/Calibration/Triggered Subsystem1'
 * '<S30>'  : 'Valvula_arduino/Algorithm/Diagnosis/TOFF/Compare To Constant1'
 * '<S31>'  : 'Valvula_arduino/Algorithm/TOFF/Compare To Constant1'
 * '<S32>'  : 'Valvula_arduino/Counter Limited/Increment Real World'
 * '<S33>'  : 'Valvula_arduino/Counter Limited/Wrap To Zero'
 * '<S34>'  : 'Valvula_arduino/SP_PWM__Counter/Compare To Constant'
 * '<S35>'  : 'Valvula_arduino/SP_PWM__Counter/Digital Input'
 * '<S36>'  : 'Valvula_arduino/SP_PWM__Counter/Enabled Subsystem'
 * '<S37>'  : 'Valvula_arduino/SP_PWM__Counter/Enabled Subsystem1'
 * '<S38>'  : 'Valvula_arduino/SP_PWM__Counter/Enabled Subsystem2'
 * '<S39>'  : 'Valvula_arduino/SP_PWM__Counter/On Delay1'
 * '<S40>'  : 'Valvula_arduino/SP_PWM__Counter/On Delay1/Model'
 * '<S41>'  : 'Valvula_arduino/SP_PWM__Counter/On Delay1/Model/OFF Delay'
 * '<S42>'  : 'Valvula_arduino/SP_PWM__Counter/On Delay1/Model/ON Delay'
 */
#endif                                 /* RTW_HEADER_Valvula_arduino_h_ */
