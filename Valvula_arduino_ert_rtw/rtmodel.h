/*
 * rtmodel.h
 *
 * Code generation for Simulink model "Valvula_arduino".
 *
 * Simulink Coder version                : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Mon Sep 14 14:46:08 2020
 *
 * Note that the generated code is not dependent on this header file.
 * The file is used in cojuction with the automatic build procedure.
 * It is included by the sample main executable harness
 * MATLAB/rtw/c/src/common/rt_main.c.
 *
 */

#ifndef RTW_HEADER_rtmodel_h_
#define RTW_HEADER_rtmodel_h_
#include "Valvula_arduino.h"

/* Macros generated for backwards compatibility  */
#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((void*) 0)
#endif
#endif                                 /* RTW_HEADER_rtmodel_h_ */
