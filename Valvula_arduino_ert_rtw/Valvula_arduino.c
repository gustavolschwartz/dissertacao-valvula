/*
 * Valvula_arduino.c
 *
 * Code generation for model "Valvula_arduino".
 *
 * Model version              : 1.21
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Mon Sep 14 14:46:08 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "Valvula_arduino.h"
#include "Valvula_arduino_private.h"
#define Valvula_arduin_RegisterAddress_ (12.0)
#define Valvula_arduino_PinNumber      (40.0)
#define Valvula_arduino_SlaveAddress_  (54.0)
#define Valvula_arduino_floatprecision (1.0)

/* Block signals (auto storage) */
B_Valvula_arduino_T Valvula_arduino_B;

/* Block states (auto storage) */
DW_Valvula_arduino_T Valvula_arduino_DW;

/* Previous zero-crossings (trigger) states */
PrevZCX_Valvula_arduino_T Valvula_arduino_PrevZCX;

/* Real-time model */
RT_MODEL_Valvula_arduino_T Valvula_arduino_M_;
RT_MODEL_Valvula_arduino_T *const Valvula_arduino_M = &Valvula_arduino_M_;
static void rate_scheduler(void);
void MultiWordAdd(const uint32_T u1[], const uint32_T u2[], uint32_T y[],
                  int16_T n)
{
  uint32_T yi;
  uint32_T u1i;
  uint32_T carry = 0UL;
  int16_T i;
  for (i = 0; i < n; i++) {
    u1i = u1[i];
    yi = (u1i + u2[i]) + carry;
    y[i] = yi;
    carry = carry != 0UL ? (uint32_T)(yi <= u1i) : (uint32_T)(yi < u1i);
  }
}

void sMultiWordMul(const uint32_T u1[], int16_T n1, const uint32_T u2[], int16_T
                   n2, uint32_T y[], int16_T n)
{
  int16_T i;
  int16_T j;
  int16_T k;
  int16_T nj;
  uint32_T u1i;
  uint32_T yk;
  uint32_T a1;
  uint32_T a0;
  uint32_T b1;
  uint32_T w10;
  uint32_T w01;
  uint32_T cb;
  boolean_T isNegative1;
  boolean_T isNegative2;
  uint32_T cb1;
  uint32_T cb2;
  isNegative1 = ((u1[n1 - 1] & 2147483648UL) != 0UL);
  isNegative2 = ((u2[n2 - 1] & 2147483648UL) != 0UL);
  cb1 = 1UL;

  /* Initialize output to zero */
  for (k = 0; k < n; k++) {
    y[k] = 0UL;
  }

  for (i = 0; i < n1; i++) {
    cb = 0UL;
    u1i = u1[i];
    if (isNegative1) {
      u1i = ~u1i + cb1;
      cb1 = (uint32_T)(u1i < cb1);
    }

    a1 = u1i >> 16U;
    a0 = u1i & 65535UL;
    cb2 = 1UL;
    k = n - i;
    nj = n2 <= k ? n2 : k;
    k = i;
    for (j = 0; j < nj; j++) {
      yk = y[k];
      u1i = u2[j];
      if (isNegative2) {
        u1i = ~u1i + cb2;
        cb2 = (uint32_T)(u1i < cb2);
      }

      b1 = u1i >> 16U;
      u1i &= 65535UL;
      w10 = a1 * u1i;
      w01 = a0 * b1;
      yk += cb;
      cb = (uint32_T)(yk < cb);
      u1i *= a0;
      yk += u1i;
      cb += (yk < u1i);
      u1i = w10 << 16U;
      yk += u1i;
      cb += (yk < u1i);
      u1i = w01 << 16U;
      yk += u1i;
      cb += (yk < u1i);
      y[k] = yk;
      cb += w10 >> 16U;
      cb += w01 >> 16U;
      cb += a1 * b1;
      k++;
    }

    if (k < n) {
      y[k] = cb;
    }
  }

  /* Apply sign */
  if (isNegative1 != isNegative2) {
    cb = 1UL;
    for (k = 0; k < n; k++) {
      yk = ~y[k] + cb;
      y[k] = yk;
      cb = (uint32_T)(yk < cb);
    }
  }
}

void sMultiWordShr(const uint32_T u1[], int16_T n1, uint16_T n2, uint32_T y[],
                   int16_T n)
{
  int16_T nb;
  int16_T i;
  uint32_T ys;
  uint32_T yi;
  uint32_T u1i;
  int16_T nc;
  uint16_T nr;
  uint16_T nl;
  int16_T i1;
  nb = (int16_T)(n2 >> 5);
  i = 0;
  ys = (u1[n1 - 1] & 2147483648UL) != 0UL ? MAX_uint32_T : 0UL;
  if (nb < n1) {
    nc = n + nb;
    if (nc > n1) {
      nc = n1;
    }

    nr = n2 - ((uint16_T)nb << 5);
    if (nr > 0U) {
      nl = 32U - nr;
      u1i = u1[nb];
      for (i1 = nb + 1; i1 < nc; i1++) {
        yi = u1i >> nr;
        u1i = u1[i1];
        y[i] = u1i << nl | yi;
        i++;
      }

      yi = u1i >> nr;
      u1i = nc < n1 ? u1[nc] : ys;
      y[i] = u1i << nl | yi;
      i++;
    } else {
      for (i1 = nb; i1 < nc; i1++) {
        y[i] = u1[i1];
        i++;
      }
    }
  }

  while (i < n) {
    y[i] = ys;
    i++;
  }
}

boolean_T sMultiWordGt(const uint32_T u1[], const uint32_T u2[], int16_T n)
{
  return sMultiWordCmp(u1, u2, n) > 0;
}

int16_T sMultiWordCmp(const uint32_T u1[], const uint32_T u2[], int16_T n)
{
  int16_T y;
  uint32_T su1;
  uint32_T su2;
  int16_T i;
  su1 = u1[n - 1] & 2147483648UL;
  su2 = u2[n - 1] & 2147483648UL;
  if ((su1 ^ su2) != 0UL) {
    y = su1 != 0UL ? -1 : 1;
  } else {
    y = 0;
    i = n;
    while ((y == 0) && (i > 0)) {
      i--;
      su1 = u1[i];
      su2 = u2[i];
      if (su1 != su2) {
        y = su1 > su2 ? 1 : -1;
      }
    }
  }

  return y;
}

boolean_T sMultiWordGe(const uint32_T u1[], const uint32_T u2[], int16_T n)
{
  return sMultiWordCmp(u1, u2, n) >= 0;
}

void MultiWordSub(const uint32_T u1[], const uint32_T u2[], uint32_T y[],
                  int16_T n)
{
  uint32_T yi;
  uint32_T u1i;
  uint32_T borrow = 0UL;
  int16_T i;
  for (i = 0; i < n; i++) {
    u1i = u1[i];
    yi = (u1i - u2[i]) - borrow;
    y[i] = yi;
    borrow = borrow != 0UL ? (uint32_T)(yi >= u1i) : (uint32_T)(yi > u1i);
  }
}

boolean_T sMultiWordLt(const uint32_T u1[], const uint32_T u2[], int16_T n)
{
  return sMultiWordCmp(u1, u2, n) < 0;
}

real_T sMultiWord2Double(const uint32_T u1[], int16_T n1, int16_T e1)
{
  real_T y;
  int16_T i;
  int16_T exp_0;
  uint32_T u1i;
  uint32_T cb;
  y = 0.0;
  exp_0 = e1;
  if ((u1[n1 - 1] & 2147483648UL) != 0UL) {
    cb = 1UL;
    for (i = 0; i < n1; i++) {
      u1i = ~u1[i];
      cb += u1i;
      y -= ldexp(cb, exp_0);
      cb = (uint32_T)(cb < u1i);
      exp_0 += 32;
    }
  } else {
    for (i = 0; i < n1; i++) {
      y += ldexp(u1[i], exp_0);
      exp_0 += 32;
    }
  }

  return y;
}

void uLong2MultiWord(uint32_T u, uint32_T y[], int16_T n)
{
  int16_T i;
  y[0] = u;
  for (i = 1; i < n; i++) {
    y[i] = 0UL;
  }
}

int32_T sMultiWord2sLongSat(const uint32_T u1[], int16_T n1)
{
  uint32_T y;
  sMultiWord2sMultiWordSat(u1, n1, &y, 1);
  return (int32_T)y;
}

void sMultiWord2sMultiWordSat(const uint32_T u1[], int16_T n1, uint32_T y[],
  int16_T n)
{
  int16_T nm1;
  uint32_T ys;
  int16_T n1m1;
  boolean_T doSaturation = false;
  nm1 = n - 1;
  n1m1 = n1 - 1;
  ys = (u1[n1m1] & 2147483648UL) != 0UL ? MAX_uint32_T : 0UL;
  if (n1 > n) {
    doSaturation = (((u1[n1m1] ^ u1[nm1]) & 2147483648UL) != 0UL);
    while ((!doSaturation) && (n1m1 >= n)) {
      doSaturation = (u1[n1m1] != ys);
      n1m1--;
    }
  }

  if (doSaturation) {
    ys = ~ys;
    for (n1m1 = 0; n1m1 < nm1; n1m1++) {
      y[n1m1] = ys;
    }

    y[n1m1] = ys ^ 2147483648UL;
  } else {
    nm1 = n1 < n ? n1 : n;
    for (n1m1 = 0; n1m1 < nm1; n1m1++) {
      y[n1m1] = u1[n1m1];
    }

    while (n1m1 < n) {
      y[n1m1] = ys;
      n1m1++;
    }
  }
}

boolean_T sMultiWordNe(const uint32_T u1[], const uint32_T u2[], int16_T n)
{
  return sMultiWordCmp(u1, u2, n) != 0;
}

/*
 *   This function updates active task flag for each subrate.
 * The function is called at model base rate, hence the
 * generated code self-manages all its subrates.
 */
static void rate_scheduler(void)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (Valvula_arduino_M->Timing.TaskCounters.TID[1])++;
  if ((Valvula_arduino_M->Timing.TaskCounters.TID[1]) > 499) {/* Sample time: [0.005s, 0.0s] */
    Valvula_arduino_M->Timing.TaskCounters.TID[1] = 0;
  }
}

/* Model step function */
void Valvula_arduino_step(void)
{
  uint8_T sts;
  uint8_T rawData[2];
  int16_T dataIn[2];
  int32_T rtb_IntegralGain;
  int16_T rtb_Saturation3;
  int16_T rtb_Gain2;
  uint8_T rtb_MultiportSwitch;
  uint8_T rtb_Delay1;
  boolean_T rtb_Compare_o;
  boolean_T rtb_LogicalOperator1_d;
  boolean_T rtb_LogicalOperator3;
  boolean_T LogicalOperator1_o;
  boolean_T LogicalOperator;
  static const char_T tmp[8] = { 'l', 'e', 'i', 't', 'u', 'r', 'a', ':' };

  uint32_T elapseTime_idx_0;
  int32_T tmp_0;
  int16_T i;
  uint16_T q0;
  uint16_T qY;
  uint32_T tmp_1;
  int64m_T tmp_2;
  int64m_T tmp_3;
  uint32_T elapseTime_idx_0_tmp;
  static int64m_T tmp_4 = { { 0UL, 0UL }/* chunks */
  };

  static int64m_T tmp_5 = { { MAX_uint32_T, MAX_uint32_T }/* chunks */
  };

  /* Outputs for Atomic SubSystem: '<Root>/SP_PWM__Counter' */
  /* RelationalOperator: '<S34>/Compare' incorporates:
   *  Constant: '<S34>/Constant'
   *  UnitDelay: '<S6>/Unit Delay1'
   */
  rtb_Compare_o = (Valvula_arduino_DW.UnitDelay1_DSTATE >=
                   Valvula_arduino_P.CompareToConstant_const);

  /* Start for MATLABSystem: '<S35>/Digital Input' */
  rtb_LogicalOperator3 = false;
  LogicalOperator = true;
  if (!(Valvula_arduino_DW.obj_m.SampleTime ==
        Valvula_arduino_P.DigitalInput_SampleTime)) {
    LogicalOperator = false;
  }

  if (LogicalOperator) {
    rtb_LogicalOperator3 = true;
  }

  if (!rtb_LogicalOperator3) {
    if (((!rtIsInf(Valvula_arduino_P.DigitalInput_SampleTime)) && (!rtIsNaN
          (Valvula_arduino_P.DigitalInput_SampleTime))) || rtIsInf
        (Valvula_arduino_P.DigitalInput_SampleTime)) {
      Valvula_arduino_B.sampleTime = Valvula_arduino_P.DigitalInput_SampleTime;
    }

    Valvula_arduino_DW.obj_m.SampleTime = Valvula_arduino_B.sampleTime;
  }

  /* MATLABSystem: '<S35>/Digital Input' */
  rtb_LogicalOperator3 = readDigitalPin((uint8_T)Valvula_arduino_PinNumber);

  /* Logic: '<S6>/Logical Operator' incorporates:
   *  MATLABSystem: '<S35>/Digital Input'
   */
  LogicalOperator = !rtb_LogicalOperator3;

  /* S-Function (sdspcount2): '<S6>/Counter' incorporates:
   *  MATLABSystem: '<S35>/Digital Input'
   *  S-Function (sdspcount2): '<S6>/Counter1'
   */
  if (LogicalOperator) {
    Valvula_arduino_DW.Counter_Count = Valvula_arduino_P.Counter_InitialCount;
  }

  if (rtb_LogicalOperator3) {
    Valvula_arduino_DW.Counter_Count++;
    Valvula_arduino_DW.Counter1_Count = Valvula_arduino_P.Counter1_InitialCount;
  }

  /* S-Function (sdspcount2): '<S6>/Counter1' incorporates:
   *  Logic: '<S6>/Logical Operator1'
   *  MATLABSystem: '<S35>/Digital Input'
   */
  if (!rtb_LogicalOperator3) {
    Valvula_arduino_DW.Counter1_Count++;
  }

  /* Outputs for Triggered SubSystem: '<S6>/Enabled Subsystem' incorporates:
   *  TriggerPort: '<S36>/Trigger'
   */
  if (LogicalOperator && (Valvula_arduino_PrevZCX.EnabledSubsystem_Trig_ZCE !=
                          POS_ZCSIG)) {
    /* Inport: '<S36>/In1' incorporates:
     *  UnitDelay: '<S6>/Unit Delay'
     */
    Valvula_arduino_B.In1_b = Valvula_arduino_DW.UnitDelay_DSTATE;
  }

  Valvula_arduino_PrevZCX.EnabledSubsystem_Trig_ZCE = LogicalOperator;

  /* End of Outputs for SubSystem: '<S6>/Enabled Subsystem' */

  /* Outputs for Triggered SubSystem: '<S6>/Enabled Subsystem1' incorporates:
   *  TriggerPort: '<S37>/Trigger'
   */
  /* MATLABSystem: '<S35>/Digital Input' incorporates:
   *  Inport: '<S37>/In1'
   *  UnitDelay: '<S6>/Unit Delay1'
   */
  if (rtb_LogicalOperator3 &&
      (Valvula_arduino_PrevZCX.EnabledSubsystem1_Trig_ZCE != POS_ZCSIG)) {
    Valvula_arduino_B.In1 = Valvula_arduino_DW.UnitDelay1_DSTATE;
  }

  Valvula_arduino_PrevZCX.EnabledSubsystem1_Trig_ZCE = rtb_LogicalOperator3;

  /* End of Outputs for SubSystem: '<S6>/Enabled Subsystem1' */

  /* Logic: '<S6>/Logical Operator3' incorporates:
   *  MATLABSystem: '<S35>/Digital Input'
   */
  rtb_LogicalOperator3 = (rtb_LogicalOperator3 || rtb_Compare_o);

  /* Outputs for Triggered SubSystem: '<S6>/Enabled Subsystem2' incorporates:
   *  TriggerPort: '<S38>/Trigger'
   */
  if (rtb_LogicalOperator3 &&
      (Valvula_arduino_PrevZCX.EnabledSubsystem2_Trig_ZCE != POS_ZCSIG)) {
    /* Inport: '<S38>/In1' */
    Valvula_arduino_B.In1_d = rtb_Compare_o;
  }

  Valvula_arduino_PrevZCX.EnabledSubsystem2_Trig_ZCE = rtb_LogicalOperator3;

  /* End of Outputs for SubSystem: '<S6>/Enabled Subsystem2' */

  /* Update for UnitDelay: '<S6>/Unit Delay1' incorporates:
   *  S-Function (sdspcount2): '<S6>/Counter1'
   */
  Valvula_arduino_DW.UnitDelay1_DSTATE = Valvula_arduino_DW.Counter1_Count;

  /* Update for UnitDelay: '<S6>/Unit Delay' incorporates:
   *  S-Function (sdspcount2): '<S6>/Counter'
   */
  Valvula_arduino_DW.UnitDelay_DSTATE = Valvula_arduino_DW.Counter_Count;

  /* End of Outputs for SubSystem: '<Root>/SP_PWM__Counter' */
  if (Valvula_arduino_M->Timing.TaskCounters.TID[1] == 0) {
    /* RelationalOperator: '<S2>/Compare' incorporates:
     *  Constant: '<S2>/Constant'
     *  UnitDelay: '<S3>/Output'
     */
    rtb_Compare_o = (Valvula_arduino_DW.Output_DSTATE >=
                     Valvula_arduino_P.CompareToConstant_const_p);

    /* Outputs for Triggered SubSystem: '<Root>/Algorithm' incorporates:
     *  TriggerPort: '<S1>/Trigger'
     */
    if (rtb_Compare_o && (Valvula_arduino_PrevZCX.Algorithm_Trig_ZCE !=
                          POS_ZCSIG)) {
      /* Start for MATLABSystem: '<S1>/Serial Receive' */
      MW_Serial_read(0U, Valvula_arduino_DW.obj_k.DataSizeInBytes,
                     Valvula_arduino_B.data, &sts);

      /* Delay: '<S10>/Delay1' */
      rtb_Delay1 = Valvula_arduino_DW.Delay1_DSTATE;

      /* Logic: '<S10>/Logical Operator1' incorporates:
       *  Constant: '<S31>/Constant'
       *  Delay: '<S10>/Delay'
       *  Delay: '<S10>/Delay1'
       *  Delay: '<S10>/Delay2'
       *  MATLABSystem: '<S1>/Serial Receive'
       *  MATLABSystem: '<S1>/Serial Receive'
       *  RelationalOperator: '<S31>/Compare'
       *  Sum: '<S10>/Sum'
       */
      rtb_LogicalOperator3 = !((uint8_T)((((uint16_T)sts +
        Valvula_arduino_DW.Delay1_DSTATE) + Valvula_arduino_DW.Delay2_DSTATE) +
        (Valvula_arduino_DW.Delay_DSTATE >=
         Valvula_arduino_P.CompareToConstant1_const_b)) != 0);

      /* Outputs for Enabled SubSystem: '<S1>/Diagnosis' incorporates:
       *  EnablePort: '<S8>/Enable'
       */
      /* Logic: '<S10>/Logical Operator2' */
      if (!rtb_LogicalOperator3) {
        /* Logic: '<S8>/Logical Operator' incorporates:
         *  Constant: '<S19>/Constant'
         *  Constant: '<S20>/Constant'
         *  Constant: '<S21>/Constant'
         *  Constant: '<S22>/Constant'
         *  Constant: '<S23>/Constant'
         *  Constant: '<S24>/Constant'
         *  Constant: '<S25>/Constant'
         *  Constant: '<S26>/Constant'
         *  MATLABSystem: '<S1>/Serial Receive'
         *  MATLABSystem: '<S1>/Serial Receive'
         *  RelationalOperator: '<S19>/Compare'
         *  RelationalOperator: '<S20>/Compare'
         *  RelationalOperator: '<S21>/Compare'
         *  RelationalOperator: '<S22>/Compare'
         *  RelationalOperator: '<S23>/Compare'
         *  RelationalOperator: '<S24>/Compare'
         *  RelationalOperator: '<S25>/Compare'
         *  RelationalOperator: '<S26>/Compare'
         */
        LogicalOperator = ((Valvula_arduino_B.data[0] ==
                            Valvula_arduino_P.CompareToConstant_const_j) &&
                           (Valvula_arduino_B.data[1] ==
                            Valvula_arduino_P.CompareToConstant1_const_e) &&
                           (Valvula_arduino_B.data[2] ==
                            Valvula_arduino_P.CompareToConstant2_const_m) &&
                           (Valvula_arduino_B.data[3] ==
                            Valvula_arduino_P.CompareToConstant3_const) &&
                           (Valvula_arduino_B.data[4] ==
                            Valvula_arduino_P.CompareToConstant4_const) &&
                           (Valvula_arduino_B.data[5] ==
                            Valvula_arduino_P.CompareToConstant5_const) &&
                           (Valvula_arduino_B.data[6] ==
                            Valvula_arduino_P.CompareToConstant6_const) &&
                           (Valvula_arduino_B.data[7] ==
                            Valvula_arduino_P.CompareToConstant7_const));

        /* Delay: '<S27>/Delay1' */
        rtb_LogicalOperator1_d = Valvula_arduino_DW.Delay1_DSTATE_d;

        /* Logic: '<S27>/Logical Operator1' incorporates:
         *  Constant: '<S30>/Constant'
         *  Delay: '<S27>/Delay'
         *  Delay: '<S27>/Delay1'
         *  Delay: '<S27>/Delay2'
         *  RelationalOperator: '<S30>/Compare'
         *  Sum: '<S27>/Sum'
         */
        LogicalOperator1_o = !((int16_T)((((uint16_T)LogicalOperator +
          Valvula_arduino_DW.Delay1_DSTATE_d) +
          Valvula_arduino_DW.Delay2_DSTATE_l) +
          (Valvula_arduino_DW.Delay_DSTATE_l >=
           Valvula_arduino_P.CompareToConstant1_const)) != 0);

        /* S-Function (sdspcount2): '<S27>/Counter' incorporates:
         *  Constant: '<S27>/Constant'
         */
        if (LogicalOperator1_o) {
          Valvula_arduino_DW.Counter_Count_nv =
            Valvula_arduino_P.Counter_InitialCount_l;
        }

        if (Valvula_arduino_P.Constant_Value_i != 0.0) {
          if (Valvula_arduino_DW.Counter_Count_nv < 74) {
            Valvula_arduino_DW.Counter_Count_nv++;
          } else {
            Valvula_arduino_DW.Counter_Count_nv = 0U;
          }
        }

        /* Logic: '<S27>/Logical Operator2' */
        Valvula_arduino_B.LogicalOperator2 = !LogicalOperator1_o;

        /* Update for Delay: '<S27>/Delay' incorporates:
         *  S-Function (sdspcount2): '<S27>/Counter'
         */
        Valvula_arduino_DW.Delay_DSTATE_l = Valvula_arduino_DW.Counter_Count_nv;

        /* Update for Delay: '<S27>/Delay1' */
        Valvula_arduino_DW.Delay1_DSTATE_d = LogicalOperator;

        /* Update for Delay: '<S27>/Delay2' */
        Valvula_arduino_DW.Delay2_DSTATE_l = rtb_LogicalOperator1_d;
      }

      /* End of Logic: '<S10>/Logical Operator2' */
      /* End of Outputs for SubSystem: '<S1>/Diagnosis' */

      /* Start for MATLABSystem: '<S1>/Sensor_Pos' */
      MW_i2cReadLoop((uint8_T)Valvula_arduino_SlaveAddress_, 1.0,
                     Valvula_arduin_RegisterAddress_, 2U, rawData);

      /* Outputs for Enabled SubSystem: '<S1>/Algorithm Subsystem' incorporates:
       *  EnablePort: '<S7>/Enable'
       */
      /* Logic: '<S1>/Logical Operator1' */
      if (!Valvula_arduino_B.LogicalOperator2) {
        elapseTime_idx_0_tmp = Valvula_arduino_M->Timing.clockTick1;
        elapseTime_idx_0 = elapseTime_idx_0_tmp -
          Valvula_arduino_DW.AlgorithmSubsystem_PREV_T[0];
        Valvula_arduino_DW.AlgorithmSubsystem_PREV_T[0] = elapseTime_idx_0_tmp;
        Valvula_arduino_DW.AlgorithmSubsystem_PREV_T[1] =
          Valvula_arduino_M->Timing.clockTickH1;

        /* Gain: '<S7>/Gain2' incorporates:
         *  Gain: '<S7>/Gain1'
         *  Logic: '<S7>/Logical Operator'
         *  Product: '<S7>/Divide'
         *  Product: '<S7>/Product'
         *  Sum: '<S7>/Sum'
         */
        Valvula_arduino_B.sampleTime = floor((real_T)Valvula_arduino_B.In1_b /
          ((real_T)Valvula_arduino_B.In1_b + (real_T)Valvula_arduino_B.In1) *
          (!Valvula_arduino_B.In1_d ? (real_T)Valvula_arduino_P.Gain1_Gain *
           0.0078125 : 0.0) * Valvula_arduino_P.Gain2_Gain);
        if (rtIsNaN(Valvula_arduino_B.sampleTime) || rtIsInf
            (Valvula_arduino_B.sampleTime)) {
          Valvula_arduino_B.sampleTime = 0.0;
        } else {
          Valvula_arduino_B.sampleTime = fmod(Valvula_arduino_B.sampleTime,
            65536.0);
        }

        rtb_Gain2 = Valvula_arduino_B.sampleTime < 0.0 ? -(int16_T)(uint16_T)
          -Valvula_arduino_B.sampleTime : (int16_T)(uint16_T)
          Valvula_arduino_B.sampleTime;

        /* End of Gain: '<S7>/Gain2' */

        /* Start for MATLABSystem: '<S1>/Sensor_Pos' */
        q0 = (uint16_T)rawData[0] << 8U;
        qY = q0 + /*MW:OvSatOk*/ rawData[1];
        if (qY < q0) {
          qY = MAX_uint16_T;
        }

        /* Gain: '<S1>/Gain3' incorporates:
         *  Bias: '<S1>/Bias1'
         *  DataTypeConversion: '<S1>/Data Type Conversion3'
         *  MATLABSystem: '<S1>/Sensor_Pos'
         *  MATLABSystem: '<S1>/Sensor_Pos'
         */
        Valvula_arduino_B.sampleTime = (real_T)Valvula_arduino_P.Gain3_Gain *
          7.62939453125E-6 * (real_T)((int16_T)qY + Valvula_arduino_P.Bias1_Bias);

        /* Saturate: '<S7>/Saturation3' */
        if (Valvula_arduino_B.sampleTime >
            Valvula_arduino_P.Saturation3_UpperSat) {
          Valvula_arduino_B.sampleTime = Valvula_arduino_P.Saturation3_UpperSat;
        } else {
          if (Valvula_arduino_B.sampleTime <
              Valvula_arduino_P.Saturation3_LowerSat) {
            Valvula_arduino_B.sampleTime =
              Valvula_arduino_P.Saturation3_LowerSat;
          }
        }

        i = (int16_T)fmod((int16_T)floor(Valvula_arduino_B.sampleTime), 65536.0);
        rtb_Saturation3 = i < 0 ? -(int16_T)(uint16_T)-(real_T)i : i;

        /* End of Saturate: '<S7>/Saturation3' */

        /* MATLABSystem: '<S7>/Serial Transmit' incorporates:
         *  SignalConversion: '<S7>/TmpSignal ConversionAtSerial TransmitInport1'
         */
        dataIn[0] = rtb_Gain2;
        dataIn[1] = rtb_Saturation3;
        for (i = 0; i < 8; i++) {
          Valvula_arduino_B.cv0[i] = tmp[i];
        }

        Valvula_arduino_B.cv0[8] = '\x00';
        MW_Serial_write(Valvula_arduino_DW.obj.port, dataIn, 2.0,
                        Valvula_arduino_DW.obj.dataSizeInBytes,
                        Valvula_arduino_DW.obj.sendModeEnum,
                        Valvula_arduino_DW.obj.dataType,
                        Valvula_arduino_DW.obj.sendFormatEnum,
                        Valvula_arduino_floatprecision, Valvula_arduino_B.cv0);

        /* End of MATLABSystem: '<S7>/Serial Transmit' */

        /* Gain: '<S12>/Proportional Gain' incorporates:
         *  Gain: '<S12>/Setpoint Weighting (Proportional)'
         *  Sum: '<S12>/Sum1'
         */
        elapseTime_idx_0_tmp = (uint32_T)Valvula_arduino_P.P;
        tmp_1 = (uint32_T)((int32_T)Valvula_arduino_P.B * rtb_Gain2 - ((int32_T)
          rtb_Saturation3 << 14));
        sMultiWordMul(&elapseTime_idx_0_tmp, 1, &tmp_1, 1,
                      &Valvula_arduino_B.rtb_Sum_m.chunks[0U], 2);

        /* Sum: '<S12>/Sum' incorporates:
         *  DiscreteIntegrator: '<S12>/Integrator'
         */
        elapseTime_idx_0_tmp = (uint32_T)Valvula_arduino_DW.Integrator_DSTATE;
        tmp_1 = 1374389535UL;
        sMultiWordMul(&elapseTime_idx_0_tmp, 1, &tmp_1, 1, &tmp_2.chunks[0U], 2);
        sMultiWordShr(&tmp_2.chunks[0U], 2, 5U, &Valvula_arduino_B.r2.chunks[0U],
                      2);
        MultiWordAdd(&Valvula_arduino_B.rtb_Sum_m.chunks[0U],
                     &Valvula_arduino_B.r2.chunks[0U],
                     &Valvula_arduino_B.Sum.chunks[0U], 2);

        /* DeadZone: '<S14>/DeadZone' */
        if (sMultiWordGt(&Valvula_arduino_B.Sum.chunks[0U],
                         &Valvula_arduino_P.DiscreteVarying2DOFPID2_UpperSa.chunks
                         [0U], 2)) {
          MultiWordSub(&Valvula_arduino_B.Sum.chunks[0U],
                       &Valvula_arduino_P.DiscreteVarying2DOFPID2_UpperSa.chunks[
                       0U], &Valvula_arduino_B.DeadZone.chunks[0U], 2);
        } else if (sMultiWordGe(&Valvula_arduino_B.Sum.chunks[0U],
                                &Valvula_arduino_P.DiscreteVarying2DOFPID2_LowerSa.chunks
                                [0U], 2)) {
          Valvula_arduino_B.DeadZone = tmp_4;
        } else {
          MultiWordSub(&Valvula_arduino_B.Sum.chunks[0U],
                       &Valvula_arduino_P.DiscreteVarying2DOFPID2_LowerSa.chunks[
                       0U], &Valvula_arduino_B.DeadZone.chunks[0U], 2);
        }

        /* End of DeadZone: '<S14>/DeadZone' */

        /* Gain: '<S12>/Integral Gain' incorporates:
         *  Sum: '<S12>/Sum2'
         */
        rtb_IntegralGain = (int32_T)(rtb_Gain2 - rtb_Saturation3) *
          Valvula_arduino_P.I;

        /* Switch: '<S13>/Switch' incorporates:
         *  Constant: '<S13>/Constant'
         *  Constant: '<S16>/Constant'
         *  Constant: '<S17>/Constant'
         *  Logic: '<S13>/Logical Operator'
         *  Product: '<S13>/Product'
         *  RelationalOperator: '<S16>/Compare'
         *  RelationalOperator: '<S17>/Compare'
         *  Saturate: '<S12>/Saturate'
         */
        if ((rtb_Gain2 >= Valvula_arduino_P.CompareToConstant1_const_f) &&
            (rtb_Saturation3 >= Valvula_arduino_P.CompareToConstant2_const)) {
          Valvula_arduino_B.Switch = Valvula_arduino_P.Constant_Value;
        } else {
          if (sMultiWordGt(&Valvula_arduino_B.Sum.chunks[0U],
                           &Valvula_arduino_P.DiscreteVarying2DOFPID2_UpperSa.chunks
                           [0U], 2)) {
            /* Saturate: '<S12>/Saturate' */
            Valvula_arduino_B.rtb_Sum_m =
              Valvula_arduino_P.DiscreteVarying2DOFPID2_UpperSa;
          } else if (sMultiWordLt(&Valvula_arduino_B.Sum.chunks[0U],
                                  &Valvula_arduino_P.DiscreteVarying2DOFPID2_LowerSa.chunks
                                  [0U], 2)) {
            /* Saturate: '<S12>/Saturate' */
            Valvula_arduino_B.rtb_Sum_m =
              Valvula_arduino_P.DiscreteVarying2DOFPID2_LowerSa;
          } else {
            /* Saturate: '<S12>/Saturate' */
            Valvula_arduino_B.rtb_Sum_m = Valvula_arduino_B.Sum;
          }

          /* Product: '<S13>/Product' incorporates:
           *  Constant: '<S15>/Constant'
           *  RelationalOperator: '<S15>/Compare'
           *  Saturate: '<S12>/Saturate'
           */
          if (!(rtb_Gain2 >= Valvula_arduino_P.CompareToConstant_const_c)) {
            Valvula_arduino_B.rtb_Sum_m = tmp_4;
          }

          Valvula_arduino_B.Switch = sMultiWord2Double
            (&Valvula_arduino_B.rtb_Sum_m.chunks[0U], 2, 0) *
            2.2737367544323206E-13;
        }

        /* End of Switch: '<S13>/Switch' */

        /* Signum: '<S14>/SignDeltaU' */
        Valvula_arduino_B.rtb_Sum_m = tmp_4;
        if (sMultiWordLt(&Valvula_arduino_B.DeadZone.chunks[0U],
                         &Valvula_arduino_B.rtb_Sum_m.chunks[0U], 2)) {
          Valvula_arduino_B.rtb_DeadZone_c = tmp_5;
        } else {
          Valvula_arduino_B.rtb_Sum_m = tmp_4;
          uLong2MultiWord(sMultiWordGt(&Valvula_arduino_B.DeadZone.chunks[0U],
            &Valvula_arduino_B.rtb_Sum_m.chunks[0U], 2),
                          &Valvula_arduino_B.rtb_DeadZone_c.chunks[0U], 2);
        }

        /* End of Signum: '<S14>/SignDeltaU' */

        /* DataTypeConversion: '<S14>/DataTypeConv1' */
        tmp_0 = sMultiWord2sLongSat(&Valvula_arduino_B.rtb_DeadZone_c.chunks[0U],
          2);
        if (tmp_0 > 127L) {
          tmp_0 = 127L;
        } else {
          if (tmp_0 < -128L) {
            tmp_0 = -128L;
          }
        }

        /* Gain: '<S14>/ZeroGain' */
        sMultiWordMul(&Valvula_arduino_P.ZeroGain_Gain.chunks[0U], 2,
                      &Valvula_arduino_B.Sum.chunks[0U], 2,
                      &Valvula_arduino_B.r1.chunks[0U], 4);
        sMultiWordShr(&Valvula_arduino_B.r1.chunks[0U], 4, 42U,
                      &Valvula_arduino_B.r0.chunks[0U], 4);
        sMultiWord2sMultiWordSat(&Valvula_arduino_B.r0.chunks[0U], 4,
          &tmp_3.chunks[0U], 2);

        /* Signum: '<S14>/SignPreIntegrator' */
        if (rtb_IntegralGain < 0L) {
          rtb_Gain2 = -1;
        } else {
          rtb_Gain2 = (rtb_IntegralGain > 0L);
        }

        /* End of Signum: '<S14>/SignPreIntegrator' */

        /* Switch: '<S12>/Switch' incorporates:
         *  Constant: '<S12>/Constant'
         *  DataTypeConversion: '<S14>/DataTypeConv1'
         *  Logic: '<S14>/AND'
         *  RelationalOperator: '<S14>/Equal'
         *  RelationalOperator: '<S14>/NotEqual'
         */
        if (sMultiWordNe(&tmp_3.chunks[0U], &Valvula_arduino_B.DeadZone.chunks
                         [0U], 2) && ((int16_T)tmp_0 == rtb_Gain2)) {
          rtb_IntegralGain = Valvula_arduino_P.Constant_Value_j;
        }

        /* Update for DiscreteIntegrator: '<S12>/Integrator' incorporates:
         *  Switch: '<S12>/Switch'
         */
        Valvula_arduino_DW.Integrator_DSTATE += (int32_T)elapseTime_idx_0 *
          rtb_IntegralGain;
      }

      /* End of Logic: '<S1>/Logical Operator1' */
      /* End of Outputs for SubSystem: '<S1>/Algorithm Subsystem' */

      /* MultiPortSwitch: '<S1>/Multiport Switch' */
      if (!Valvula_arduino_B.LogicalOperator2) {
        Valvula_arduino_B.sampleTime = floor(Valvula_arduino_B.Switch);
        if (rtIsNaN(Valvula_arduino_B.sampleTime) || rtIsInf
            (Valvula_arduino_B.sampleTime)) {
          Valvula_arduino_B.sampleTime = 0.0;
        } else {
          Valvula_arduino_B.sampleTime = fmod(Valvula_arduino_B.sampleTime,
            256.0);
        }

        rtb_MultiportSwitch = (uint8_T)(Valvula_arduino_B.sampleTime < 0.0 ?
          (int16_T)(uint8_T)-(int8_T)(uint8_T)-Valvula_arduino_B.sampleTime :
          (int16_T)(uint8_T)Valvula_arduino_B.sampleTime);
      } else {
        rtb_MultiportSwitch = 0U;
      }

      /* End of MultiPortSwitch: '<S1>/Multiport Switch' */

      /* S-Function (arduinoanalogoutput_sfcn): '<S9>/PWM' */
      MW_analogWrite(Valvula_arduino_P.PWM_pinNumber, rtb_MultiportSwitch);

      /* S-Function (sdspcount2): '<S10>/Counter' incorporates:
       *  Constant: '<S10>/Constant'
       */
      if (rtb_LogicalOperator3) {
        Valvula_arduino_DW.Counter_Count_n =
          Valvula_arduino_P.Counter_InitialCount_m;
      }

      if (Valvula_arduino_P.Constant_Value_l != 0.0) {
        if (Valvula_arduino_DW.Counter_Count_n < 76) {
          Valvula_arduino_DW.Counter_Count_n++;
        } else {
          Valvula_arduino_DW.Counter_Count_n = 0U;
        }
      }

      /* Update for Delay: '<S10>/Delay1' incorporates:
       *  MATLABSystem: '<S1>/Serial Receive'
       *  MATLABSystem: '<S1>/Serial Receive'
       */
      Valvula_arduino_DW.Delay1_DSTATE = sts;

      /* Update for Delay: '<S10>/Delay2' */
      Valvula_arduino_DW.Delay2_DSTATE = rtb_Delay1;

      /* Update for Delay: '<S10>/Delay' incorporates:
       *  S-Function (sdspcount2): '<S10>/Counter'
       */
      Valvula_arduino_DW.Delay_DSTATE = Valvula_arduino_DW.Counter_Count_n;
    }

    Valvula_arduino_PrevZCX.Algorithm_Trig_ZCE = rtb_Compare_o;

    /* End of Outputs for SubSystem: '<Root>/Algorithm' */

    /* Sum: '<S32>/FixPt Sum1' incorporates:
     *  Constant: '<S32>/FixPt Constant'
     *  UnitDelay: '<S3>/Output'
     */
    sts = (uint8_T)((uint16_T)Valvula_arduino_DW.Output_DSTATE +
                    Valvula_arduino_P.FixPtConstant_Value);

    /* Switch: '<S33>/FixPt Switch' */
    if (sts > Valvula_arduino_P.CounterLimited_uplimit) {
      /* Update for UnitDelay: '<S3>/Output' incorporates:
       *  Constant: '<S33>/Constant'
       */
      Valvula_arduino_DW.Output_DSTATE = Valvula_arduino_P.Constant_Value_ib;
    } else {
      /* Update for UnitDelay: '<S3>/Output' */
      Valvula_arduino_DW.Output_DSTATE = sts;
    }

    /* End of Switch: '<S33>/FixPt Switch' */
  }

  if (Valvula_arduino_M->Timing.TaskCounters.TID[1] == 0) {
    /* Update absolute timer for sample time: [0.005s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The resolution of this integer timer is 0.005, which is the step size
     * of the task. Size of "clockTick1" ensures timer will not overflow during the
     * application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    Valvula_arduino_M->Timing.clockTick1++;
    if (!Valvula_arduino_M->Timing.clockTick1) {
      Valvula_arduino_M->Timing.clockTickH1++;
    }
  }

  rate_scheduler();
}

/* Model initialize function */
void Valvula_arduino_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)Valvula_arduino_M, 0,
                sizeof(RT_MODEL_Valvula_arduino_T));

  /* block I/O */
  (void) memset(((void *) &Valvula_arduino_B), 0,
                sizeof(B_Valvula_arduino_T));

  /* states (dwork) */
  (void) memset((void *)&Valvula_arduino_DW, 0,
                sizeof(DW_Valvula_arduino_T));

  {
    real_T sampleTime;

    /* Start for Atomic SubSystem: '<Root>/SP_PWM__Counter' */
    /* Start for MATLABSystem: '<S35>/Digital Input' */
    Valvula_arduino_DW.obj_m.isInitialized = 0L;
    if (((!rtIsInf(Valvula_arduino_P.DigitalInput_SampleTime)) && (!rtIsNaN
          (Valvula_arduino_P.DigitalInput_SampleTime))) || rtIsInf
        (Valvula_arduino_P.DigitalInput_SampleTime)) {
      sampleTime = Valvula_arduino_P.DigitalInput_SampleTime;
    }

    Valvula_arduino_DW.obj_m.SampleTime = sampleTime;
    Valvula_arduino_DW.obj_m.isInitialized = 1L;
    digitalIOSetup((uint8_T)Valvula_arduino_PinNumber, false);

    /* End of Start for MATLABSystem: '<S35>/Digital Input' */
    /* End of Start for SubSystem: '<Root>/SP_PWM__Counter' */

    /* Start for Triggered SubSystem: '<Root>/Algorithm' */
    /* Start for MATLABSystem: '<S1>/Serial Receive' */
    Valvula_arduino_DW.obj_k.isInitialized = 0L;
    Valvula_arduino_DW.obj_k.isInitialized = 1L;
    Valvula_arduino_DW.obj_k.DataTypeWidth = 1U;
    if (Valvula_arduino_DW.obj_k.DataTypeWidth > 8191U) {
      Valvula_arduino_DW.obj_k.DataSizeInBytes = MAX_uint16_T;
    } else {
      Valvula_arduino_DW.obj_k.DataSizeInBytes =
        Valvula_arduino_DW.obj_k.DataTypeWidth << 3;
    }

    /* End of Start for MATLABSystem: '<S1>/Serial Receive' */

    /* Start for MATLABSystem: '<S1>/Sensor_Pos' */
    Valvula_arduino_DW.obj_n.isInitialized = 0L;
    Valvula_arduino_DW.obj_n.isInitialized = 1L;

    /* Start for Enabled SubSystem: '<S1>/Algorithm Subsystem' */
    /* Start for MATLABSystem: '<S7>/Serial Transmit' */
    Valvula_arduino_DW.obj.isInitialized = 0L;
    Valvula_arduino_DW.obj.isInitialized = 1L;
    Valvula_arduino_DW.obj.port = 1.0;
    Valvula_arduino_DW.obj.dataSizeInBytes = 2.0;
    Valvula_arduino_DW.obj.dataType = 4.0;
    Valvula_arduino_DW.obj.sendModeEnum = 0.0;
    Valvula_arduino_DW.obj.sendFormatEnum = 0.0;

    /* End of Start for SubSystem: '<S1>/Algorithm Subsystem' */

    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S9>/PWM' */
    MW_pinModeOutput(Valvula_arduino_P.PWM_pinNumber);

    /* End of Start for SubSystem: '<Root>/Algorithm' */
    Valvula_arduino_PrevZCX.Algorithm_Trig_ZCE = POS_ZCSIG;
    Valvula_arduino_PrevZCX.EnabledSubsystem_Trig_ZCE = POS_ZCSIG;
    Valvula_arduino_PrevZCX.EnabledSubsystem1_Trig_ZCE = POS_ZCSIG;
    Valvula_arduino_PrevZCX.EnabledSubsystem2_Trig_ZCE = POS_ZCSIG;

    /* InitializeConditions for UnitDelay: '<S3>/Output' */
    Valvula_arduino_DW.Output_DSTATE = Valvula_arduino_P.Output_InitialCondition;

    /* SystemInitialize for Atomic SubSystem: '<Root>/SP_PWM__Counter' */
    /* InitializeConditions for UnitDelay: '<S6>/Unit Delay1' */
    Valvula_arduino_DW.UnitDelay1_DSTATE =
      Valvula_arduino_P.UnitDelay1_InitialCondition;

    /* InitializeConditions for S-Function (sdspcount2): '<S6>/Counter' */
    Valvula_arduino_DW.Counter_Count = Valvula_arduino_P.Counter_InitialCount;

    /* InitializeConditions for S-Function (sdspcount2): '<S6>/Counter1' */
    Valvula_arduino_DW.Counter1_Count = Valvula_arduino_P.Counter1_InitialCount;

    /* InitializeConditions for UnitDelay: '<S6>/Unit Delay' */
    Valvula_arduino_DW.UnitDelay_DSTATE =
      Valvula_arduino_P.UnitDelay_InitialCondition;

    /* SystemInitialize for Triggered SubSystem: '<S6>/Enabled Subsystem' */
    /* SystemInitialize for Outport: '<S36>/Out1' */
    Valvula_arduino_B.In1_b = Valvula_arduino_P.Out1_Y0;

    /* End of SystemInitialize for SubSystem: '<S6>/Enabled Subsystem' */

    /* SystemInitialize for Triggered SubSystem: '<S6>/Enabled Subsystem1' */
    /* SystemInitialize for Outport: '<S37>/Out1' */
    Valvula_arduino_B.In1 = Valvula_arduino_P.Out1_Y0_a;

    /* End of SystemInitialize for SubSystem: '<S6>/Enabled Subsystem1' */

    /* SystemInitialize for Triggered SubSystem: '<S6>/Enabled Subsystem2' */
    /* SystemInitialize for Outport: '<S38>/Out1' */
    Valvula_arduino_B.In1_d = Valvula_arduino_P.Out1_Y0_p;

    /* End of SystemInitialize for SubSystem: '<S6>/Enabled Subsystem2' */
    /* End of SystemInitialize for SubSystem: '<Root>/SP_PWM__Counter' */

    /* SystemInitialize for Triggered SubSystem: '<Root>/Algorithm' */
    /* InitializeConditions for Delay: '<S10>/Delay1' */
    Valvula_arduino_DW.Delay1_DSTATE = Valvula_arduino_P.Delay1_InitialCondition;

    /* InitializeConditions for Delay: '<S10>/Delay2' */
    Valvula_arduino_DW.Delay2_DSTATE = Valvula_arduino_P.Delay2_InitialCondition;

    /* InitializeConditions for Delay: '<S10>/Delay' */
    Valvula_arduino_DW.Delay_DSTATE = Valvula_arduino_P.Delay_InitialCondition_c;

    /* InitializeConditions for S-Function (sdspcount2): '<S10>/Counter' */
    Valvula_arduino_DW.Counter_Count_n =
      Valvula_arduino_P.Counter_InitialCount_m;

    /* SystemInitialize for Enabled SubSystem: '<S1>/Diagnosis' */
    /* InitializeConditions for Delay: '<S27>/Delay' */
    Valvula_arduino_DW.Delay_DSTATE_l = Valvula_arduino_P.Delay_InitialCondition;

    /* InitializeConditions for Delay: '<S27>/Delay1' */
    Valvula_arduino_DW.Delay1_DSTATE_d =
      Valvula_arduino_P.Delay1_InitialCondition_m;

    /* InitializeConditions for Delay: '<S27>/Delay2' */
    Valvula_arduino_DW.Delay2_DSTATE_l =
      Valvula_arduino_P.Delay2_InitialCondition_j;

    /* InitializeConditions for S-Function (sdspcount2): '<S27>/Counter' */
    Valvula_arduino_DW.Counter_Count_nv =
      Valvula_arduino_P.Counter_InitialCount_l;

    /* End of SystemInitialize for SubSystem: '<S1>/Diagnosis' */

    /* SystemInitialize for Enabled SubSystem: '<S1>/Algorithm Subsystem' */
    /* InitializeConditions for DiscreteIntegrator: '<S12>/Integrator' */
    Valvula_arduino_DW.Integrator_DSTATE = Valvula_arduino_P.Integrator_IC;

    /* End of SystemInitialize for SubSystem: '<S1>/Algorithm Subsystem' */
    /* End of SystemInitialize for SubSystem: '<Root>/Algorithm' */
  }
}

/* Model terminate function */
void Valvula_arduino_terminate(void)
{
  /* Terminate for Atomic SubSystem: '<Root>/SP_PWM__Counter' */
  /* Start for MATLABSystem: '<S35>/Digital Input' */
  if (Valvula_arduino_DW.obj_m.isInitialized == 1L) {
    Valvula_arduino_DW.obj_m.isInitialized = 2L;
  }

  /* End of Start for MATLABSystem: '<S35>/Digital Input' */
  /* End of Terminate for SubSystem: '<Root>/SP_PWM__Counter' */

  /* Terminate for Triggered SubSystem: '<Root>/Algorithm' */
  /* Start for MATLABSystem: '<S1>/Serial Receive' */
  if (Valvula_arduino_DW.obj_k.isInitialized == 1L) {
    Valvula_arduino_DW.obj_k.isInitialized = 2L;
  }

  /* End of Start for MATLABSystem: '<S1>/Serial Receive' */

  /* Start for MATLABSystem: '<S1>/Sensor_Pos' */
  if (Valvula_arduino_DW.obj_n.isInitialized == 1L) {
    Valvula_arduino_DW.obj_n.isInitialized = 2L;
  }

  /* End of Start for MATLABSystem: '<S1>/Sensor_Pos' */

  /* Terminate for Enabled SubSystem: '<S1>/Algorithm Subsystem' */
  /* Terminate for MATLABSystem: '<S7>/Serial Transmit' */
  if (Valvula_arduino_DW.obj.isInitialized == 1L) {
    Valvula_arduino_DW.obj.isInitialized = 2L;
  }

  /* End of Terminate for MATLABSystem: '<S7>/Serial Transmit' */
  /* End of Terminate for SubSystem: '<S1>/Algorithm Subsystem' */
  /* End of Terminate for SubSystem: '<Root>/Algorithm' */
}
