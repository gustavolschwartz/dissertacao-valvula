/*
 * File: Valvula_arduino_data.c
 *
 * Code generated for Simulink model 'Valvula_arduino'.
 *
 * Model version                  : 1.21
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 14:46:08 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "Valvula_arduino.h"
#include "Valvula_arduino_private.h"

/* Block parameters (auto storage) */
P_Valvula_arduino_T Valvula_arduino_P = {
  /* Variable: P
   * Referenced by: '<S12>/Proportional Gain'
   */
  1436469712,

  /* Variable: B
   * Referenced by: '<S12>/Setpoint Weighting (Proportional)'
   */
  17593,

  /* Variable: I
   * Referenced by: '<S12>/Integral Gain'
   */
  23234,

  /* Mask Parameter: CompareToConstant1_const
   * Referenced by: '<S30>/Constant'
   */
  2.0,

  /* Mask Parameter: CompareToConstant1_const_b
   * Referenced by: '<S31>/Constant'
   */
  2.0,

  /* Mask Parameter: DiscreteVarying2DOFPID2_LowerSa
   * Referenced by:
   *   '<S12>/Saturate'
   *   '<S14>/DeadZone'
   */
  { { 0UL, 0UL } },

  /* Mask Parameter: DiscreteVarying2DOFPID2_UpperSa
   * Referenced by:
   *   '<S12>/Saturate'
   *   '<S14>/DeadZone'
   */
  { { 0UL, 0x3FC00UL } },

  /* Mask Parameter: Counter_InitialCount
   * Referenced by: '<S6>/Counter'
   */
  0U,

  /* Mask Parameter: Counter1_InitialCount
   * Referenced by: '<S6>/Counter1'
   */
  0U,

  /* Mask Parameter: CompareToConstant_const
   * Referenced by: '<S34>/Constant'
   */
  500U,

  /* Mask Parameter: PWM_pinNumber
   * Referenced by: '<S9>/PWM'
   */
  3U,

  /* Mask Parameter: CompareToConstant_const_c
   * Referenced by: '<S15>/Constant'
   */
  1,

  /* Mask Parameter: CompareToConstant1_const_f
   * Referenced by: '<S16>/Constant'
   */
  98,

  /* Mask Parameter: CompareToConstant2_const
   * Referenced by: '<S17>/Constant'
   */
  98,

  /* Mask Parameter: Counter_InitialCount_l
   * Referenced by: '<S27>/Counter'
   */
  0U,

  /* Mask Parameter: Counter_InitialCount_m
   * Referenced by: '<S10>/Counter'
   */
  0U,

  /* Mask Parameter: CompareToConstant_const_j
   * Referenced by: '<S19>/Constant'
   */
  99U,

  /* Mask Parameter: CompareToConstant1_const_e
   * Referenced by: '<S20>/Constant'
   */
  104U,

  /* Mask Parameter: CompareToConstant2_const_m
   * Referenced by: '<S21>/Constant'
   */
  112U,

  /* Mask Parameter: CompareToConstant3_const
   * Referenced by: '<S22>/Constant'
   */
  48U,

  /* Mask Parameter: CompareToConstant4_const
   * Referenced by: '<S23>/Constant'
   */
  49U,

  /* Mask Parameter: CompareToConstant5_const
   * Referenced by: '<S24>/Constant'
   */
  48U,

  /* Mask Parameter: CompareToConstant6_const
   * Referenced by: '<S25>/Constant'
   */
  48U,

  /* Mask Parameter: CompareToConstant7_const
   * Referenced by: '<S26>/Constant'
   */
  49U,

  /* Mask Parameter: CompareToConstant_const_p
   * Referenced by: '<S2>/Constant'
   */
  1U,

  /* Mask Parameter: CounterLimited_uplimit
   * Referenced by: '<S33>/FixPt Switch'
   */
  1U,

  /* Expression: 120
   * Referenced by: '<S13>/Constant'
   */
  120.0,

  /* Expression: 100
   * Referenced by: '<S7>/Gain2'
   */
  100.0,

  /* Expression: 0.0
   * Referenced by: '<S27>/Delay'
   */
  0.0,

  /* Expression: 1
   * Referenced by: '<S27>/Constant'
   */
  1.0,

  /* Expression: 0.0
   * Referenced by: '<S10>/Delay'
   */
  0.0,

  /* Expression: 1
   * Referenced by: '<S10>/Constant'
   */
  1.0,

  /* Expression: 1/100000
   * Referenced by: '<S35>/Digital Input'
   */
  1.0E-5,

  /* Computed Parameter: ZeroGain_Gain
   * Referenced by: '<S14>/ZeroGain'
   */
  { { 0UL, 0UL } },

  /* Computed Parameter: Out1_Y0
   * Referenced by: '<S36>/Out1'
   */
  0U,

  /* Computed Parameter: Out1_Y0_a
   * Referenced by: '<S37>/Out1'
   */
  0U,

  /* Computed Parameter: UnitDelay1_InitialCondition
   * Referenced by: '<S6>/Unit Delay1'
   */
  0U,

  /* Computed Parameter: UnitDelay_InitialCondition
   * Referenced by: '<S6>/Unit Delay'
   */
  0U,

  /* Computed Parameter: Constant_Value_j
   * Referenced by: '<S12>/Constant'
   */
  0,

  /* Computed Parameter: Integrator_IC
   * Referenced by: '<S12>/Integrator'
   */
  0,

  /* Computed Parameter: Saturation3_UpperSat
   * Referenced by: '<S7>/Saturation3'
   */
  99,

  /* Computed Parameter: Saturation3_LowerSat
   * Referenced by: '<S7>/Saturation3'
   */
  1,

  /* Computed Parameter: Bias1_Bias
   * Referenced by: '<S1>/Bias1'
   */
  -1610,

  /* Computed Parameter: Delay_DelayLength
   * Referenced by: '<S27>/Delay'
   */
  1U,

  /* Computed Parameter: Delay1_DelayLength
   * Referenced by: '<S27>/Delay1'
   */
  1U,

  /* Computed Parameter: Delay2_DelayLength
   * Referenced by: '<S27>/Delay2'
   */
  1U,

  /* Computed Parameter: Delay1_DelayLength_b
   * Referenced by: '<S10>/Delay1'
   */
  1U,

  /* Computed Parameter: Delay2_DelayLength_m
   * Referenced by: '<S10>/Delay2'
   */
  1U,

  /* Computed Parameter: Delay_DelayLength_g
   * Referenced by: '<S10>/Delay'
   */
  1U,

  /* Computed Parameter: Gain3_Gain
   * Referenced by: '<S1>/Gain3'
   */
  16487,

  /* Computed Parameter: Delay1_InitialCondition
   * Referenced by: '<S10>/Delay1'
   */
  0U,

  /* Computed Parameter: Delay2_InitialCondition
   * Referenced by: '<S10>/Delay2'
   */
  0U,

  /* Computed Parameter: Constant_Value_ib
   * Referenced by: '<S33>/Constant'
   */
  0U,

  /* Computed Parameter: Output_InitialCondition
   * Referenced by: '<S3>/Output'
   */
  0U,

  /* Computed Parameter: FixPtConstant_Value
   * Referenced by: '<S32>/FixPt Constant'
   */
  1U,

  /* Computed Parameter: Delay1_InitialCondition_m
   * Referenced by: '<S27>/Delay1'
   */
  0,

  /* Computed Parameter: Delay2_InitialCondition_j
   * Referenced by: '<S27>/Delay2'
   */
  0,

  /* Computed Parameter: Out1_Y0_p
   * Referenced by: '<S38>/Out1'
   */
  0,

  /* Computed Parameter: Gain1_Gain
   * Referenced by: '<S7>/Gain1'
   */
  128U
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
