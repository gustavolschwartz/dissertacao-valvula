/*
 * Valvula_arduino_types.h
 *
 * Code generation for model "Valvula_arduino".
 *
 * Model version              : 1.21
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Mon Sep 14 14:46:08 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Valvula_arduino_types_h_
#define RTW_HEADER_Valvula_arduino_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"
#ifndef struct_tag_sMXuVOqiR8VrXITXlaWuyyE
#define struct_tag_sMXuVOqiR8VrXITXlaWuyyE

struct tag_sMXuVOqiR8VrXITXlaWuyyE
{
  real_T uint8;
  real_T uint16;
};

#endif                                 /*struct_tag_sMXuVOqiR8VrXITXlaWuyyE*/

#ifndef typedef_sMXuVOqiR8VrXITXlaWuyyE_Valvu_T
#define typedef_sMXuVOqiR8VrXITXlaWuyyE_Valvu_T

typedef struct tag_sMXuVOqiR8VrXITXlaWuyyE sMXuVOqiR8VrXITXlaWuyyE_Valvu_T;

#endif                                 /*typedef_sMXuVOqiR8VrXITXlaWuyyE_Valvu_T*/

#ifndef typedef_codertarget_arduinobase_inter_T
#define typedef_codertarget_arduinobase_inter_T

typedef struct {
  int32_T isInitialized;
} codertarget_arduinobase_inter_T;

#endif                                 /*typedef_codertarget_arduinobase_inter_T*/

#ifndef typedef_codertarget_arduinobase_int_i_T
#define typedef_codertarget_arduinobase_int_i_T

typedef struct {
  int32_T isInitialized;
  uint16_T DataSizeInBytes;
  uint16_T DataTypeWidth;
} codertarget_arduinobase_int_i_T;

#endif                                 /*typedef_codertarget_arduinobase_int_i_T*/

#ifndef typedef_codertarget_arduinobase_block_T
#define typedef_codertarget_arduinobase_block_T

typedef struct {
  int32_T isInitialized;
  real_T SampleTime;
} codertarget_arduinobase_block_T;

#endif                                 /*typedef_codertarget_arduinobase_block_T*/

#ifndef typedef_cell_wrap_Valvula_arduino_T
#define typedef_cell_wrap_Valvula_arduino_T

typedef struct {
  uint32_T f1[8];
} cell_wrap_Valvula_arduino_T;

#endif                                 /*typedef_cell_wrap_Valvula_arduino_T*/

#ifndef typedef_codertarget_arduinobase_in_ij_T
#define typedef_codertarget_arduinobase_in_ij_T

typedef struct {
  int32_T isInitialized;
  cell_wrap_Valvula_arduino_T inputVarSize;
  real_T port;
  real_T dataSizeInBytes;
  real_T dataType;
  real_T sendModeEnum;
  real_T sendFormatEnum;
} codertarget_arduinobase_in_ij_T;

#endif                                 /*typedef_codertarget_arduinobase_in_ij_T*/

/* Parameters (auto storage) */
typedef struct P_Valvula_arduino_T_ P_Valvula_arduino_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_Valvula_arduino_T RT_MODEL_Valvula_arduino_T;

#endif                                 /* RTW_HEADER_Valvula_arduino_types_h_ */
