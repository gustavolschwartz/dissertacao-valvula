function [dx, y] = throttleODE(t, x, F, c, k, b, varargin)
% ODE function for throttle body dynamics.
% Represent equation of motion by a set of first order equations (state-space) 
%
%	dx: state derivatives at time t
%	y: output at time t
%	
%	t: time value (scalar)
%	x: state vector at time t
%	F: input (step command) at time t
%	c, k, K, b: parameters to be estimated

%   Copyright 2009-2010 The MathWorks, Inc.

G1 = max(0,x(1));
G2 = min(70,G1); % nonlinear displacement value

% State equations
dx(1) = x(2);
dx(2) = b*F - c*x(2) - k*x(1);
% Output equation
y(1)= G2;
end

