/*
 * File: untitled_data.c
 *
 * Code generated for Simulink model 'untitled'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Thu Jul  9 10:35:46 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "untitled.h"
#include "untitled_private.h"

/* Block parameters (auto storage) */
P_untitled_T untitled_P = {
  /* Mask Parameter: DiscreteDerivative_ICPrevScaled
   * Referenced by: '<S10>/UD'
   */
  0.0,

  /* Mask Parameter: CompareToConstant1_const
   * Referenced by: '<S14>/Constant'
   */
  98.0,

  /* Mask Parameter: CompareToConstant_const
   * Referenced by: '<S13>/Constant'
   */
  1.0,

  /* Mask Parameter: PWM_pinNumber
   * Referenced by: '<S5>/PWM'
   */
  3U,

  /* Mask Parameter: CompareToConstant2_const
   * Referenced by: '<S15>/Constant'
   */
  12845056,

  /* Expression: [1 1 1 1 1 1 1 1 1]*4
   * Referenced by: '<S3>/1-D Lookup Table3'
   */
  { 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0 },

  /* Expression: [-100 -10 -5 -1 0 1 5 10 100]
   * Referenced by: '<S3>/1-D Lookup Table3'
   */
  { -100.0, -10.0, -5.0, -1.0, 0.0, 1.0, 5.0, 10.0, 100.0 },

  /* Expression: 88
   * Referenced by: '<S3>/Bias'
   */
  88.0,

  /* Expression: 120
   * Referenced by: '<S9>/Constant'
   */
  120.0,

  /* Expression: 40
   * Referenced by: '<Root>/Sine Wave1'
   */
  40.0,

  /* Expression: 50
   * Referenced by: '<Root>/Sine Wave1'
   */
  50.0,

  /* Expression: 2*pi/10
   * Referenced by: '<Root>/Sine Wave1'
   */
  0.62831853071795862,

  /* Computed Parameter: SineWave1_Hsin
   * Referenced by: '<Root>/Sine Wave1'
   */
  0.0062831439655589511,

  /* Computed Parameter: SineWave1_HCos
   * Referenced by: '<Root>/Sine Wave1'
   */
  0.99998026085613712,

  /* Computed Parameter: SineWave1_PSin
   * Referenced by: '<Root>/Sine Wave1'
   */
  -0.0062831439655589511,

  /* Computed Parameter: SineWave1_PCos
   * Referenced by: '<Root>/Sine Wave1'
   */
  0.99998026085613712,

  /* Computed Parameter: DiscreteTimeIntegrator_gainval
   * Referenced by: '<S3>/Discrete-Time Integrator'
   */
  0.01,

  /* Expression: 0
   * Referenced by: '<S3>/Discrete-Time Integrator'
   */
  0.0,

  /* Expression: 100
   * Referenced by: '<S3>/Discrete-Time Integrator'
   */
  100.0,

  /* Expression: -100
   * Referenced by: '<S3>/Discrete-Time Integrator'
   */
  -100.0,

  /* Expression: [1 1 1 1 1 1 1 1 1]*0
   * Referenced by: '<S3>/1-D Lookup Table5'
   */
  { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },

  /* Expression: [-100 -10 -5 -1 0 1 5 10 100]
   * Referenced by: '<S3>/1-D Lookup Table5'
   */
  { -100.0, -10.0, -5.0, -1.0, 0.0, 1.0, 5.0, 10.0, 100.0 },

  /* Computed Parameter: TSamp_WtEt
   * Referenced by: '<S10>/TSamp'
   */
  100.0,

  /* Expression: [1 1 1 1 1 1 1]*8
   * Referenced by: '<S3>/1-D Lookup Table1'
   */
  { 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0 },

  /* Expression: [-10 -5 -1 0 1 5 10]
   * Referenced by: '<S3>/1-D Lookup Table1'
   */
  { -10.0, -5.0, -1.0, 0.0, 1.0, 5.0, 10.0 },

  /* Computed Parameter: Bias1_Bias
   * Referenced by: '<Root>/Bias1'
   */
  -1611,

  /* Computed Parameter: Gain3_Gain
   * Referenced by: '<Root>/Gain3'
   */
  16446,

  /* Computed Parameter: Saturation_UpperSat
   * Referenced by: '<Root>/Saturation'
   */
  255U,

  /* Computed Parameter: Saturation_LowerSat
   * Referenced by: '<Root>/Saturation'
   */
  0U,

  /* Computed Parameter: UnitDelay_InitialCondition
   * Referenced by: '<Root>/Unit Delay'
   */
  0
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
