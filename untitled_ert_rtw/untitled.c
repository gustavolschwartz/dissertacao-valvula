/*
 * File: untitled.c
 *
 * Code generated for Simulink model 'untitled'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Thu Jul  9 10:35:46 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "untitled.h"
#include "untitled_private.h"
#include "untitled_dt.h"
#define untitled_RegisterAddress_      (12.0)
#define untitled_SlaveAddress_         (54.0)

/* Block signals (auto storage) */
B_untitled_T untitled_B;

/* Block states (auto storage) */
DW_untitled_T untitled_DW;

/* Real-time model */
RT_MODEL_untitled_T untitled_M_;
RT_MODEL_untitled_T *const untitled_M = &untitled_M_;
real_T look1_binlxpw(real_T u0, const real_T bp0[], const real_T table[],
                     uint32_T maxIndex)
{
  real_T frac;
  uint32_T iRght;
  uint32_T iLeft;
  uint32_T bpIdx;

  /* Lookup 1-D
     Search method: 'binary'
     Use previous index: 'off'
     Interpolation method: 'Linear'
     Extrapolation method: 'Linear'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  /* Prelookup - Index and Fraction
     Index Search method: 'binary'
     Extrapolation method: 'Linear'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  if (u0 <= bp0[0UL]) {
    iLeft = 0UL;
    frac = (u0 - bp0[0UL]) / (bp0[1UL] - bp0[0UL]);
  } else if (u0 < bp0[maxIndex]) {
    /* Binary Search */
    bpIdx = maxIndex >> 1UL;
    iLeft = 0UL;
    iRght = maxIndex;
    while (iRght - iLeft > 1UL) {
      if (u0 < bp0[bpIdx]) {
        iRght = bpIdx;
      } else {
        iLeft = bpIdx;
      }

      bpIdx = (iRght + iLeft) >> 1UL;
    }

    frac = (u0 - bp0[iLeft]) / (bp0[iLeft + 1UL] - bp0[iLeft]);
  } else {
    iLeft = maxIndex - 1UL;
    frac = (u0 - bp0[maxIndex - 1UL]) / (bp0[maxIndex] - bp0[maxIndex - 1UL]);
  }

  /* Interpolation 1-D
     Interpolation method: 'Linear'
     Use last breakpoint for index at or above upper limit: 'off'
     Overflow mode: 'portable wrapping'
   */
  return (table[iLeft + 1UL] - table[iLeft]) * frac + table[iLeft];
}

real_T look1_binlcpw(real_T u0, const real_T bp0[], const real_T table[],
                     uint32_T maxIndex)
{
  real_T frac;
  uint32_T iRght;
  uint32_T iLeft;
  uint32_T bpIdx;

  /* Lookup 1-D
     Search method: 'binary'
     Use previous index: 'off'
     Interpolation method: 'Linear'
     Extrapolation method: 'Clip'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  /* Prelookup - Index and Fraction
     Index Search method: 'binary'
     Extrapolation method: 'Clip'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  if (u0 <= bp0[0UL]) {
    iLeft = 0UL;
    frac = 0.0;
  } else if (u0 < bp0[maxIndex]) {
    /* Binary Search */
    bpIdx = maxIndex >> 1UL;
    iLeft = 0UL;
    iRght = maxIndex;
    while (iRght - iLeft > 1UL) {
      if (u0 < bp0[bpIdx]) {
        iRght = bpIdx;
      } else {
        iLeft = bpIdx;
      }

      bpIdx = (iRght + iLeft) >> 1UL;
    }

    frac = (u0 - bp0[iLeft]) / (bp0[iLeft + 1UL] - bp0[iLeft]);
  } else {
    iLeft = maxIndex - 1UL;
    frac = 1.0;
  }

  /* Interpolation 1-D
     Interpolation method: 'Linear'
     Use last breakpoint for index at or above upper limit: 'off'
     Overflow mode: 'portable wrapping'
   */
  return (table[iLeft + 1UL] - table[iLeft]) * frac + table[iLeft];
}

/* Model step function */
void untitled_step(void)
{
  uint8_T rawData[2];
  boolean_T rtb_Compare_b;
  real_T rtb_TSamp;
  boolean_T rtb_LogicalOperator;
  real_T rtb_In;
  uint16_T q0;
  uint16_T qY;
  real_T u0;
  uint8_T tmp;
  real_T Add_tmp_tmp;

  /* MATLABSystem: '<Root>/I2C LSB1' */
  MW_i2cReadLoop((uint8_T)untitled_SlaveAddress_, 1.0, untitled_RegisterAddress_,
                 2U, rawData);
  q0 = (uint16_T)rawData[0] << 8U;
  qY = q0 + /*MW:OvSatOk*/ rawData[1];
  if (qY < q0) {
    qY = MAX_uint16_T;
  }

  /* Gain: '<Root>/Gain3' incorporates:
   *  Bias: '<Root>/Bias1'
   *  DataTypeConversion: '<Root>/Data Type Conversion3'
   *  MATLABSystem: '<Root>/I2C LSB1'
   */
  untitled_B.Gain3 = (int32_T)((int16_T)qY + untitled_P.Bias1_Bias) *
    untitled_P.Gain3_Gain;

  /* Sin: '<Root>/Sine Wave1' */
  if (untitled_DW.systemEnable != 0L) {
    rtb_In = untitled_P.SineWave1_Freq * untitled_M->Timing.taskTime0;
    untitled_DW.lastSin = sin(rtb_In);
    untitled_DW.lastCos = cos(rtb_In);
    untitled_DW.systemEnable = 0L;
  }

  rtb_In = ((untitled_DW.lastSin * untitled_P.SineWave1_PCos +
             untitled_DW.lastCos * untitled_P.SineWave1_PSin) *
            untitled_P.SineWave1_HCos + (untitled_DW.lastCos *
             untitled_P.SineWave1_PCos - untitled_DW.lastSin *
             untitled_P.SineWave1_PSin) * untitled_P.SineWave1_Hsin) *
    untitled_P.SineWave1_Amp + untitled_P.SineWave1_Bias;

  /* End of Sin: '<Root>/Sine Wave1' */

  /* RateTransition: '<Root>/Rate Transition' */
  untitled_B.RateTransition = rtb_In;

  /* Sum: '<Root>/Add' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion6'
   *  Sum: '<S3>/Add'
   */
  Add_tmp_tmp = (real_T)untitled_B.Gain3 * 7.62939453125E-6;
  rtb_In = untitled_B.RateTransition - Add_tmp_tmp;
  untitled_B.Add = rtb_In;

  /* Logic: '<S9>/Logical Operator' incorporates:
   *  Constant: '<S14>/Constant'
   *  Constant: '<S15>/Constant'
   *  RelationalOperator: '<S14>/Compare'
   *  RelationalOperator: '<S15>/Compare'
   */
  rtb_LogicalOperator = ((untitled_B.RateTransition >=
    untitled_P.CompareToConstant1_const) && (untitled_B.Gain3 >=
    untitled_P.CompareToConstant2_const));

  /* SampleTimeMath: '<S10>/TSamp' incorporates:
   *  Lookup_n-D: '<S3>/1-D Lookup Table5'
   *  Product: '<S3>/Product2'
   *
   * About '<S10>/TSamp':
   *  y = u * K where K = 1 / ( w * Ts )
   */
  rtb_TSamp = rtb_In * look1_binlxpw(rtb_In, untitled_P.uDLookupTable5_bp01Data,
    untitled_P.uDLookupTable5_tableData, 8UL) * untitled_P.TSamp_WtEt;

  /* RelationalOperator: '<S13>/Compare' incorporates:
   *  Constant: '<S13>/Constant'
   */
  rtb_Compare_b = (untitled_B.RateTransition >=
                   untitled_P.CompareToConstant_const);

  /* Switch: '<S9>/Switch' incorporates:
   *  Constant: '<S9>/Constant'
   *  Product: '<S9>/Product'
   *  Saturate: '<Root>/Saturation'
   */
  if (rtb_LogicalOperator) {
    untitled_B.Switch = untitled_P.Constant_Value;
  } else {
    /* Bias: '<S3>/Bias' incorporates:
     *  DiscreteIntegrator: '<S3>/Discrete-Time Integrator'
     *  Lookup_n-D: '<S3>/1-D Lookup Table3'
     *  Product: '<S3>/Product1'
     *  Sum: '<S10>/Diff'
     *  Sum: '<S3>/Sum'
     *  UnitDelay: '<S10>/UD'
     *
     * Block description for '<S10>/Diff':
     *
     *  Add in CPU
     *
     * Block description for '<S10>/UD':
     *
     *  Store in Global RAM
     */
    u0 = ((look1_binlxpw(rtb_In, untitled_P.uDLookupTable3_bp01Data,
                         untitled_P.uDLookupTable3_tableData, 8UL) * rtb_In +
           untitled_DW.DiscreteTimeIntegrator_DSTATE) + (rtb_TSamp -
           untitled_DW.UD_DSTATE)) + untitled_P.Bias_Bias;

    /* Saturate: '<Root>/Saturation' */
    if (u0 > untitled_P.Saturation_UpperSat) {
      u0 = untitled_P.Saturation_UpperSat;
    } else {
      if (u0 < untitled_P.Saturation_LowerSat) {
        u0 = untitled_P.Saturation_LowerSat;
      }
    }

    u0 = floor(u0);
    if (rtIsNaN(u0)) {
      u0 = 0.0;
    } else {
      u0 = fmod(u0, 256.0);
    }

    untitled_B.Switch = rtb_Compare_b ? (int16_T)(uint8_T)u0 : 0;
  }

  /* End of Switch: '<S9>/Switch' */

  /* DataTypeConversion: '<S5>/Data Type Conversion' */
  if (untitled_B.Switch < 256.0) {
    if (untitled_B.Switch >= 0.0) {
      tmp = (uint8_T)untitled_B.Switch;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint8_T;
  }

  /* End of DataTypeConversion: '<S5>/Data Type Conversion' */

  /* S-Function (arduinoanalogoutput_sfcn): '<S5>/PWM' */
  MW_analogWrite(untitled_P.PWM_pinNumber, tmp);

  /* DataTypeConversion: '<Root>/Data Type Conversion5' */
  untitled_B.SP = untitled_B.RateTransition;

  /* DataTypeConversion: '<Root>/Data Type Conversion6' */
  untitled_B.VP = Add_tmp_tmp;

  /* DataTypeConversion: '<Root>/Data Type Conversion4' */
  untitled_B.DataTypeConversion4 = untitled_B.Switch;

  /* DigitalClock: '<Root>/Digital Clock1' */
  untitled_B.t = untitled_M->Timing.taskTime0;

  /* SignalConversion: '<Root>/TmpSignal ConversionAtTo Workspace1Inport1' */
  untitled_B.TmpSignalConversionAtToWorkspac[0] = untitled_B.SP;
  untitled_B.TmpSignalConversionAtToWorkspac[1] = untitled_B.VP;
  untitled_B.TmpSignalConversionAtToWorkspac[2] = untitled_B.DataTypeConversion4;
  untitled_B.TmpSignalConversionAtToWorkspac[3] = untitled_B.t;

  /* Update for Sin: '<Root>/Sine Wave1' */
  Add_tmp_tmp = untitled_DW.lastSin;
  untitled_DW.lastSin = untitled_DW.lastSin * untitled_P.SineWave1_HCos +
    untitled_DW.lastCos * untitled_P.SineWave1_Hsin;
  untitled_DW.lastCos = untitled_DW.lastCos * untitled_P.SineWave1_HCos -
    Add_tmp_tmp * untitled_P.SineWave1_Hsin;

  /* Update for DiscreteIntegrator: '<S3>/Discrete-Time Integrator' incorporates:
   *  Logic: '<S3>/Logical Operator'
   *  Lookup_n-D: '<S3>/1-D Lookup Table1'
   *  Product: '<S3>/Product'
   *  UnitDelay: '<Root>/Unit Delay'
   */
  untitled_DW.DiscreteTimeIntegrator_DSTATE += look1_binlcpw(rtb_In,
    untitled_P.uDLookupTable1_bp01Data, untitled_P.uDLookupTable1_tableData, 6UL)
    * (real_T)!untitled_DW.UnitDelay_DSTATE * rtb_In *
    untitled_P.DiscreteTimeIntegrator_gainval;
  if (untitled_DW.DiscreteTimeIntegrator_DSTATE >=
      untitled_P.DiscreteTimeIntegrator_UpperSat) {
    untitled_DW.DiscreteTimeIntegrator_DSTATE =
      untitled_P.DiscreteTimeIntegrator_UpperSat;
  } else {
    if (untitled_DW.DiscreteTimeIntegrator_DSTATE <=
        untitled_P.DiscreteTimeIntegrator_LowerSat) {
      untitled_DW.DiscreteTimeIntegrator_DSTATE =
        untitled_P.DiscreteTimeIntegrator_LowerSat;
    }
  }

  /* End of Update for DiscreteIntegrator: '<S3>/Discrete-Time Integrator' */

  /* Update for UnitDelay: '<S10>/UD'
   *
   * Block description for '<S10>/UD':
   *
   *  Store in Global RAM
   */
  untitled_DW.UD_DSTATE = rtb_TSamp;

  /* Update for UnitDelay: '<Root>/Unit Delay' incorporates:
   *  Logic: '<S9>/Logical Operator1'
   *  Logic: '<S9>/Logical Operator2'
   */
  untitled_DW.UnitDelay_DSTATE = ((!rtb_Compare_b) || rtb_LogicalOperator);

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.01s, 0.0s] */
    rtExtModeUpload(0, untitled_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.01s, 0.0s] */
    if ((rtmGetTFinal(untitled_M)!=-1) &&
        !((rtmGetTFinal(untitled_M)-untitled_M->Timing.taskTime0) >
          untitled_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(untitled_M, "Simulation finished");
    }

    if (rtmGetStopRequested(untitled_M)) {
      rtmSetErrorStatus(untitled_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  untitled_M->Timing.taskTime0 =
    (++untitled_M->Timing.clockTick0) * untitled_M->Timing.stepSize0;
}

/* Model initialize function */
void untitled_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)untitled_M, 0,
                sizeof(RT_MODEL_untitled_T));
  rtmSetTFinal(untitled_M, 10.0);
  untitled_M->Timing.stepSize0 = 0.01;

  /* External mode info */
  untitled_M->Sizes.checksums[0] = (3304459623U);
  untitled_M->Sizes.checksums[1] = (1322369838U);
  untitled_M->Sizes.checksums[2] = (1588098930U);
  untitled_M->Sizes.checksums[3] = (1678669447U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[4];
    untitled_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(untitled_M->extModeInfo,
      &untitled_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(untitled_M->extModeInfo, untitled_M->Sizes.checksums);
    rteiSetTPtr(untitled_M->extModeInfo, rtmGetTPtr(untitled_M));
  }

  /* block I/O */
  (void) memset(((void *) &untitled_B), 0,
                sizeof(B_untitled_T));

  /* states (dwork) */
  (void) memset((void *)&untitled_DW, 0,
                sizeof(DW_untitled_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    untitled_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 17;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Start for MATLABSystem: '<Root>/I2C LSB1' */
  untitled_DW.obj.isInitialized = 0L;
  untitled_DW.obj.isInitialized = 1L;

  /* Start for S-Function (arduinoanalogoutput_sfcn): '<S5>/PWM' */
  MW_pinModeOutput(untitled_P.PWM_pinNumber);

  /* InitializeConditions for DiscreteIntegrator: '<S3>/Discrete-Time Integrator' */
  untitled_DW.DiscreteTimeIntegrator_DSTATE =
    untitled_P.DiscreteTimeIntegrator_IC;

  /* InitializeConditions for UnitDelay: '<S10>/UD'
   *
   * Block description for '<S10>/UD':
   *
   *  Store in Global RAM
   */
  untitled_DW.UD_DSTATE = untitled_P.DiscreteDerivative_ICPrevScaled;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  untitled_DW.UnitDelay_DSTATE = untitled_P.UnitDelay_InitialCondition;

  /* Enable for Sin: '<Root>/Sine Wave1' */
  untitled_DW.systemEnable = 1L;
}

/* Model terminate function */
void untitled_terminate(void)
{
  /* Terminate for MATLABSystem: '<Root>/I2C LSB1' */
  if (untitled_DW.obj.isInitialized == 1L) {
    untitled_DW.obj.isInitialized = 2L;
  }

  /* End of Terminate for MATLABSystem: '<Root>/I2C LSB1' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
