function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "untitled"};
	this.sidHashMap["untitled"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>"] = {sid: "untitled:17"};
	this.sidHashMap["untitled:17"] = {rtwname: "<S1>"};
	this.rtwnameHashMap["<S2>"] = {sid: "untitled:28"};
	this.sidHashMap["untitled:28"] = {rtwname: "<S2>"};
	this.rtwnameHashMap["<S3>"] = {sid: "untitled:58"};
	this.sidHashMap["untitled:58"] = {rtwname: "<S3>"};
	this.rtwnameHashMap["<S4>"] = {sid: "untitled:78"};
	this.sidHashMap["untitled:78"] = {rtwname: "<S4>"};
	this.rtwnameHashMap["<S5>"] = {sid: "untitled:79"};
	this.sidHashMap["untitled:79"] = {rtwname: "<S5>"};
	this.rtwnameHashMap["<S6>"] = {sid: "untitled:80"};
	this.sidHashMap["untitled:80"] = {rtwname: "<S6>"};
	this.rtwnameHashMap["<S7>"] = {sid: "untitled:83"};
	this.sidHashMap["untitled:83"] = {rtwname: "<S7>"};
	this.rtwnameHashMap["<S8>"] = {sid: "untitled:84"};
	this.sidHashMap["untitled:84"] = {rtwname: "<S8>"};
	this.rtwnameHashMap["<S9>"] = {sid: "untitled:92"};
	this.sidHashMap["untitled:92"] = {rtwname: "<S9>"};
	this.rtwnameHashMap["<S10>"] = {sid: "untitled:69"};
	this.sidHashMap["untitled:69"] = {rtwname: "<S10>"};
	this.rtwnameHashMap["<S11>"] = {sid: "untitled:83:2"};
	this.sidHashMap["untitled:83:2"] = {rtwname: "<S11>"};
	this.rtwnameHashMap["<S12>"] = {sid: "untitled:84:2"};
	this.sidHashMap["untitled:84:2"] = {rtwname: "<S12>"};
	this.rtwnameHashMap["<S13>"] = {sid: "untitled:96"};
	this.sidHashMap["untitled:96"] = {rtwname: "<S13>"};
	this.rtwnameHashMap["<S14>"] = {sid: "untitled:97"};
	this.sidHashMap["untitled:97"] = {rtwname: "<S14>"};
	this.rtwnameHashMap["<S15>"] = {sid: "untitled:98"};
	this.sidHashMap["untitled:98"] = {rtwname: "<S15>"};
	this.rtwnameHashMap["<Root>/Add"] = {sid: "untitled:1"};
	this.sidHashMap["untitled:1"] = {rtwname: "<Root>/Add"};
	this.rtwnameHashMap["<Root>/Add3"] = {sid: "untitled:2"};
	this.sidHashMap["untitled:2"] = {rtwname: "<Root>/Add3"};
	this.rtwnameHashMap["<Root>/Bias"] = {sid: "untitled:3"};
	this.sidHashMap["untitled:3"] = {rtwname: "<Root>/Bias"};
	this.rtwnameHashMap["<Root>/Bias1"] = {sid: "untitled:4"};
	this.sidHashMap["untitled:4"] = {rtwname: "<Root>/Bias1"};
	this.rtwnameHashMap["<Root>/Bias3"] = {sid: "untitled:5"};
	this.sidHashMap["untitled:5"] = {rtwname: "<Root>/Bias3"};
	this.rtwnameHashMap["<Root>/Bias4"] = {sid: "untitled:6"};
	this.sidHashMap["untitled:6"] = {rtwname: "<Root>/Bias4"};
	this.rtwnameHashMap["<Root>/Constant1"] = {sid: "untitled:7"};
	this.sidHashMap["untitled:7"] = {rtwname: "<Root>/Constant1"};
	this.rtwnameHashMap["<Root>/Data Type Conversion"] = {sid: "untitled:8"};
	this.sidHashMap["untitled:8"] = {rtwname: "<Root>/Data Type Conversion"};
	this.rtwnameHashMap["<Root>/Data Type Conversion1"] = {sid: "untitled:9"};
	this.sidHashMap["untitled:9"] = {rtwname: "<Root>/Data Type Conversion1"};
	this.rtwnameHashMap["<Root>/Data Type Conversion2"] = {sid: "untitled:10"};
	this.sidHashMap["untitled:10"] = {rtwname: "<Root>/Data Type Conversion2"};
	this.rtwnameHashMap["<Root>/Data Type Conversion3"] = {sid: "untitled:11"};
	this.sidHashMap["untitled:11"] = {rtwname: "<Root>/Data Type Conversion3"};
	this.rtwnameHashMap["<Root>/Data Type Conversion4"] = {sid: "untitled:12"};
	this.sidHashMap["untitled:12"] = {rtwname: "<Root>/Data Type Conversion4"};
	this.rtwnameHashMap["<Root>/Data Type Conversion5"] = {sid: "untitled:13"};
	this.sidHashMap["untitled:13"] = {rtwname: "<Root>/Data Type Conversion5"};
	this.rtwnameHashMap["<Root>/Data Type Conversion6"] = {sid: "untitled:14"};
	this.sidHashMap["untitled:14"] = {rtwname: "<Root>/Data Type Conversion6"};
	this.rtwnameHashMap["<Root>/Digital Clock"] = {sid: "untitled:15"};
	this.sidHashMap["untitled:15"] = {rtwname: "<Root>/Digital Clock"};
	this.rtwnameHashMap["<Root>/Digital Clock1"] = {sid: "untitled:16"};
	this.sidHashMap["untitled:16"] = {rtwname: "<Root>/Digital Clock1"};
	this.rtwnameHashMap["<Root>/Discrete Derivative"] = {sid: "untitled:17"};
	this.sidHashMap["untitled:17"] = {rtwname: "<Root>/Discrete Derivative"};
	this.rtwnameHashMap["<Root>/Display"] = {sid: "untitled:18"};
	this.sidHashMap["untitled:18"] = {rtwname: "<Root>/Display"};
	this.rtwnameHashMap["<Root>/Display8"] = {sid: "untitled:19"};
	this.sidHashMap["untitled:19"] = {rtwname: "<Root>/Display8"};
	this.rtwnameHashMap["<Root>/ERRO"] = {sid: "untitled:20"};
	this.sidHashMap["untitled:20"] = {rtwname: "<Root>/ERRO"};
	this.rtwnameHashMap["<Root>/From Workspace"] = {sid: "untitled:21"};
	this.sidHashMap["untitled:21"] = {rtwname: "<Root>/From Workspace"};
	this.rtwnameHashMap["<Root>/Gain1"] = {sid: "untitled:22"};
	this.sidHashMap["untitled:22"] = {rtwname: "<Root>/Gain1"};
	this.rtwnameHashMap["<Root>/Gain2"] = {sid: "untitled:23"};
	this.sidHashMap["untitled:23"] = {rtwname: "<Root>/Gain2"};
	this.rtwnameHashMap["<Root>/Gain3"] = {sid: "untitled:24"};
	this.sidHashMap["untitled:24"] = {rtwname: "<Root>/Gain3"};
	this.rtwnameHashMap["<Root>/Gain8"] = {sid: "untitled:25"};
	this.sidHashMap["untitled:25"] = {rtwname: "<Root>/Gain8"};
	this.rtwnameHashMap["<Root>/I2C LSB"] = {sid: "untitled:26"};
	this.sidHashMap["untitled:26"] = {rtwname: "<Root>/I2C LSB"};
	this.rtwnameHashMap["<Root>/I2C LSB1"] = {sid: "untitled:27"};
	this.sidHashMap["untitled:27"] = {rtwname: "<Root>/I2C LSB1"};
	this.rtwnameHashMap["<Root>/Modelo1"] = {sid: "untitled:28"};
	this.sidHashMap["untitled:28"] = {rtwname: "<Root>/Modelo1"};
	this.rtwnameHashMap["<Root>/Mux"] = {sid: "untitled:56"};
	this.sidHashMap["untitled:56"] = {rtwname: "<Root>/Mux"};
	this.rtwnameHashMap["<Root>/Mux1"] = {sid: "untitled:57"};
	this.sidHashMap["untitled:57"] = {rtwname: "<Root>/Mux1"};
	this.rtwnameHashMap["<Root>/PID"] = {sid: "untitled:58"};
	this.sidHashMap["untitled:58"] = {rtwname: "<Root>/PID"};
	this.rtwnameHashMap["<Root>/PWM"] = {sid: "untitled:77"};
	this.sidHashMap["untitled:77"] = {rtwname: "<Root>/PWM"};
	this.rtwnameHashMap["<Root>/PWM1"] = {sid: "untitled:78"};
	this.sidHashMap["untitled:78"] = {rtwname: "<Root>/PWM1"};
	this.rtwnameHashMap["<Root>/PWM2"] = {sid: "untitled:79"};
	this.sidHashMap["untitled:79"] = {rtwname: "<Root>/PWM2"};
	this.rtwnameHashMap["<Root>/PWM3"] = {sid: "untitled:80"};
	this.sidHashMap["untitled:80"] = {rtwname: "<Root>/PWM3"};
	this.rtwnameHashMap["<Root>/Rate Transition"] = {sid: "untitled:81"};
	this.sidHashMap["untitled:81"] = {rtwname: "<Root>/Rate Transition"};
	this.rtwnameHashMap["<Root>/Rate Transition1"] = {sid: "untitled:82"};
	this.sidHashMap["untitled:82"] = {rtwname: "<Root>/Rate Transition1"};
	this.rtwnameHashMap["<Root>/Repeating Sequence Stair1"] = {sid: "untitled:83"};
	this.sidHashMap["untitled:83"] = {rtwname: "<Root>/Repeating Sequence Stair1"};
	this.rtwnameHashMap["<Root>/Repeating Sequence Stair2"] = {sid: "untitled:84"};
	this.sidHashMap["untitled:84"] = {rtwname: "<Root>/Repeating Sequence Stair2"};
	this.rtwnameHashMap["<Root>/SP"] = {sid: "untitled:85"};
	this.sidHashMap["untitled:85"] = {rtwname: "<Root>/SP"};
	this.rtwnameHashMap["<Root>/Saturation"] = {sid: "untitled:86"};
	this.sidHashMap["untitled:86"] = {rtwname: "<Root>/Saturation"};
	this.rtwnameHashMap["<Root>/Saturation1"] = {sid: "untitled:87"};
	this.sidHashMap["untitled:87"] = {rtwname: "<Root>/Saturation1"};
	this.rtwnameHashMap["<Root>/Saturation3"] = {sid: "untitled:88"};
	this.sidHashMap["untitled:88"] = {rtwname: "<Root>/Saturation3"};
	this.rtwnameHashMap["<Root>/Sine Wave"] = {sid: "untitled:89"};
	this.sidHashMap["untitled:89"] = {rtwname: "<Root>/Sine Wave"};
	this.rtwnameHashMap["<Root>/Sine Wave1"] = {sid: "untitled:90"};
	this.sidHashMap["untitled:90"] = {rtwname: "<Root>/Sine Wave1"};
	this.rtwnameHashMap["<Root>/Step"] = {sid: "untitled:91"};
	this.sidHashMap["untitled:91"] = {rtwname: "<Root>/Step"};
	this.rtwnameHashMap["<Root>/Subsystem"] = {sid: "untitled:92"};
	this.sidHashMap["untitled:92"] = {rtwname: "<Root>/Subsystem"};
	this.rtwnameHashMap["<Root>/To Workspace"] = {sid: "untitled:107"};
	this.sidHashMap["untitled:107"] = {rtwname: "<Root>/To Workspace"};
	this.rtwnameHashMap["<Root>/To Workspace1"] = {sid: "untitled:108"};
	this.sidHashMap["untitled:108"] = {rtwname: "<Root>/To Workspace1"};
	this.rtwnameHashMap["<Root>/Unit Delay"] = {sid: "untitled:109"};
	this.sidHashMap["untitled:109"] = {rtwname: "<Root>/Unit Delay"};
	this.rtwnameHashMap["<Root>/VP"] = {sid: "untitled:110"};
	this.sidHashMap["untitled:110"] = {rtwname: "<Root>/VP"};
	this.rtwnameHashMap["<S1>/U"] = {sid: "untitled:17:1"};
	this.sidHashMap["untitled:17:1"] = {rtwname: "<S1>/U"};
	this.rtwnameHashMap["<S1>/Data Type Duplicate"] = {sid: "untitled:17:2"};
	this.sidHashMap["untitled:17:2"] = {rtwname: "<S1>/Data Type Duplicate"};
	this.rtwnameHashMap["<S1>/Diff"] = {sid: "untitled:17:3"};
	this.sidHashMap["untitled:17:3"] = {rtwname: "<S1>/Diff"};
	this.rtwnameHashMap["<S1>/TSamp"] = {sid: "untitled:17:4"};
	this.sidHashMap["untitled:17:4"] = {rtwname: "<S1>/TSamp"};
	this.rtwnameHashMap["<S1>/UD"] = {sid: "untitled:17:5"};
	this.sidHashMap["untitled:17:5"] = {rtwname: "<S1>/UD"};
	this.rtwnameHashMap["<S1>/Y"] = {sid: "untitled:17:6"};
	this.sidHashMap["untitled:17:6"] = {rtwname: "<S1>/Y"};
	this.rtwnameHashMap["<S2>/T"] = {sid: "untitled:29"};
	this.sidHashMap["untitled:29"] = {rtwname: "<S2>/T"};
	this.rtwnameHashMap["<S2>/Add"] = {sid: "untitled:30"};
	this.sidHashMap["untitled:30"] = {rtwname: "<S2>/Add"};
	this.rtwnameHashMap["<S2>/Add1"] = {sid: "untitled:31"};
	this.sidHashMap["untitled:31"] = {rtwname: "<S2>/Add1"};
	this.rtwnameHashMap["<S2>/Constant"] = {sid: "untitled:32"};
	this.sidHashMap["untitled:32"] = {rtwname: "<S2>/Constant"};
	this.rtwnameHashMap["<S2>/Constant1"] = {sid: "untitled:33"};
	this.sidHashMap["untitled:33"] = {rtwname: "<S2>/Constant1"};
	this.rtwnameHashMap["<S2>/Constant2"] = {sid: "untitled:34"};
	this.sidHashMap["untitled:34"] = {rtwname: "<S2>/Constant2"};
	this.rtwnameHashMap["<S2>/Constant3"] = {sid: "untitled:35"};
	this.sidHashMap["untitled:35"] = {rtwname: "<S2>/Constant3"};
	this.rtwnameHashMap["<S2>/Damping"] = {sid: "untitled:36"};
	this.sidHashMap["untitled:36"] = {rtwname: "<S2>/Damping"};
	this.rtwnameHashMap["<S2>/Delay"] = {sid: "untitled:37"};
	this.sidHashMap["untitled:37"] = {rtwname: "<S2>/Delay"};
	this.rtwnameHashMap["<S2>/Delay1"] = {sid: "untitled:38"};
	this.sidHashMap["untitled:38"] = {rtwname: "<S2>/Delay1"};
	this.rtwnameHashMap["<S2>/Discrete-Time Integrator"] = {sid: "untitled:39"};
	this.sidHashMap["untitled:39"] = {rtwname: "<S2>/Discrete-Time Integrator"};
	this.rtwnameHashMap["<S2>/Discrete-Time Integrator1"] = {sid: "untitled:40"};
	this.sidHashMap["untitled:40"] = {rtwname: "<S2>/Discrete-Time Integrator1"};
	this.rtwnameHashMap["<S2>/Gain1"] = {sid: "untitled:41"};
	this.sidHashMap["untitled:41"] = {rtwname: "<S2>/Gain1"};
	this.rtwnameHashMap["<S2>/Inertia"] = {sid: "untitled:42"};
	this.sidHashMap["untitled:42"] = {rtwname: "<S2>/Inertia"};
	this.rtwnameHashMap["<S2>/Inertia1"] = {sid: "untitled:43"};
	this.sidHashMap["untitled:43"] = {rtwname: "<S2>/Inertia1"};
	this.rtwnameHashMap["<S2>/Max"] = {sid: "untitled:44"};
	this.sidHashMap["untitled:44"] = {rtwname: "<S2>/Max"};
	this.rtwnameHashMap["<S2>/Max1"] = {sid: "untitled:45"};
	this.sidHashMap["untitled:45"] = {rtwname: "<S2>/Max1"};
	this.rtwnameHashMap["<S2>/Max2"] = {sid: "untitled:46"};
	this.sidHashMap["untitled:46"] = {rtwname: "<S2>/Max2"};
	this.rtwnameHashMap["<S2>/Max3"] = {sid: "untitled:47"};
	this.sidHashMap["untitled:47"] = {rtwname: "<S2>/Max3"};
	this.rtwnameHashMap["<S2>/Saturation"] = {sid: "untitled:48"};
	this.sidHashMap["untitled:48"] = {rtwname: "<S2>/Saturation"};
	this.rtwnameHashMap["<S2>/Saturation4"] = {sid: "untitled:49"};
	this.sidHashMap["untitled:49"] = {rtwname: "<S2>/Saturation4"};
	this.rtwnameHashMap["<S2>/Saturation5"] = {sid: "untitled:50"};
	this.sidHashMap["untitled:50"] = {rtwname: "<S2>/Saturation5"};
	this.rtwnameHashMap["<S2>/Spring"] = {sid: "untitled:51"};
	this.sidHashMap["untitled:51"] = {rtwname: "<S2>/Spring"};
	this.rtwnameHashMap["<S2>/Sum2"] = {sid: "untitled:52"};
	this.sidHashMap["untitled:52"] = {rtwname: "<S2>/Sum2"};
	this.rtwnameHashMap["<S2>/Y"] = {sid: "untitled:53"};
	this.sidHashMap["untitled:53"] = {rtwname: "<S2>/Y"};
	this.rtwnameHashMap["<S3>/Signal"] = {sid: "untitled:59"};
	this.sidHashMap["untitled:59"] = {rtwname: "<S3>/Signal"};
	this.rtwnameHashMap["<S3>/Ref"] = {sid: "untitled:60"};
	this.sidHashMap["untitled:60"] = {rtwname: "<S3>/Ref"};
	this.rtwnameHashMap["<S3>/Rst"] = {sid: "untitled:61"};
	this.sidHashMap["untitled:61"] = {rtwname: "<S3>/Rst"};
	this.rtwnameHashMap["<S3>/1-D Lookup Table1"] = {sid: "untitled:62"};
	this.sidHashMap["untitled:62"] = {rtwname: "<S3>/1-D Lookup Table1"};
	this.rtwnameHashMap["<S3>/1-D Lookup Table2"] = {sid: "untitled:63"};
	this.sidHashMap["untitled:63"] = {rtwname: "<S3>/1-D Lookup Table2"};
	this.rtwnameHashMap["<S3>/1-D Lookup Table3"] = {sid: "untitled:64"};
	this.sidHashMap["untitled:64"] = {rtwname: "<S3>/1-D Lookup Table3"};
	this.rtwnameHashMap["<S3>/1-D Lookup Table4"] = {sid: "untitled:65"};
	this.sidHashMap["untitled:65"] = {rtwname: "<S3>/1-D Lookup Table4"};
	this.rtwnameHashMap["<S3>/1-D Lookup Table5"] = {sid: "untitled:66"};
	this.sidHashMap["untitled:66"] = {rtwname: "<S3>/1-D Lookup Table5"};
	this.rtwnameHashMap["<S3>/Add"] = {sid: "untitled:67"};
	this.sidHashMap["untitled:67"] = {rtwname: "<S3>/Add"};
	this.rtwnameHashMap["<S3>/Bias"] = {sid: "untitled:68"};
	this.sidHashMap["untitled:68"] = {rtwname: "<S3>/Bias"};
	this.rtwnameHashMap["<S3>/Discrete Derivative"] = {sid: "untitled:69"};
	this.sidHashMap["untitled:69"] = {rtwname: "<S3>/Discrete Derivative"};
	this.rtwnameHashMap["<S3>/Discrete-Time Integrator"] = {sid: "untitled:70"};
	this.sidHashMap["untitled:70"] = {rtwname: "<S3>/Discrete-Time Integrator"};
	this.rtwnameHashMap["<S3>/Logical Operator"] = {sid: "untitled:71"};
	this.sidHashMap["untitled:71"] = {rtwname: "<S3>/Logical Operator"};
	this.rtwnameHashMap["<S3>/Product"] = {sid: "untitled:72"};
	this.sidHashMap["untitled:72"] = {rtwname: "<S3>/Product"};
	this.rtwnameHashMap["<S3>/Product1"] = {sid: "untitled:73"};
	this.sidHashMap["untitled:73"] = {rtwname: "<S3>/Product1"};
	this.rtwnameHashMap["<S3>/Product2"] = {sid: "untitled:74"};
	this.sidHashMap["untitled:74"] = {rtwname: "<S3>/Product2"};
	this.rtwnameHashMap["<S3>/Sum"] = {sid: "untitled:75"};
	this.sidHashMap["untitled:75"] = {rtwname: "<S3>/Sum"};
	this.rtwnameHashMap["<S3>/Out1"] = {sid: "untitled:76"};
	this.sidHashMap["untitled:76"] = {rtwname: "<S3>/Out1"};
	this.rtwnameHashMap["<S4>/In1"] = {sid: "untitled:78:116"};
	this.sidHashMap["untitled:78:116"] = {rtwname: "<S4>/In1"};
	this.rtwnameHashMap["<S4>/Data Type Conversion"] = {sid: "untitled:78:114"};
	this.sidHashMap["untitled:78:114"] = {rtwname: "<S4>/Data Type Conversion"};
	this.rtwnameHashMap["<S4>/PWM"] = {sid: "untitled:78:215"};
	this.sidHashMap["untitled:78:215"] = {rtwname: "<S4>/PWM"};
	this.rtwnameHashMap["<S5>/In1"] = {sid: "untitled:79:116"};
	this.sidHashMap["untitled:79:116"] = {rtwname: "<S5>/In1"};
	this.rtwnameHashMap["<S5>/Data Type Conversion"] = {sid: "untitled:79:114"};
	this.sidHashMap["untitled:79:114"] = {rtwname: "<S5>/Data Type Conversion"};
	this.rtwnameHashMap["<S5>/PWM"] = {sid: "untitled:79:215"};
	this.sidHashMap["untitled:79:215"] = {rtwname: "<S5>/PWM"};
	this.rtwnameHashMap["<S6>/In1"] = {sid: "untitled:80:116"};
	this.sidHashMap["untitled:80:116"] = {rtwname: "<S6>/In1"};
	this.rtwnameHashMap["<S6>/Data Type Conversion"] = {sid: "untitled:80:114"};
	this.sidHashMap["untitled:80:114"] = {rtwname: "<S6>/Data Type Conversion"};
	this.rtwnameHashMap["<S6>/PWM"] = {sid: "untitled:80:215"};
	this.sidHashMap["untitled:80:215"] = {rtwname: "<S6>/PWM"};
	this.rtwnameHashMap["<S7>/Force to be scalar"] = {sid: "untitled:83:1"};
	this.sidHashMap["untitled:83:1"] = {rtwname: "<S7>/Force to be scalar"};
	this.rtwnameHashMap["<S7>/LimitedCounter"] = {sid: "untitled:83:2"};
	this.sidHashMap["untitled:83:2"] = {rtwname: "<S7>/LimitedCounter"};
	this.rtwnameHashMap["<S7>/Out"] = {sid: "untitled:83:3"};
	this.sidHashMap["untitled:83:3"] = {rtwname: "<S7>/Out"};
	this.rtwnameHashMap["<S7>/Output"] = {sid: "untitled:83:4"};
	this.sidHashMap["untitled:83:4"] = {rtwname: "<S7>/Output"};
	this.rtwnameHashMap["<S7>/Vector"] = {sid: "untitled:83:5"};
	this.sidHashMap["untitled:83:5"] = {rtwname: "<S7>/Vector"};
	this.rtwnameHashMap["<S7>/y"] = {sid: "untitled:83:6"};
	this.sidHashMap["untitled:83:6"] = {rtwname: "<S7>/y"};
	this.rtwnameHashMap["<S8>/Force to be scalar"] = {sid: "untitled:84:1"};
	this.sidHashMap["untitled:84:1"] = {rtwname: "<S8>/Force to be scalar"};
	this.rtwnameHashMap["<S8>/LimitedCounter"] = {sid: "untitled:84:2"};
	this.sidHashMap["untitled:84:2"] = {rtwname: "<S8>/LimitedCounter"};
	this.rtwnameHashMap["<S8>/Out"] = {sid: "untitled:84:3"};
	this.sidHashMap["untitled:84:3"] = {rtwname: "<S8>/Out"};
	this.rtwnameHashMap["<S8>/Output"] = {sid: "untitled:84:4"};
	this.sidHashMap["untitled:84:4"] = {rtwname: "<S8>/Output"};
	this.rtwnameHashMap["<S8>/Vector"] = {sid: "untitled:84:5"};
	this.sidHashMap["untitled:84:5"] = {rtwname: "<S8>/Vector"};
	this.rtwnameHashMap["<S8>/y"] = {sid: "untitled:84:6"};
	this.sidHashMap["untitled:84:6"] = {rtwname: "<S8>/y"};
	this.rtwnameHashMap["<S9>/VM"] = {sid: "untitled:93"};
	this.sidHashMap["untitled:93"] = {rtwname: "<S9>/VM"};
	this.rtwnameHashMap["<S9>/SP"] = {sid: "untitled:94"};
	this.sidHashMap["untitled:94"] = {rtwname: "<S9>/SP"};
	this.rtwnameHashMap["<S9>/VP"] = {sid: "untitled:95"};
	this.sidHashMap["untitled:95"] = {rtwname: "<S9>/VP"};
	this.rtwnameHashMap["<S9>/Compare To Constant"] = {sid: "untitled:96"};
	this.sidHashMap["untitled:96"] = {rtwname: "<S9>/Compare To Constant"};
	this.rtwnameHashMap["<S9>/Compare To Constant1"] = {sid: "untitled:97"};
	this.sidHashMap["untitled:97"] = {rtwname: "<S9>/Compare To Constant1"};
	this.rtwnameHashMap["<S9>/Compare To Constant2"] = {sid: "untitled:98"};
	this.sidHashMap["untitled:98"] = {rtwname: "<S9>/Compare To Constant2"};
	this.rtwnameHashMap["<S9>/Constant"] = {sid: "untitled:99"};
	this.sidHashMap["untitled:99"] = {rtwname: "<S9>/Constant"};
	this.rtwnameHashMap["<S9>/Logical Operator"] = {sid: "untitled:100"};
	this.sidHashMap["untitled:100"] = {rtwname: "<S9>/Logical Operator"};
	this.rtwnameHashMap["<S9>/Logical Operator1"] = {sid: "untitled:101"};
	this.sidHashMap["untitled:101"] = {rtwname: "<S9>/Logical Operator1"};
	this.rtwnameHashMap["<S9>/Logical Operator2"] = {sid: "untitled:102"};
	this.sidHashMap["untitled:102"] = {rtwname: "<S9>/Logical Operator2"};
	this.rtwnameHashMap["<S9>/Product"] = {sid: "untitled:103"};
	this.sidHashMap["untitled:103"] = {rtwname: "<S9>/Product"};
	this.rtwnameHashMap["<S9>/Switch"] = {sid: "untitled:104"};
	this.sidHashMap["untitled:104"] = {rtwname: "<S9>/Switch"};
	this.rtwnameHashMap["<S9>/Out1"] = {sid: "untitled:105"};
	this.sidHashMap["untitled:105"] = {rtwname: "<S9>/Out1"};
	this.rtwnameHashMap["<S9>/Rst"] = {sid: "untitled:106"};
	this.sidHashMap["untitled:106"] = {rtwname: "<S9>/Rst"};
	this.rtwnameHashMap["<S10>/U"] = {sid: "untitled:69:1"};
	this.sidHashMap["untitled:69:1"] = {rtwname: "<S10>/U"};
	this.rtwnameHashMap["<S10>/Data Type Duplicate"] = {sid: "untitled:69:2"};
	this.sidHashMap["untitled:69:2"] = {rtwname: "<S10>/Data Type Duplicate"};
	this.rtwnameHashMap["<S10>/Diff"] = {sid: "untitled:69:3"};
	this.sidHashMap["untitled:69:3"] = {rtwname: "<S10>/Diff"};
	this.rtwnameHashMap["<S10>/TSamp"] = {sid: "untitled:69:4"};
	this.sidHashMap["untitled:69:4"] = {rtwname: "<S10>/TSamp"};
	this.rtwnameHashMap["<S10>/UD"] = {sid: "untitled:69:5"};
	this.sidHashMap["untitled:69:5"] = {rtwname: "<S10>/UD"};
	this.rtwnameHashMap["<S10>/Y"] = {sid: "untitled:69:6"};
	this.sidHashMap["untitled:69:6"] = {rtwname: "<S10>/Y"};
	this.rtwnameHashMap["<S11>/Data Type Propagation"] = {sid: "untitled:83:2:1"};
	this.sidHashMap["untitled:83:2:1"] = {rtwname: "<S11>/Data Type Propagation"};
	this.rtwnameHashMap["<S11>/Force to be scalar"] = {sid: "untitled:83:2:2"};
	this.sidHashMap["untitled:83:2:2"] = {rtwname: "<S11>/Force to be scalar"};
	this.rtwnameHashMap["<S11>/Increment Real World"] = {sid: "untitled:83:2:3"};
	this.sidHashMap["untitled:83:2:3"] = {rtwname: "<S11>/Increment Real World"};
	this.rtwnameHashMap["<S11>/Output"] = {sid: "untitled:83:2:4"};
	this.sidHashMap["untitled:83:2:4"] = {rtwname: "<S11>/Output"};
	this.rtwnameHashMap["<S11>/Wrap To Zero"] = {sid: "untitled:83:2:5"};
	this.sidHashMap["untitled:83:2:5"] = {rtwname: "<S11>/Wrap To Zero"};
	this.rtwnameHashMap["<S11>/y"] = {sid: "untitled:83:2:6"};
	this.sidHashMap["untitled:83:2:6"] = {rtwname: "<S11>/y"};
	this.rtwnameHashMap["<S12>/Data Type Propagation"] = {sid: "untitled:84:2:1"};
	this.sidHashMap["untitled:84:2:1"] = {rtwname: "<S12>/Data Type Propagation"};
	this.rtwnameHashMap["<S12>/Force to be scalar"] = {sid: "untitled:84:2:2"};
	this.sidHashMap["untitled:84:2:2"] = {rtwname: "<S12>/Force to be scalar"};
	this.rtwnameHashMap["<S12>/Increment Real World"] = {sid: "untitled:84:2:3"};
	this.sidHashMap["untitled:84:2:3"] = {rtwname: "<S12>/Increment Real World"};
	this.rtwnameHashMap["<S12>/Output"] = {sid: "untitled:84:2:4"};
	this.sidHashMap["untitled:84:2:4"] = {rtwname: "<S12>/Output"};
	this.rtwnameHashMap["<S12>/Wrap To Zero"] = {sid: "untitled:84:2:5"};
	this.sidHashMap["untitled:84:2:5"] = {rtwname: "<S12>/Wrap To Zero"};
	this.rtwnameHashMap["<S12>/y"] = {sid: "untitled:84:2:6"};
	this.sidHashMap["untitled:84:2:6"] = {rtwname: "<S12>/y"};
	this.rtwnameHashMap["<S13>/u"] = {sid: "untitled:96:1"};
	this.sidHashMap["untitled:96:1"] = {rtwname: "<S13>/u"};
	this.rtwnameHashMap["<S13>/Compare"] = {sid: "untitled:96:2"};
	this.sidHashMap["untitled:96:2"] = {rtwname: "<S13>/Compare"};
	this.rtwnameHashMap["<S13>/Constant"] = {sid: "untitled:96:3"};
	this.sidHashMap["untitled:96:3"] = {rtwname: "<S13>/Constant"};
	this.rtwnameHashMap["<S13>/y"] = {sid: "untitled:96:4"};
	this.sidHashMap["untitled:96:4"] = {rtwname: "<S13>/y"};
	this.rtwnameHashMap["<S14>/u"] = {sid: "untitled:97:1"};
	this.sidHashMap["untitled:97:1"] = {rtwname: "<S14>/u"};
	this.rtwnameHashMap["<S14>/Compare"] = {sid: "untitled:97:2"};
	this.sidHashMap["untitled:97:2"] = {rtwname: "<S14>/Compare"};
	this.rtwnameHashMap["<S14>/Constant"] = {sid: "untitled:97:3"};
	this.sidHashMap["untitled:97:3"] = {rtwname: "<S14>/Constant"};
	this.rtwnameHashMap["<S14>/y"] = {sid: "untitled:97:4"};
	this.sidHashMap["untitled:97:4"] = {rtwname: "<S14>/y"};
	this.rtwnameHashMap["<S15>/u"] = {sid: "untitled:98:1"};
	this.sidHashMap["untitled:98:1"] = {rtwname: "<S15>/u"};
	this.rtwnameHashMap["<S15>/Compare"] = {sid: "untitled:98:2"};
	this.sidHashMap["untitled:98:2"] = {rtwname: "<S15>/Compare"};
	this.rtwnameHashMap["<S15>/Constant"] = {sid: "untitled:98:3"};
	this.sidHashMap["untitled:98:3"] = {rtwname: "<S15>/Constant"};
	this.rtwnameHashMap["<S15>/y"] = {sid: "untitled:98:4"};
	this.sidHashMap["untitled:98:4"] = {rtwname: "<S15>/y"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
