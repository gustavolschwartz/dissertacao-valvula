/*
 * File: untitled_types.h
 *
 * Code generated for Simulink model 'untitled'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Thu Jul  9 10:35:46 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_untitled_types_h_
#define RTW_HEADER_untitled_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef struct_tag_sMXuVOqiR8VrXITXlaWuyyE
#define struct_tag_sMXuVOqiR8VrXITXlaWuyyE

struct tag_sMXuVOqiR8VrXITXlaWuyyE
{
  real_T uint8;
  real_T uint16;
};

#endif                                 /*struct_tag_sMXuVOqiR8VrXITXlaWuyyE*/

#ifndef typedef_sMXuVOqiR8VrXITXlaWuyyE_untit_T
#define typedef_sMXuVOqiR8VrXITXlaWuyyE_untit_T

typedef struct tag_sMXuVOqiR8VrXITXlaWuyyE sMXuVOqiR8VrXITXlaWuyyE_untit_T;

#endif                                 /*typedef_sMXuVOqiR8VrXITXlaWuyyE_untit_T*/

#ifndef typedef_codertarget_arduinobase_inter_T
#define typedef_codertarget_arduinobase_inter_T

typedef struct {
  int32_T isInitialized;
} codertarget_arduinobase_inter_T;

#endif                                 /*typedef_codertarget_arduinobase_inter_T*/

/* Parameters (auto storage) */
typedef struct P_untitled_T_ P_untitled_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_untitled_T RT_MODEL_untitled_T;

#endif                                 /* RTW_HEADER_untitled_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
