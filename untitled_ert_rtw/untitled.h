/*
 * File: untitled.h
 *
 * Code generated for Simulink model 'untitled'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Thu Jul  9 10:35:46 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_untitled_h_
#define RTW_HEADER_untitled_h_
#include <math.h>
#include <float.h>
#include <string.h>
#include <stddef.h>
#ifndef untitled_COMMON_INCLUDES_
# define untitled_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "MW_arduinoI2C.h"
#include "arduino_analogoutput_lct.h"
#endif                                 /* untitled_COMMON_INCLUDES_ */

#include "untitled_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "MW_target_hardware_resources.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T RateTransition;               /* '<Root>/Rate Transition' */
  real_T Add;                          /* '<Root>/Add' */
  real_T Switch;                       /* '<S9>/Switch' */
  real_T SP;                           /* '<Root>/Data Type Conversion5' */
  real_T VP;                           /* '<Root>/Data Type Conversion6' */
  real_T DataTypeConversion4;          /* '<Root>/Data Type Conversion4' */
  real_T t;                            /* '<Root>/Digital Clock1' */
  real_T TmpSignalConversionAtToWorkspac[4];
  int32_T Gain3;                       /* '<Root>/Gain3' */
} B_untitled_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T DiscreteTimeIntegrator_DSTATE;/* '<S3>/Discrete-Time Integrator' */
  real_T UD_DSTATE;                    /* '<S10>/UD' */
  real_T lastSin;                      /* '<Root>/Sine Wave1' */
  real_T lastCos;                      /* '<Root>/Sine Wave1' */
  struct {
    void *LoggedData;
  } ToWorkspace1_PWORK;                /* '<Root>/To Workspace1' */

  int32_T systemEnable;                /* '<Root>/Sine Wave1' */
  codertarget_arduinobase_inter_T obj; /* '<Root>/I2C LSB1' */
  boolean_T UnitDelay_DSTATE;          /* '<Root>/Unit Delay' */
} DW_untitled_T;

/* Parameters (auto storage) */
struct P_untitled_T_ {
  real_T DiscreteDerivative_ICPrevScaled;/* Mask Parameter: DiscreteDerivative_ICPrevScaled
                                          * Referenced by: '<S10>/UD'
                                          */
  real_T CompareToConstant1_const;     /* Mask Parameter: CompareToConstant1_const
                                        * Referenced by: '<S14>/Constant'
                                        */
  real_T CompareToConstant_const;      /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S13>/Constant'
                                        */
  uint32_T PWM_pinNumber;              /* Mask Parameter: PWM_pinNumber
                                        * Referenced by: '<S5>/PWM'
                                        */
  int32_T CompareToConstant2_const;    /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S15>/Constant'
                                        */
  real_T uDLookupTable3_tableData[9];  /* Expression: [1 1 1 1 1 1 1 1 1]*4
                                        * Referenced by: '<S3>/1-D Lookup Table3'
                                        */
  real_T uDLookupTable3_bp01Data[9];   /* Expression: [-100 -10 -5 -1 0 1 5 10 100]
                                        * Referenced by: '<S3>/1-D Lookup Table3'
                                        */
  real_T Bias_Bias;                    /* Expression: 88
                                        * Referenced by: '<S3>/Bias'
                                        */
  real_T Constant_Value;               /* Expression: 120
                                        * Referenced by: '<S9>/Constant'
                                        */
  real_T SineWave1_Amp;                /* Expression: 40
                                        * Referenced by: '<Root>/Sine Wave1'
                                        */
  real_T SineWave1_Bias;               /* Expression: 50
                                        * Referenced by: '<Root>/Sine Wave1'
                                        */
  real_T SineWave1_Freq;               /* Expression: 2*pi/10
                                        * Referenced by: '<Root>/Sine Wave1'
                                        */
  real_T SineWave1_Hsin;               /* Computed Parameter: SineWave1_Hsin
                                        * Referenced by: '<Root>/Sine Wave1'
                                        */
  real_T SineWave1_HCos;               /* Computed Parameter: SineWave1_HCos
                                        * Referenced by: '<Root>/Sine Wave1'
                                        */
  real_T SineWave1_PSin;               /* Computed Parameter: SineWave1_PSin
                                        * Referenced by: '<Root>/Sine Wave1'
                                        */
  real_T SineWave1_PCos;               /* Computed Parameter: SineWave1_PCos
                                        * Referenced by: '<Root>/Sine Wave1'
                                        */
  real_T DiscreteTimeIntegrator_gainval;/* Computed Parameter: DiscreteTimeIntegrator_gainval
                                         * Referenced by: '<S3>/Discrete-Time Integrator'
                                         */
  real_T DiscreteTimeIntegrator_IC;    /* Expression: 0
                                        * Referenced by: '<S3>/Discrete-Time Integrator'
                                        */
  real_T DiscreteTimeIntegrator_UpperSat;/* Expression: 100
                                          * Referenced by: '<S3>/Discrete-Time Integrator'
                                          */
  real_T DiscreteTimeIntegrator_LowerSat;/* Expression: -100
                                          * Referenced by: '<S3>/Discrete-Time Integrator'
                                          */
  real_T uDLookupTable5_tableData[9];  /* Expression: [1 1 1 1 1 1 1 1 1]*0
                                        * Referenced by: '<S3>/1-D Lookup Table5'
                                        */
  real_T uDLookupTable5_bp01Data[9];   /* Expression: [-100 -10 -5 -1 0 1 5 10 100]
                                        * Referenced by: '<S3>/1-D Lookup Table5'
                                        */
  real_T TSamp_WtEt;                   /* Computed Parameter: TSamp_WtEt
                                        * Referenced by: '<S10>/TSamp'
                                        */
  real_T uDLookupTable1_tableData[7];  /* Expression: [1 1 1 1 1 1 1]*8
                                        * Referenced by: '<S3>/1-D Lookup Table1'
                                        */
  real_T uDLookupTable1_bp01Data[7];   /* Expression: [-10 -5 -1 0 1 5 10]
                                        * Referenced by: '<S3>/1-D Lookup Table1'
                                        */
  int16_T Bias1_Bias;                  /* Computed Parameter: Bias1_Bias
                                        * Referenced by: '<Root>/Bias1'
                                        */
  int16_T Gain3_Gain;                  /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<Root>/Gain3'
                                        */
  uint8_T Saturation_UpperSat;         /* Computed Parameter: Saturation_UpperSat
                                        * Referenced by: '<Root>/Saturation'
                                        */
  uint8_T Saturation_LowerSat;         /* Computed Parameter: Saturation_LowerSat
                                        * Referenced by: '<Root>/Saturation'
                                        */
  boolean_T UnitDelay_InitialCondition;/* Computed Parameter: UnitDelay_InitialCondition
                                        * Referenced by: '<Root>/Unit Delay'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_untitled_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_untitled_T untitled_P;

/* Block signals (auto storage) */
extern B_untitled_T untitled_B;

/* Block states (auto storage) */
extern DW_untitled_T untitled_DW;

/* Model entry point functions */
extern void untitled_initialize(void);
extern void untitled_step(void);
extern void untitled_terminate(void);

/* Real-time Model object */
extern RT_MODEL_untitled_T *const untitled_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S10>/Data Type Duplicate' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'untitled'
 * '<S1>'   : 'untitled/Discrete Derivative'
 * '<S2>'   : 'untitled/Modelo1'
 * '<S3>'   : 'untitled/PID'
 * '<S4>'   : 'untitled/PWM1'
 * '<S5>'   : 'untitled/PWM2'
 * '<S6>'   : 'untitled/PWM3'
 * '<S7>'   : 'untitled/Repeating Sequence Stair1'
 * '<S8>'   : 'untitled/Repeating Sequence Stair2'
 * '<S9>'   : 'untitled/Subsystem'
 * '<S10>'  : 'untitled/PID/Discrete Derivative'
 * '<S11>'  : 'untitled/Repeating Sequence Stair1/LimitedCounter'
 * '<S12>'  : 'untitled/Repeating Sequence Stair2/LimitedCounter'
 * '<S13>'  : 'untitled/Subsystem/Compare To Constant'
 * '<S14>'  : 'untitled/Subsystem/Compare To Constant1'
 * '<S15>'  : 'untitled/Subsystem/Compare To Constant2'
 */
#endif                                 /* RTW_HEADER_untitled_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
