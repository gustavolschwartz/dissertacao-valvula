/*
 * untitled_dt.h
 *
 * Code generation for model "untitled".
 *
 * Model version              : 1.0
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Thu Jul  9 10:35:46 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(int16_T),
  sizeof(int32_T),
  sizeof(codertarget_arduinobase_inter_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "int16_T",
  "int32_T",
  "codertarget_arduinobase_inter_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&untitled_B.RateTransition), 0, 0, 11 },

  { (char_T *)(&untitled_B.Gain3), 15, 0, 1 }
  ,

  { (char_T *)(&untitled_DW.DiscreteTimeIntegrator_DSTATE), 0, 0, 4 },

  { (char_T *)(&untitled_DW.ToWorkspace1_PWORK.LoggedData), 11, 0, 1 },

  { (char_T *)(&untitled_DW.systemEnable), 6, 0, 1 },

  { (char_T *)(&untitled_DW.obj), 16, 0, 1 },

  { (char_T *)(&untitled_DW.UnitDelay_DSTATE), 8, 0, 1 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  7U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&untitled_P.DiscreteDerivative_ICPrevScaled), 0, 0, 3 },

  { (char_T *)(&untitled_P.PWM_pinNumber), 7, 0, 1 },

  { (char_T *)(&untitled_P.CompareToConstant2_const), 15, 0, 1 },

  { (char_T *)(&untitled_P.uDLookupTable3_tableData[0]), 0, 0, 64 },

  { (char_T *)(&untitled_P.Bias1_Bias), 4, 0, 2 },

  { (char_T *)(&untitled_P.Saturation_UpperSat), 3, 0, 2 },

  { (char_T *)(&untitled_P.UnitDelay_InitialCondition), 8, 0, 1 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  7U,
  rtPTransitions
};

/* [EOF] untitled_dt.h */
