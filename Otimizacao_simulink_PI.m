function [P,I,B] = runtracklsq
% RUNTRACKLSQ demonstrates using LSQNONLIN with Simulink.

% test                       % Load the model
% A10=0.810631075360283;
% A20=0;
% A30=0;
% C10=-0.000818919069153289;
% C20=-0.00255571191046379;
% C30=-0.0865664659504913;
% A10=evalin('base','A1');
% A20=evalin('base','A2');
% A30=evalin('base','A3');
% C10=evalin('base','C1');
% C20=evalin('base','C2');
% C30=evalin('base','C3');
% B=evalin('base','B');
par_model=evalin('base','par_model');
par0 = [5,8,1]; % Set initial values
Ts=0.01;
% M8=evalin('base','M8');
% m8_ss=evalin('base','m8_ss');
% X80=evalin('base','X80');

options = optimoptions(@lsqnonlin,'Algorithm','levenberg-marquardt',...
   'Display','iter','MaxIterations',150,'UseParallel',0);
% 'StepTolerance',1e-8,'OptimalityTolerance',1e-8, 'ScaleProblem', 'jacobian'
par = lsqnonlin(@tracklsq, par0, [], [], options);
P = par(1);
I = par(2);
B = par(3);

% figure, compare(iddata(M8.VP,M8.VM,Ts),iddata(evalin('base','Modelo(:,1)'),M8.VM,Ts))
% assignin('base','P8',P);
% assignin('base','I8',I);
% assignin('base','D8',D);
assignin('base','P',P);
assignin('base','I',I);
assignin('base','B',B);
% assignin('base','ts',ts);
% assignin('base','mp',mp);
    function F = tracklsq(par)
      % Track the output of optsim to a signal of 1
        
        P = par(1);
        I = par(2);
        B = par(3);

      % Set sim options and compute function value
      myobj = sim('test','SrcWorkspace','Current', ...
          'StopTime','36','SaveOutput','on','OutputSaveName','yout');
      F = myobj.get('yout');
%       F=F1{1}.Values.Data;
      
    end
end