#include <AS5600.h>

AS5600 encoder;
double vp,err,sp,output,sp_map,kp=3.4,bias=40.0;
double v[10], t, t0;
int u=0;
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(3,OUTPUT);
  pinMode(2,INPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  vp = encoder.getPosition(); // get the absolute position of the encoder
  vp=(vp-1536.0)*100.0/(2370.0-1536.0);
  vp=map(vp,-0.72,97.60,0.0,100);
    sp = pulseIn(2, HIGH);
  sp_map=sp*0.1267-0.0955;
  sp_map=map(sp_map,0,255.0,0,100.0);
  err=sp_map-vp;
  output=kp*err+bias;

  
//  Serial.print("VP: ");
//  Serial.println(vp);
//
//  
//  Serial.print("SP: ");
//  Serial.println(sp_map);
//  
//  Serial.print("Erro: ");
//  Serial.println(err);
//  
//  Serial.print("Saida: ");
//  Serial.println(output);
  
  if (output>=255) {output=255;}
  if (output<=0) {output=0;}
  analogWrite(3,output);
 
//   t=(millis()-t0);
//   t0=millis();
//  v[u]=t;
//  u=u+1;
//
//  if (u==9){
//    Serial.println(" Periodo ");
//    for (int i=1;i<9;i++){
//      Serial.println(v[i]);
//      }
//      u=0;
//    }
  
}
