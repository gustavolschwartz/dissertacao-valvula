%% Inicia cluster de processamento paralelo
%parpool

%% load dados_tratados_dois_estados_18_06_20.mat

% data1 = iddata(exp1.out, exp1.in, Ts, 'Tstart', -0.01, ...
%    'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');
% data2 = iddata(exp2.out, exp2.in, Ts, 'Tstart', -0.01, ...
%    'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');
% data3 = iddata(exp3.out, exp3.in, Ts, 'Tstart', -0.01, ...
%    'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');
% data4 = iddata(exp4.out, exp4.in, Ts, 'Tstart', -0.01, ...
%    'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');
% data5 = iddata(exp5.out, exp5.in, Ts, 'Tstart', -0.01, ...
%    'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');

data1 = iddata(x1(:,3), x1(:,2), Ts, 'Tstart', 0, ...
   'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');
data2 = iddata(x2(:,3), x2(:,2), Ts, 'Tstart', 0, ...
   'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');
data3 = iddata(x3(:,3), x3(:,2), Ts, 'Tstart', 0, ...
   'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');
data4 = iddata(x4(:,3), x4(:,2), Ts, 'Tstart', 0, ...
   'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');
data5 = iddata(x5(:,3), x5(:,2), Ts, 'Tstart', 0, ...
   'InputName', 'Step Command', 'OutputName', 'Valve Angle', 'OutputUnit', 'Deg.');

%%

MultiExpData = merge(data1, data2)

ValidationData =  merge(data3, data4, data5);


type throttleODE.m
%% 
% ODE file name:
ODEFile = 'throttleODE'; % file must exist in current working directory or be on MATLAB path

%%
% Dimensions [ny nu nx]
Orders = [1 1 2]; % we have 2 states: displacement x1(t) and velocity x2(t)

%% 
% Initial parameter values
% For our example, some values could come from a linear modeling exercise:
% estimating a black box linear model of second order would suggest values
% for c, k and b. The nonlinear spring constant could be a large value
% (some multiple of linear spring constant k). The following values were
% chosen following such analysis.
c0 = 50;
k0 = 120;
% K0 = 1e6; % mostly a wild guess
b0 = 4e4;
Par0 = {c0; k0; b0}; % initial guess of parameter values

%%
% We also know that the rest position of the valve is 15 degrees. So initial
% value of state x1(t) is known. Initial velocity could be assumed to be
% zero. Now we have a reasonable guess for model's initial states:
x0 = [0; 0];

%%
% We want to create a continuous-time model (since our description is in
% terms  of differential equations, not difference equations). So we choose
% the model sample time to be zero.
ts = 0;

%% 
% Now we are ready to call the IDNLGREY constructor using the above
% variables as input arguments:
Model0 = idnlgrey(ODEFile, Orders, Par0, x0, ts);

%%
% We can assign input-output and state names to this model so that it is
% compatible with the estimation data:
Model0.InputName = 'Step Command';
Model0.OutputName = 'Valve Angle';
setinit(Model0,'Name',{'Position';'Velocity'}) % set state names
display(Model0)

%%
% Before we perform estimation, we can check how good the initial guess
% model Model0 is in emulating observed output. To do this, we compare the
% output signals in estimation data to simulated response of the model. In
% doing so, let us also use the model's own guess of initial states
% (chosen by using 'init'/'model' as property-value pair input to the
% "compare" command):

% compare(MultiExpData, Model0, 'init', 'model')

%%
% The guess model gives about 60% fit to estimation data. Now let us
% perform an estimation using "pem" command to improve the fit to
% estimation data. Note that by default, the initial states are fixed to
% their initial values and only parameters are updated. You can also
% estimate initial state values, fix certain parameters to their original
% values and/or specify min/max bounds for estimation using the
% "Parameters" and "InitialStates" properties of the model. There are also
% some dedicated commands that simplify such settings: "setpar" and
% "setinit".

% % % % % % Model1 = pem(MultiExpData, Model0, 'Display', 'on', 'MaxIter', 50);

%%
% By default, the estimation algorithm uses 'lsqnonlin' search method which
% requires Optimization Toolbox. If that toolbox is not available on user's
% copy of MATLAB, then System Identification Toolbox's built-in solvers are
% used. For example, we could use 'Levenberg-Marquardt' solver that does
% not require Optimization Toolbox:
Model1 = pem(merge(data1,data2,data3,data4,data5), Model0, 'Display', 'on', 'SearchMethod', 'lsqnonlin', 'MaxIter', 100);
Model2 = pem(merge(data1,data2,data3,data4,data5), Model0, 'Display', 'on', 'SearchMethod', 'lm', 'MaxIter', 100);

%%
% As before, let us compare the response of these models to estimation and
% validation data sets:
figure, compare(data1,Model1,Model2, 'Init', 'Model')
figure, compare(data2,Model1,Model2, 'Init', 'Model')
figure, compare(data3,Model1,Model2, 'Init', 'Model')
figure, compare(data4,Model1,Model2, 'Init', 'Model')
figure, compare(data5,Model1,Model2, 'Init', 'Model')
% figure, compare(MultiExpData, Model2, 'Init', 'Model')
% figure, compare(ValidationData, Model2, 'Init', 'Model') 

%%
% The results are mixed: some outputs are reproduced well, others are not.
% The trick to getting good results in grey-box case is trying out various
% initial guesses for parameters, as well as freeing initial states for
% estimation. For example, we could add x2(0) to our list of entities to
% estimate. This will not only estimate x2(0) value for each experiment in
% the estimation dataset (MultiExpData has two experiments), but also
% change the parameter estimates by separating out the influence of
% non-zero initial velocity from overall response.
% % % % % % Model0.InitialStates(2).Fixed = false;
% % % % % % Model3 = pem(MultiExpData, Model0, 'Display', 'on', 'SearchMethod', 'lm', 'MaxIter', 100);

%%
% However, in doing so, we have estimated initial states that are
% tailor-made for estimation data sets. Validating using independent data
% sets may require initial states to be estimated separately for those
% datasets. This can be achieved by using 'Init'/'Estimate' in "compare"
% command.

% % % % % % figure, compare(MultiExpData, Model2, 'Init', 'Model') % estimated initial states may be used for estimation data

% Prepare for validation using 3 data sets. We have to create a new model
% because we are validating using a dataset that has a different number of
% experiments (3) than the estimation dataset (2). 
% % % % % % x0val = {[0 0 0];[0 0 0]};
% % % % % % Model3a = idnlgrey(ODEFile, Orders, getpar(Model3), x0val, ts);
% % % % % % Model3a.InputName = Model3.InputName;
% % % % % % Model3a.OutputName = Model3.OutputName;

%%
% The new model Model3a has same parameters as those of the estimated model
% Model3. Its initial states will be estimated to maximize fit to
% individual datasets in ValidationData.

% % % % % figure, compare(ValidationData, Model3a, 'Init', 'Estimate')

%%
% To view/fetch parameter values use the "getpar" command.
% % % % % % getpar(Model2)
% % % % % % p=getpar(Model3,'Value')
% % % % % % P=cell2mat(p)
%% Uncertainty Analysis
% A nice feature of grey-box estimation is that its parameters are few and
% have clear physical meaning. The parameters of a black box model are
% often arbitrary coefficients. Because the parameters are well defined for
% grey-box models, it is possible to view the standard deviations which
% provides another measure of the reliability of their estimates. Remember
% that the estimated parameter values are simply nominal values of
% uncertain entities that have a covariance matrix associated with them. To
% view the parameter values of the model along with their +/- 1 std errors,
% use the "present" command:
% 
% % % % % present(Model2)
% 
% %%
% % For those who are mathematically inclined, the entire covariance matrix
% % may be fetched from the "CovarianceMatrix" property of the model, as in:
% % % % % Cov = Model2.CovarianceMatrix
% 
% %%
% % This concludes our discussion of grey-box modeling approach. With this
% % approach there are two main tasks that users have to do: 
% % (1) Deriving accurate equations of motion that use well defined
% %     parameters. 
% % (2) Use reasonable initial guesses for parameter values. 
% %
% % Some related requirements are: 
% % - Parameterize the equation in a well conditioned way. Avoid parameters
% % that can potentially make the output values or state derivative values
% % infinite for finite parameter values.
% % - Reduce search space by fixing known parameters or prescribing min/max
% % bounds on free parameters.
% 
% %% Simulink Blocks For Simulation and Code Generation
% % Identified nonlinear models can be simulated using the "sim" command.
% % They can be linearized about chosen operating points. They can be
% % exported to Simulink. System Identification Toolbox provides dedicated
% % blocks that facilitate importing estimated models into Simulink.
% 
% % Simulation of estimated models
% sim(mnonlin8, getexp(MultiExpData,1))
% sim(Model2, getexp(MultiExpData,2))
% sim(hw8, getexp(ValidationData,3))
% 
% % Simulink block library
% slident 
% 
% open_system('Models_in_Simulink') % an example diagram

%%
% RTW supports code generation for black box models, as long as custom
% regressors are not used in them.  
