/*
 * File: coleta_dados.c
 *
 * Code generated for Simulink model 'coleta_dados'.
 *
 * Model version                  : 1.39
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 14:47:52 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "coleta_dados_capi.h"
#include "coleta_dados.h"
#include "coleta_dados_private.h"
#include "coleta_dados_dt.h"
#define coleta_dados_floatprecision    (2.0)
#define coleta_dados_portNumber        (1.0)

/* Block signals (auto storage) */
B_coleta_dados_T coleta_dados_B;

/* Block states (auto storage) */
DW_coleta_dados_T coleta_dados_DW;

/* Real-time model */
RT_MODEL_coleta_dados_T coleta_dados_M_;
RT_MODEL_coleta_dados_T *const coleta_dados_M = &coleta_dados_M_;

/* Model step function */
void coleta_dados_step(void)
{
  real_T dataIn;
  uint8_T data;
  uint8_T sts;

  /* MATLABSystem: '<Root>/Serial Transmit' incorporates:
   *  Constant: '<Root>/Constant'
   */
  dataIn = coleta_dados_P.Constant_Value;
  MW_Serial_write(coleta_dados_DW.obj.port, &dataIn, 1.0,
                  coleta_dados_DW.obj.dataSizeInBytes,
                  coleta_dados_DW.obj.sendModeEnum, coleta_dados_DW.obj.dataType,
                  coleta_dados_DW.obj.sendFormatEnum,
                  coleta_dados_floatprecision, '\x00');

  /* Start for MATLABSystem: '<Root>/Serial Receive' */
  MW_Serial_read((uint8_T)coleta_dados_portNumber,
                 coleta_dados_DW.obj_n.DataSizeInBytes, &data, &sts);

  /* MATLABSystem: '<Root>/Serial Receive' */
  coleta_dados_B.SerialReceive_o1 = data;
  coleta_dados_B.SerialReceive_o2 = sts;

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.005s, 0.0s] */
    rtExtModeUpload(0, coleta_dados_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.005s, 0.0s] */
    if ((rtmGetTFinal(coleta_dados_M)!=-1) &&
        !((rtmGetTFinal(coleta_dados_M)-coleta_dados_M->Timing.taskTime0) >
          coleta_dados_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(coleta_dados_M, "Simulation finished");
    }

    if (rtmGetStopRequested(coleta_dados_M)) {
      rtmSetErrorStatus(coleta_dados_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  coleta_dados_M->Timing.taskTime0 =
    (++coleta_dados_M->Timing.clockTick0) * coleta_dados_M->Timing.stepSize0;
}

/* Model initialize function */
void coleta_dados_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)coleta_dados_M, 0,
                sizeof(RT_MODEL_coleta_dados_T));
  rtmSetTFinal(coleta_dados_M, -1);
  coleta_dados_M->Timing.stepSize0 = 0.005;

  /* External mode info */
  coleta_dados_M->Sizes.checksums[0] = (2084449498U);
  coleta_dados_M->Sizes.checksums[1] = (1220680841U);
  coleta_dados_M->Sizes.checksums[2] = (1139535892U);
  coleta_dados_M->Sizes.checksums[3] = (3052700808U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[3];
    coleta_dados_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(coleta_dados_M->extModeInfo,
      &coleta_dados_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(coleta_dados_M->extModeInfo,
                        coleta_dados_M->Sizes.checksums);
    rteiSetTPtr(coleta_dados_M->extModeInfo, rtmGetTPtr(coleta_dados_M));
  }

  /* block I/O */
  (void) memset(((void *) &coleta_dados_B), 0,
                sizeof(B_coleta_dados_T));

  /* states (dwork) */
  (void) memset((void *)&coleta_dados_DW, 0,
                sizeof(DW_coleta_dados_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    coleta_dados_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Initialize DataMapInfo substructure containing ModelMap for C API */
  coleta_dados_InitializeDataMapInfo();

  /* Start for MATLABSystem: '<Root>/Serial Transmit' */
  coleta_dados_DW.obj.isInitialized = 0L;
  coleta_dados_DW.obj.isInitialized = 1L;
  coleta_dados_DW.obj.port = 1.0;
  coleta_dados_DW.obj.dataSizeInBytes = 8.0;
  coleta_dados_DW.obj.dataType = 6.0;
  coleta_dados_DW.obj.sendModeEnum = 0.0;
  coleta_dados_DW.obj.sendFormatEnum = 0.0;

  /* Start for MATLABSystem: '<Root>/Serial Receive' */
  coleta_dados_DW.obj_n.isInitialized = 0L;
  coleta_dados_DW.obj_n.isInitialized = 1L;
  coleta_dados_DW.obj_n.DataTypeWidth = 1U;
  coleta_dados_DW.obj_n.DataSizeInBytes = coleta_dados_DW.obj_n.DataTypeWidth;
}

/* Model terminate function */
void coleta_dados_terminate(void)
{
  /* Terminate for MATLABSystem: '<Root>/Serial Transmit' */
  if (coleta_dados_DW.obj.isInitialized == 1L) {
    coleta_dados_DW.obj.isInitialized = 2L;
  }

  /* End of Terminate for MATLABSystem: '<Root>/Serial Transmit' */

  /* Start for MATLABSystem: '<Root>/Serial Receive' */
  if (coleta_dados_DW.obj_n.isInitialized == 1L) {
    coleta_dados_DW.obj_n.isInitialized = 2L;
  }

  /* End of Start for MATLABSystem: '<Root>/Serial Receive' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
