/*
 * File: coleta_dados_data.c
 *
 * Code generated for Simulink model 'coleta_dados'.
 *
 * Model version                  : 1.39
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 14:47:52 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "coleta_dados.h"
#include "coleta_dados_private.h"

/* Block parameters (auto storage) */
P_coleta_dados_T coleta_dados_P = {
  /* Expression: 1
   * Referenced by: '<Root>/Constant'
   */
  1.0
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
