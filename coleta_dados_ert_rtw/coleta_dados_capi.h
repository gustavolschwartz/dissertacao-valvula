/*
 * File: coleta_dados_capi.h
 *
 * Code generated for Simulink model 'coleta_dados'.
 *
 * Model version                  : 1.39
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 14:47:52 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_coleta_dados_capi_h_
#define RTW_HEADER_coleta_dados_capi_h_
#include "coleta_dados.h"

extern void coleta_dados_InitializeDataMapInfo(void);

#endif                                 /* RTW_HEADER_coleta_dados_capi_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
