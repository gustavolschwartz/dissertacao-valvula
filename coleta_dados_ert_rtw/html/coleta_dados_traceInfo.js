function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "coleta_dados"};
	this.sidHashMap["coleta_dados"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<Root>/Constant"] = {sid: "coleta_dados:1060"};
	this.sidHashMap["coleta_dados:1060"] = {rtwname: "<Root>/Constant"};
	this.rtwnameHashMap["<Root>/Display"] = {sid: "coleta_dados:1057"};
	this.sidHashMap["coleta_dados:1057"] = {rtwname: "<Root>/Display"};
	this.rtwnameHashMap["<Root>/Display1"] = {sid: "coleta_dados:1058"};
	this.sidHashMap["coleta_dados:1058"] = {rtwname: "<Root>/Display1"};
	this.rtwnameHashMap["<Root>/Serial Receive"] = {sid: "coleta_dados:1055"};
	this.sidHashMap["coleta_dados:1055"] = {rtwname: "<Root>/Serial Receive"};
	this.rtwnameHashMap["<Root>/Serial Transmit"] = {sid: "coleta_dados:1059"};
	this.sidHashMap["coleta_dados:1059"] = {rtwname: "<Root>/Serial Transmit"};
	this.rtwnameHashMap["<Root>/To Workspace"] = {sid: "coleta_dados:1056"};
	this.sidHashMap["coleta_dados:1056"] = {rtwname: "<Root>/To Workspace"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
