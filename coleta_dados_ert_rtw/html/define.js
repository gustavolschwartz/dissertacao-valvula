function CodeDefine() { 
this.def = new Array();
this.def["IsrOverrun"] = {file: "ert_main_c.html",line:25,type:"var"};
this.def["OverrunFlag"] = {file: "ert_main_c.html",line:26,type:"var"};
this.def["rt_OneStep"] = {file: "ert_main_c.html",line:27,type:"fcn"};
this.def["stopRequested"] = {file: "ert_main_c.html",line:55,type:"var"};
this.def["main"] = {file: "ert_main_c.html",line:56,type:"fcn"};
this.def["coleta_dados_B"] = {file: "coleta_dados_c.html",line:26,type:"var"};
this.def["coleta_dados_DW"] = {file: "coleta_dados_c.html",line:29,type:"var"};
this.def["coleta_dados_M_"] = {file: "coleta_dados_c.html",line:32,type:"var"};
this.def["coleta_dados_M"] = {file: "coleta_dados_c.html",line:33,type:"var"};
this.def["coleta_dados_step"] = {file: "coleta_dados_c.html",line:36,type:"fcn"};
this.def["coleta_dados_initialize"] = {file: "coleta_dados_c.html",line:91,type:"fcn"};
this.def["coleta_dados_terminate"] = {file: "coleta_dados_c.html",line:168,type:"fcn"};
this.def["B_coleta_dados_T"] = {file: "coleta_dados_h.html",line:92,type:"type"};
this.def["DW_coleta_dados_T"] = {file: "coleta_dados_h.html",line:101,type:"type"};
this.def["codertarget_arduinobase_inter_T"] = {file: "coleta_dados_types_h.html",line:30,type:"type"};
this.def["cell_wrap_coleta_dados_T"] = {file: "coleta_dados_types_h.html",line:39,type:"type"};
this.def["codertarget_arduinobase_int_f_T"] = {file: "coleta_dados_types_h.html",line:54,type:"type"};
this.def["P_coleta_dados_T"] = {file: "coleta_dados_types_h.html",line:59,type:"type"};
this.def["RT_MODEL_coleta_dados_T"] = {file: "coleta_dados_types_h.html",line:62,type:"type"};
this.def["coleta_dados_P"] = {file: "coleta_dados_data_c.html",line:22,type:"var"};
this.def["BuiltInDTypeId"] = {file: "builtin_typeid_types_h.html",line:35,type:"type"};
this.def["DTypeId"] = {file: "builtin_typeid_types_h.html",line:40,type:"type"};
this.def["chunk_T"] = {file: "multiword_types_h.html",line:25,type:"type"};
this.def["uchunk_T"] = {file: "multiword_types_h.html",line:26,type:"type"};
this.def["int8_T"] = {file: "rtwtypes_h.html",line:49,type:"type"};
this.def["uint8_T"] = {file: "rtwtypes_h.html",line:50,type:"type"};
this.def["int16_T"] = {file: "rtwtypes_h.html",line:51,type:"type"};
this.def["uint16_T"] = {file: "rtwtypes_h.html",line:52,type:"type"};
this.def["int32_T"] = {file: "rtwtypes_h.html",line:53,type:"type"};
this.def["uint32_T"] = {file: "rtwtypes_h.html",line:54,type:"type"};
this.def["real32_T"] = {file: "rtwtypes_h.html",line:55,type:"type"};
this.def["real64_T"] = {file: "rtwtypes_h.html",line:56,type:"type"};
this.def["real_T"] = {file: "rtwtypes_h.html",line:62,type:"type"};
this.def["time_T"] = {file: "rtwtypes_h.html",line:63,type:"type"};
this.def["boolean_T"] = {file: "rtwtypes_h.html",line:64,type:"type"};
this.def["int_T"] = {file: "rtwtypes_h.html",line:65,type:"type"};
this.def["uint_T"] = {file: "rtwtypes_h.html",line:66,type:"type"};
this.def["ulong_T"] = {file: "rtwtypes_h.html",line:67,type:"type"};
this.def["char_T"] = {file: "rtwtypes_h.html",line:68,type:"type"};
this.def["uchar_T"] = {file: "rtwtypes_h.html",line:69,type:"type"};
this.def["byte_T"] = {file: "rtwtypes_h.html",line:70,type:"type"};
this.def["creal32_T"] = {file: "rtwtypes_h.html",line:80,type:"type"};
this.def["creal64_T"] = {file: "rtwtypes_h.html",line:85,type:"type"};
this.def["creal_T"] = {file: "rtwtypes_h.html",line:90,type:"type"};
this.def["cint8_T"] = {file: "rtwtypes_h.html",line:97,type:"type"};
this.def["cuint8_T"] = {file: "rtwtypes_h.html",line:104,type:"type"};
this.def["cint16_T"] = {file: "rtwtypes_h.html",line:111,type:"type"};
this.def["cuint16_T"] = {file: "rtwtypes_h.html",line:118,type:"type"};
this.def["cint32_T"] = {file: "rtwtypes_h.html",line:125,type:"type"};
this.def["cuint32_T"] = {file: "rtwtypes_h.html",line:132,type:"type"};
this.def["pointer_T"] = {file: "rtwtypes_h.html",line:150,type:"type"};
this.def["rtBlockSignals"] = {file: "coleta_dados_capi_c.html",line:42,type:"var"};
this.def["rtBlockParameters"] = {file: "coleta_dados_capi_c.html",line:57,type:"var"};
this.def["rtBlockStates"] = {file: "coleta_dados_capi_c.html",line:70,type:"var"};
this.def["rtRootInputs"] = {file: "coleta_dados_capi_c.html",line:81,type:"var"};
this.def["rtRootOutputs"] = {file: "coleta_dados_capi_c.html",line:91,type:"var"};
this.def["rtModelParameters"] = {file: "coleta_dados_capi_c.html",line:101,type:"var"};
this.def["rtDataAddrMap"] = {file: "coleta_dados_capi_c.html",line:109,type:"var"};
this.def["rtVarDimsAddrMap"] = {file: "coleta_dados_capi_c.html",line:116,type:"var"};
this.def["rtDataTypeMap"] = {file: "coleta_dados_capi_c.html",line:123,type:"var"};
this.def["rtElementMap"] = {file: "coleta_dados_capi_c.html",line:136,type:"var"};
this.def["rtDimensionMap"] = {file: "coleta_dados_capi_c.html",line:142,type:"var"};
this.def["rtDimensionArray"] = {file: "coleta_dados_capi_c.html",line:148,type:"var"};
this.def["rtcapiStoredFloats"] = {file: "coleta_dados_capi_c.html",line:160,type:"var"};
this.def["rtFixPtMap"] = {file: "coleta_dados_capi_c.html",line:165,type:"var"};
this.def["rtSampleTimeMap"] = {file: "coleta_dados_capi_c.html",line:171,type:"var"};
this.def["mmiStatic"] = {file: "coleta_dados_capi_c.html",line:177,type:"var"};
this.def["coleta_dados_GetCAPIStaticMap"] = {file: "coleta_dados_capi_c.html",line:211,type:"fcn"};
this.def["coleta_dados_InitializeDataMapInfo"] = {file: "coleta_dados_capi_c.html",line:219,type:"fcn"};
this.def["coleta_dados_host_InitializeDataMapInfo"] = {file: "coleta_dados_capi_c.html",line:251,type:"fcn"};
this.def["rtDataTypeSizes"] = {file: "coleta_dados_dt_h.html",line:21,type:"var"};
this.def["rtDataTypeNames"] = {file: "coleta_dados_dt_h.html",line:41,type:"var"};
this.def["rtBTransitions"] = {file: "coleta_dados_dt_h.html",line:61,type:"var"};
this.def["rtBTransTable"] = {file: "coleta_dados_dt_h.html",line:73,type:"var"};
this.def["rtPTransitions"] = {file: "coleta_dados_dt_h.html",line:79,type:"var"};
this.def["rtPTransTable"] = {file: "coleta_dados_dt_h.html",line:84,type:"var"};
this.def["getLoopbackIP"] = {file: "MW_target_hardware_resources_h.html",line:11,type:"var"};
this.def["coleta_dados_host_DataMapInfo_T"] = {file: "coleta_dados_capi_host_h.html",line:9,type:"type"};
}
CodeDefine.instance = new CodeDefine();
var testHarnessInfo = {OwnerFileName: "", HarnessOwner: "", HarnessName: "", IsTestHarness: "0"};
var relPathToBuildDir = "../ert_main.c";
var fileSep = "\\";
var isPC = true;
function Html2SrcLink() {
	this.html2SrcPath = new Array;
	this.html2Root = new Array;
	this.html2SrcPath["ert_main_c.html"] = "../ert_main.c";
	this.html2Root["ert_main_c.html"] = "ert_main_c.html";
	this.html2SrcPath["coleta_dados_c.html"] = "../coleta_dados.c";
	this.html2Root["coleta_dados_c.html"] = "coleta_dados_c.html";
	this.html2SrcPath["coleta_dados_h.html"] = "../coleta_dados.h";
	this.html2Root["coleta_dados_h.html"] = "coleta_dados_h.html";
	this.html2SrcPath["coleta_dados_private_h.html"] = "../coleta_dados_private.h";
	this.html2Root["coleta_dados_private_h.html"] = "coleta_dados_private_h.html";
	this.html2SrcPath["coleta_dados_types_h.html"] = "../coleta_dados_types.h";
	this.html2Root["coleta_dados_types_h.html"] = "coleta_dados_types_h.html";
	this.html2SrcPath["coleta_dados_data_c.html"] = "../coleta_dados_data.c";
	this.html2Root["coleta_dados_data_c.html"] = "coleta_dados_data_c.html";
	this.html2SrcPath["builtin_typeid_types_h.html"] = "../builtin_typeid_types.h";
	this.html2Root["builtin_typeid_types_h.html"] = "builtin_typeid_types_h.html";
	this.html2SrcPath["multiword_types_h.html"] = "../multiword_types.h";
	this.html2Root["multiword_types_h.html"] = "multiword_types_h.html";
	this.html2SrcPath["rtwtypes_h.html"] = "../rtwtypes.h";
	this.html2Root["rtwtypes_h.html"] = "rtwtypes_h.html";
	this.html2SrcPath["coleta_dados_capi_c.html"] = "../coleta_dados_capi.c";
	this.html2Root["coleta_dados_capi_c.html"] = "coleta_dados_capi_c.html";
	this.html2SrcPath["coleta_dados_capi_h.html"] = "../coleta_dados_capi.h";
	this.html2Root["coleta_dados_capi_h.html"] = "coleta_dados_capi_h.html";
	this.html2SrcPath["coleta_dados_dt_h.html"] = "../coleta_dados_dt.h";
	this.html2Root["coleta_dados_dt_h.html"] = "coleta_dados_dt_h.html";
	this.html2SrcPath["rtmodel_h.html"] = "../rtmodel.h";
	this.html2Root["rtmodel_h.html"] = "rtmodel_h.html";
	this.html2SrcPath["MW_target_hardware_resources_h.html"] = "../MW_target_hardware_resources.h";
	this.html2Root["MW_target_hardware_resources_h.html"] = "MW_target_hardware_resources_h.html";
	this.html2SrcPath["coleta_dados_capi_host_h.html"] = "../coleta_dados_capi_host.h";
	this.html2Root["coleta_dados_capi_host_h.html"] = "coleta_dados_capi_host_h.html";
	this.getLink2Src = function (htmlFileName) {
		 if (this.html2SrcPath[htmlFileName])
			 return this.html2SrcPath[htmlFileName];
		 else
			 return null;
	}
	this.getLinkFromRoot = function (htmlFileName) {
		 if (this.html2Root[htmlFileName])
			 return this.html2Root[htmlFileName];
		 else
			 return null;
	}
}
Html2SrcLink.instance = new Html2SrcLink();
var fileList = [
"ert_main_c.html","coleta_dados_c.html","coleta_dados_h.html","coleta_dados_private_h.html","coleta_dados_types_h.html","coleta_dados_data_c.html","builtin_typeid_types_h.html","multiword_types_h.html","rtwtypes_h.html","coleta_dados_capi_c.html","coleta_dados_capi_h.html","coleta_dados_dt_h.html","rtmodel_h.html","MW_target_hardware_resources_h.html","coleta_dados_capi_host_h.html"];
