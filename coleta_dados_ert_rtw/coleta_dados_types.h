/*
 * File: coleta_dados_types.h
 *
 * Code generated for Simulink model 'coleta_dados'.
 *
 * Model version                  : 1.39
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 14:47:52 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_coleta_dados_types_h_
#define RTW_HEADER_coleta_dados_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#ifndef typedef_codertarget_arduinobase_inter_T
#define typedef_codertarget_arduinobase_inter_T

typedef struct {
  int32_T isInitialized;
  uint16_T DataSizeInBytes;
  uint16_T DataTypeWidth;
} codertarget_arduinobase_inter_T;

#endif                                 /*typedef_codertarget_arduinobase_inter_T*/

#ifndef typedef_cell_wrap_coleta_dados_T
#define typedef_cell_wrap_coleta_dados_T

typedef struct {
  uint32_T f1[8];
} cell_wrap_coleta_dados_T;

#endif                                 /*typedef_cell_wrap_coleta_dados_T*/

#ifndef typedef_codertarget_arduinobase_int_f_T
#define typedef_codertarget_arduinobase_int_f_T

typedef struct {
  int32_T isInitialized;
  cell_wrap_coleta_dados_T inputVarSize;
  real_T port;
  real_T dataSizeInBytes;
  real_T dataType;
  real_T sendModeEnum;
  real_T sendFormatEnum;
} codertarget_arduinobase_int_f_T;

#endif                                 /*typedef_codertarget_arduinobase_int_f_T*/

/* Parameters (auto storage) */
typedef struct P_coleta_dados_T_ P_coleta_dados_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_coleta_dados_T RT_MODEL_coleta_dados_T;

#endif                                 /* RTW_HEADER_coleta_dados_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
