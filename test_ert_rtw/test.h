/*
 * File: test.h
 *
 * Code generated for Simulink model 'test'.
 *
 * Model version                  : 1.37
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 09:59:02 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_test_h_
#define RTW_HEADER_test_h_
#include <math.h>
#include <float.h>
#include <string.h>
#include <stddef.h>
#include "rtw_modelmap.h"
#ifndef test_COMMON_INCLUDES_
# define test_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "MW_arduinoI2C.h"
#include "arduino_analogoutput_lct.h"
#endif                                 /* test_COMMON_INCLUDES_ */

#include "test_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "MW_target_hardware_resources.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetDataMapInfo
# define rtmGetDataMapInfo(rtm)        ((rtm)->DataMapInfo)
#endif

#ifndef rtmSetDataMapInfo
# define rtmSetDataMapInfo(rtm, val)   ((rtm)->DataMapInfo = (val))
#endif

#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T In;                           /* '<Root>/Rate Transition' */
  real_T Pos_Real;                     /* '<Root>/Gain3' */
  real_T Switch;                       /* '<S3>/Switch' */
} B_test_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Integrator_DSTATE;            /* '<S2>/Integrator' */
  real_T RateTransition_Buffer0;       /* '<Root>/Rate Transition' */
  struct {
    void *LoggedData[2];
  } Scope1_PWORK;                      /* '<Root>/Scope1' */

  struct {
    void *LoggedData;
  } ToWorkspace1_PWORK;                /* '<Root>/To Workspace1' */

  struct {
    void *LoggedData;
  } ToWorkspace3_PWORK;                /* '<Root>/To Workspace3' */

  struct {
    void *LoggedData;
  } ToWorkspace5_PWORK;                /* '<Root>/To Workspace5' */

  codertarget_arduinobase_inter_T obj; /* '<Root>/Sensor_Pos' */
  uint8_T Output_DSTATE;               /* '<S32>/Output' */
} DW_test_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T Out1;                         /* '<Root>/Out1' */
} ExtY_test_T;

/* Parameters (auto storage) */
struct P_test_T_ {
  real_T B;                            /* Variable: B
                                        * Referenced by: '<S2>/Setpoint Weighting (Proportional)'
                                        */
  real_T I;                            /* Variable: I
                                        * Referenced by: '<S2>/Integral Gain'
                                        */
  real_T P;                            /* Variable: P
                                        * Referenced by: '<S2>/Proportional Gain'
                                        */
  real_T DiscreteVarying2DOFPID2_LowerSa;/* Mask Parameter: DiscreteVarying2DOFPID2_LowerSa
                                          * Referenced by:
                                          *   '<S2>/Saturate'
                                          *   '<S13>/DeadZone'
                                          */
  real_T Teste3_OutValues[7];          /* Mask Parameter: Teste3_OutValues
                                        * Referenced by: '<S11>/Vector'
                                        */
  real_T DiscreteVarying2DOFPID2_UpperSa;/* Mask Parameter: DiscreteVarying2DOFPID2_UpperSa
                                          * Referenced by:
                                          *   '<S2>/Saturate'
                                          *   '<S13>/DeadZone'
                                          */
  real_T CompareToConstant1_const;     /* Mask Parameter: CompareToConstant1_const
                                        * Referenced by: '<S15>/Constant'
                                        */
  real_T CompareToConstant2_const;     /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S16>/Constant'
                                        */
  real_T CompareToConstant_const;      /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S14>/Constant'
                                        */
  uint32_T PWM_pinNumber;              /* Mask Parameter: PWM_pinNumber
                                        * Referenced by: '<S6>/PWM'
                                        */
  uint8_T LimitedCounter_uplimit;      /* Mask Parameter: LimitedCounter_uplimit
                                        * Referenced by: '<S34>/FixPt Switch'
                                        */
  real_T Constant_Value;               /* Expression: 120
                                        * Referenced by: '<S3>/Constant'
                                        */
  real_T RateTransition_InitialCondition;/* Expression: 0
                                          * Referenced by: '<Root>/Rate Transition'
                                          */
  real_T Integrator_gainval;           /* Computed Parameter: Integrator_gainval
                                        * Referenced by: '<S2>/Integrator'
                                        */
  real_T Integrator_IC;                /* Expression: InitialConditionForIntegrator
                                        * Referenced by: '<S2>/Integrator'
                                        */
  real_T ZeroGain_Gain;                /* Expression: 0
                                        * Referenced by: '<S13>/ZeroGain'
                                        */
  real_T Constant_Value_k;             /* Expression: 0
                                        * Referenced by: '<S2>/Constant'
                                        */
  int16_T Bias1_Bias;                  /* Computed Parameter: Bias1_Bias
                                        * Referenced by: '<Root>/Bias1'
                                        */
  int16_T Gain3_Gain;                  /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<Root>/Gain3'
                                        */
  uint8_T Constant_Value_b;            /* Computed Parameter: Constant_Value_b
                                        * Referenced by: '<S34>/Constant'
                                        */
  uint8_T FixPtConstant_Value;         /* Computed Parameter: FixPtConstant_Value
                                        * Referenced by: '<S33>/FixPt Constant'
                                        */
  uint8_T Output_InitialCondition;     /* Computed Parameter: Output_InitialCondition
                                        * Referenced by: '<S32>/Output'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_test_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * DataMapInfo:
   * The following substructure contains information regarding
   * structures generated in the model's C API.
   */
  struct {
    rtwCAPI_ModelMappingInfo mmi;
  } DataMapInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    uint16_T clockTick1;
    struct {
      uint16_T TID[2];
    } TaskCounters;

    struct {
      boolean_T TID0_1;
    } RateInteraction;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_test_T test_P;

/* Block signals (auto storage) */
extern B_test_T test_B;

/* Block states (auto storage) */
extern DW_test_T test_DW;

/* External outputs (root outports fed by signals with auto storage) */
extern ExtY_test_T test_Y;

/* External function called from main */
extern void test_SetEventsForThisBaseStep(boolean_T *eventFlags);

/* Model entry point functions */
extern void test_SetEventsForThisBaseStep(boolean_T *eventFlags);
extern void test_initialize(void);
extern void test_step0(void);
extern void test_step1(void);
extern void test_terminate(void);

/* Function to get C API Model Mapping Static Info */
extern const rtwCAPI_ModelMappingStaticInfo*
  test_GetCAPIStaticMap(void);

/* Real-time Model object */
extern RT_MODEL_test_T *const test_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S3>/Logical Operator1' : Unused code path elimination
 * Block '<S3>/Logical Operator2' : Unused code path elimination
 * Block '<S32>/Data Type Propagation' : Unused code path elimination
 * Block '<S33>/FixPt Data Type Duplicate' : Unused code path elimination
 * Block '<S34>/FixPt Data Type Duplicate1' : Unused code path elimination
 * Block '<S11>/Out' : Eliminate redundant signal conversion block
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'test'
 * '<S1>'   : 'test/Discrete Varying 2DOF PID1'
 * '<S2>'   : 'test/Discrete Varying 2DOF PID2'
 * '<S3>'   : 'test/Limites'
 * '<S4>'   : 'test/Limites1'
 * '<S5>'   : 'test/PID'
 * '<S6>'   : 'test/PWM'
 * '<S7>'   : 'test/Regressores'
 * '<S8>'   : 'test/Regressores1'
 * '<S9>'   : 'test/Teste1'
 * '<S10>'  : 'test/Teste2'
 * '<S11>'  : 'test/Teste3'
 * '<S12>'  : 'test/Discrete Varying 2DOF PID1/Clamping circuit'
 * '<S13>'  : 'test/Discrete Varying 2DOF PID2/Clamping circuit'
 * '<S14>'  : 'test/Limites/Compare To Constant'
 * '<S15>'  : 'test/Limites/Compare To Constant1'
 * '<S16>'  : 'test/Limites/Compare To Constant2'
 * '<S17>'  : 'test/Limites1/Compare To Constant'
 * '<S18>'  : 'test/Limites1/Compare To Constant1'
 * '<S19>'  : 'test/Limites1/Compare To Constant2'
 * '<S20>'  : 'test/PID/Discrete PID Controller'
 * '<S21>'  : 'test/PID/Discrete PID Controller/Differentiator'
 * '<S22>'  : 'test/Regressores/Discrete Derivative'
 * '<S23>'  : 'test/Regressores/Discrete Derivative1'
 * '<S24>'  : 'test/Regressores1/Discrete Derivative'
 * '<S25>'  : 'test/Regressores1/Discrete Derivative1'
 * '<S26>'  : 'test/Teste1/LimitedCounter'
 * '<S27>'  : 'test/Teste1/LimitedCounter/Increment Real World'
 * '<S28>'  : 'test/Teste1/LimitedCounter/Wrap To Zero'
 * '<S29>'  : 'test/Teste2/LimitedCounter'
 * '<S30>'  : 'test/Teste2/LimitedCounter/Increment Real World'
 * '<S31>'  : 'test/Teste2/LimitedCounter/Wrap To Zero'
 * '<S32>'  : 'test/Teste3/LimitedCounter'
 * '<S33>'  : 'test/Teste3/LimitedCounter/Increment Real World'
 * '<S34>'  : 'test/Teste3/LimitedCounter/Wrap To Zero'
 */
#endif                                 /* RTW_HEADER_test_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
