/*
 * File: test_capi.c
 *
 * Code generated for Simulink model 'test'.
 *
 * Model version                  : 1.37
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 09:59:02 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include <stddef.h>
#include "rtw_capi.h"
#ifdef HOST_CAPI_BUILD
#include "test_capi_host.h"
#define sizeof(s)                      ((size_t)(0xFFFF))
#undef rt_offsetof
#define rt_offsetof(s,el)              ((uint16_T)(0xFFFF))
#define TARGET_CONST
#define TARGET_STRING(s)               (s)
#else                                  /* HOST_CAPI_BUILD */
#include "builtin_typeid_types.h"
#include "test.h"
#include "test_capi.h"
#include "test_private.h"
#ifdef LIGHT_WEIGHT_CAPI
#define TARGET_CONST
#define TARGET_STRING(s)               (NULL)
#else
#define TARGET_CONST                   const
#define TARGET_STRING(s)               (s)
#endif
#endif                                 /* HOST_CAPI_BUILD */

/* Block output signal information */
static const rtwCAPI_Signals rtBlockSignals[] = {
  /* addrMapIndex, sysNum, blockPath,
   * signalName, portNumber, dataTypeIndex, dimIndex, fxpIndex, sTimeIndex
   */
  { 0, 0, TARGET_STRING("test/Gain3"),
    TARGET_STRING("Pos_Real"), 0, 0, 0, 0, 0 },

  { 1, 0, TARGET_STRING("test/Rate Transition"),
    TARGET_STRING("In"), 0, 0, 0, 0, 0 },

  { 2, 0, TARGET_STRING("test/Limites/Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  {
    0, 0, (NULL), (NULL), 0, 0, 0, 0, 0
  }
};

static const rtwCAPI_BlockParameters rtBlockParameters[] = {
  /* addrMapIndex, blockPath,
   * paramName, dataTypeIndex, dimIndex, fixPtIdx
   */
  { 3, TARGET_STRING("test/Discrete Varying 2DOF PID2"),
    TARGET_STRING("UpperSaturationLimit"), 0, 0, 0 },

  { 4, TARGET_STRING("test/Discrete Varying 2DOF PID2"),
    TARGET_STRING("LowerSaturationLimit"), 0, 0, 0 },

  { 5, TARGET_STRING("test/Teste3"),
    TARGET_STRING("OutValues"), 0, 1, 0 },

  { 6, TARGET_STRING("test/Bias1"),
    TARGET_STRING("Bias"), 1, 0, 0 },

  { 7, TARGET_STRING("test/Gain3"),
    TARGET_STRING("Gain"), 1, 0, 1 },

  { 8, TARGET_STRING("test/Rate Transition"),
    TARGET_STRING("InitialCondition"), 0, 0, 0 },

  { 9, TARGET_STRING("test/Discrete Varying 2DOF PID2/Constant"),
    TARGET_STRING("Value"), 0, 0, 0 },

  { 10, TARGET_STRING("test/Discrete Varying 2DOF PID2/Integrator"),
    TARGET_STRING("gainval"), 0, 0, 0 },

  { 11, TARGET_STRING("test/Discrete Varying 2DOF PID2/Integrator"),
    TARGET_STRING("InitialCondition"), 0, 0, 0 },

  { 12, TARGET_STRING("test/Limites/Compare To Constant"),
    TARGET_STRING("const"), 0, 0, 0 },

  { 13, TARGET_STRING("test/Limites/Compare To Constant1"),
    TARGET_STRING("const"), 0, 0, 0 },

  { 14, TARGET_STRING("test/Limites/Compare To Constant2"),
    TARGET_STRING("const"), 0, 0, 0 },

  { 15, TARGET_STRING("test/Limites/Constant"),
    TARGET_STRING("Value"), 0, 0, 0 },

  { 16, TARGET_STRING("test/PWM/PWM"),
    TARGET_STRING("pinNumber"), 2, 0, 0 },

  { 17, TARGET_STRING("test/Teste3/LimitedCounter"),
    TARGET_STRING("uplimit"), 3, 0, 0 },

  { 18, TARGET_STRING("test/Discrete Varying 2DOF PID2/Clamping circuit/ZeroGain"),
    TARGET_STRING("Gain"), 0, 0, 0 },

  { 19, TARGET_STRING("test/Teste3/LimitedCounter/Output"),
    TARGET_STRING("InitialCondition"), 3, 0, 0 },

  { 20, TARGET_STRING("test/Teste3/LimitedCounter/Increment Real World/FixPt Constant"),
    TARGET_STRING("Value"), 3, 0, 0 },

  { 21, TARGET_STRING("test/Teste3/LimitedCounter/Wrap To Zero/Constant"),
    TARGET_STRING("Value"), 3, 0, 0 },

  {
    0, (NULL), (NULL), 0, 0, 0
  }
};

/* Block states information */
static const rtwCAPI_States rtBlockStates[] = {
  /* addrMapIndex, contStateStartIndex, blockPath,
   * stateName, pathAlias, dWorkIndex, dataTypeIndex, dimIndex,
   * fixPtIdx, sTimeIndex, isContinuous, hierInfoIdx, flatElemIdx
   */
  { 22, -1, TARGET_STRING("test/Discrete Varying 2DOF PID2/Integrator"),
    TARGET_STRING(""), "", 0, 0, 0, 0, 0, 0, -1, 0 },

  { 23, -1, TARGET_STRING("test/Teste3/LimitedCounter/Output"),
    TARGET_STRING(""), "", 0, 3, 0, 0, 1, 0, -1, 0 },

  {
    0, -1, (NULL), (NULL), (NULL), 0, 0, 0, 0, 0, 0, -1, 0
  }
};

/* Root Inputs information */
static const rtwCAPI_Signals rtRootInputs[] = {
  /* addrMapIndex, sysNum, blockPath,
   * signalName, portNumber, dataTypeIndex, dimIndex, fxpIndex, sTimeIndex
   */
  {
    0, 0, (NULL), (NULL), 0, 0, 0, 0, 0
  }
};

/* Root Outputs information */
static const rtwCAPI_Signals rtRootOutputs[] = {
  /* addrMapIndex, sysNum, blockPath,
   * signalName, portNumber, dataTypeIndex, dimIndex, fxpIndex, sTimeIndex
   */
  { 24, 0, TARGET_STRING("test/Out1"),
    TARGET_STRING(""), 1, 0, 0, 0, 0 },

  {
    0, 0, (NULL), (NULL), 0, 0, 0, 0, 0
  }
};

/* Tunable variable parameters */
static const rtwCAPI_ModelParameters rtModelParameters[] = {
  /* addrMapIndex, varName, dataTypeIndex, dimIndex, fixPtIndex */
  { 25, TARGET_STRING("B"), 0, 0, 0 },

  { 26, TARGET_STRING("I"), 0, 0, 0 },

  { 27, TARGET_STRING("P"), 0, 0, 0 },

  { 0, (NULL), 0, 0, 0 }
};

#ifndef HOST_CAPI_BUILD

/* Declare Data Addresses statically */
static void* rtDataAddrMap[] = {
  &test_B.Pos_Real,                    /* 0: Signal */
  &test_B.In,                          /* 1: Signal */
  &test_B.Switch,                      /* 2: Signal */
  &test_P.DiscreteVarying2DOFPID2_UpperSa,/* 3: Mask Parameter */
  &test_P.DiscreteVarying2DOFPID2_LowerSa,/* 4: Mask Parameter */
  &test_P.Teste3_OutValues[0],         /* 5: Mask Parameter */
  &test_P.Bias1_Bias,                  /* 6: Block Parameter */
  &test_P.Gain3_Gain,                  /* 7: Block Parameter */
  &test_P.RateTransition_InitialCondition,/* 8: Block Parameter */
  &test_P.Constant_Value_k,            /* 9: Block Parameter */
  &test_P.Integrator_gainval,          /* 10: Block Parameter */
  &test_P.Integrator_IC,               /* 11: Block Parameter */
  &test_P.CompareToConstant_const,     /* 12: Mask Parameter */
  &test_P.CompareToConstant1_const,    /* 13: Mask Parameter */
  &test_P.CompareToConstant2_const,    /* 14: Mask Parameter */
  &test_P.Constant_Value,              /* 15: Block Parameter */
  &test_P.PWM_pinNumber,               /* 16: Block Parameter */
  &test_P.LimitedCounter_uplimit,      /* 17: Mask Parameter */
  &test_P.ZeroGain_Gain,               /* 18: Block Parameter */
  &test_P.Output_InitialCondition,     /* 19: Block Parameter */
  &test_P.FixPtConstant_Value,         /* 20: Block Parameter */
  &test_P.Constant_Value_b,            /* 21: Block Parameter */
  &test_DW.Integrator_DSTATE,          /* 22: Discrete State */
  &test_DW.Output_DSTATE,              /* 23: Discrete State */
  &test_Y.Out1,                        /* 24: Root Output */
  &test_P.B,                           /* 25: Model Parameter */
  &test_P.I,                           /* 26: Model Parameter */
  &test_P.P,                           /* 27: Model Parameter */
};

/* Declare Data Run-Time Dimension Buffer Addresses statically */
static int32_T* rtVarDimsAddrMap[] = {
  (NULL)
};

#endif

/* Data Type Map - use dataTypeMapIndex to access this structure */
static TARGET_CONST rtwCAPI_DataTypeMap rtDataTypeMap[] = {
  /* cName, mwName, numElements, elemMapIndex, dataSize, slDataId, *
   * isComplex, isPointer */
  { "double", "real_T", 0, 0, sizeof(real_T), SS_DOUBLE, 0, 0 },

  { "int", "int16_T", 0, 0, sizeof(int16_T), SS_INT16, 0, 0 },

  { "unsigned long", "uint32_T", 0, 0, sizeof(uint32_T), SS_UINT32, 0, 0 },

  { "unsigned char", "uint8_T", 0, 0, sizeof(uint8_T), SS_UINT8, 0, 0 }
};

#ifdef HOST_CAPI_BUILD
#undef sizeof
#endif

/* Structure Element Map - use elemMapIndex to access this structure */
static TARGET_CONST rtwCAPI_ElementMap rtElementMap[] = {
  /* elementName, elementOffset, dataTypeIndex, dimIndex, fxpIndex */
  { (NULL), 0, 0, 0, 0 },
};

/* Dimension Map - use dimensionMapIndex to access elements of ths structure*/
static const rtwCAPI_DimensionMap rtDimensionMap[] = {
  /* dataOrientation, dimArrayIndex, numDims, vardimsIndex */
  { rtwCAPI_SCALAR, 0, 2, 0 },

  { rtwCAPI_VECTOR, 2, 2, 0 }
};

/* Dimension Array- use dimArrayIndex to access elements of this array */
static const uint_T rtDimensionArray[] = {
  1,                                   /* 0 */
  1,                                   /* 1 */
  1,                                   /* 2 */
  7                                    /* 3 */
};

/* C-API stores floating point values in an array. The elements of this  *
 * are unique. This ensures that values which are shared across the model*
 * are stored in the most efficient way. These values are referenced by  *
 *           - rtwCAPI_FixPtMap.fracSlopePtr,                            *
 *           - rtwCAPI_FixPtMap.biasPtr,                                 *
 *           - rtwCAPI_SampleTimeMap.samplePeriodPtr,                    *
 *           - rtwCAPI_SampleTimeMap.sampleOffsetPtr                     */
static const real_T rtcapiStoredFloats[] = {
  0.01, 0.0, 1.0, 5.0
};

/* Fixed Point Map */
static const rtwCAPI_FixPtMap rtFixPtMap[] = {
  /* fracSlopePtr, biasPtr, scaleType, wordLength, exponent, isSigned */
  { (NULL), (NULL), rtwCAPI_FIX_RESERVED, 0, 0, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 16, -17, 1 }
};

/* Sample Time Map - use sTimeIndex to access elements of ths structure */
static const rtwCAPI_SampleTimeMap rtSampleTimeMap[] = {
  /* samplePeriodPtr, sampleOffsetPtr, tid, samplingMode */
  { (const void *) &rtcapiStoredFloats[0], (const void *) &rtcapiStoredFloats[1],
    0, 0 },

  { (const void *) &rtcapiStoredFloats[3], (const void *) &rtcapiStoredFloats[1],
    1, 0 }
};

static rtwCAPI_ModelMappingStaticInfo mmiStatic = {
  /* Signals:{signals, numSignals,
   *           rootInputs, numRootInputs,
   *           rootOutputs, numRootOutputs},
   * Params: {blockParameters, numBlockParameters,
   *          modelParameters, numModelParameters},
   * States: {states, numStates},
   * Maps:   {dataTypeMap, dimensionMap, fixPtMap,
   *          elementMap, sampleTimeMap, dimensionArray},
   * TargetType: targetType
   */
  { rtBlockSignals, 3,
    rtRootInputs, 0,
    rtRootOutputs, 1 },

  { rtBlockParameters, 19,
    rtModelParameters, 3 },

  { rtBlockStates, 2 },

  { rtDataTypeMap, rtDimensionMap, rtFixPtMap,
    rtElementMap, rtSampleTimeMap, rtDimensionArray },
  "float",

  { 4132955378U,
    3494056472U,
    1338565159U,
    1204804192U },
  (NULL), 0,
  0
};

/* Function to get C API Model Mapping Static Info */
const rtwCAPI_ModelMappingStaticInfo*
  test_GetCAPIStaticMap(void)
{
  return &mmiStatic;
}

/* Cache pointers into DataMapInfo substructure of RTModel */
#ifndef HOST_CAPI_BUILD

void test_InitializeDataMapInfo(void)
{
  /* Set C-API version */
  rtwCAPI_SetVersion(test_M->DataMapInfo.mmi, 1);

  /* Cache static C-API data into the Real-time Model Data structure */
  rtwCAPI_SetStaticMap(test_M->DataMapInfo.mmi, &mmiStatic);

  /* Cache static C-API logging data into the Real-time Model Data structure */
  rtwCAPI_SetLoggingStaticMap(test_M->DataMapInfo.mmi, (NULL));

  /* Cache C-API Data Addresses into the Real-Time Model Data structure */
  rtwCAPI_SetDataAddressMap(test_M->DataMapInfo.mmi, rtDataAddrMap);

  /* Cache C-API Data Run-Time Dimension Buffer Addresses into the Real-Time Model Data structure */
  rtwCAPI_SetVarDimsAddressMap(test_M->DataMapInfo.mmi, rtVarDimsAddrMap);

  /* Cache the instance C-API logging pointer */
  rtwCAPI_SetInstanceLoggingInfo(test_M->DataMapInfo.mmi, (NULL));

  /* Set reference to submodels */
  rtwCAPI_SetChildMMIArray(test_M->DataMapInfo.mmi, (NULL));
  rtwCAPI_SetChildMMIArrayLen(test_M->DataMapInfo.mmi, 0);
}

#else                                  /* HOST_CAPI_BUILD */
#ifdef __cplusplus

extern "C" {

#endif

  void test_host_InitializeDataMapInfo(test_host_DataMapInfo_T *dataMap, const
    char *path)
  {
    /* Set C-API version */
    rtwCAPI_SetVersion(dataMap->mmi, 1);

    /* Cache static C-API data into the Real-time Model Data structure */
    rtwCAPI_SetStaticMap(dataMap->mmi, &mmiStatic);

    /* host data address map is NULL */
    rtwCAPI_SetDataAddressMap(dataMap->mmi, NULL);

    /* host vardims address map is NULL */
    rtwCAPI_SetVarDimsAddressMap(dataMap->mmi, NULL);

    /* Set Instance specific path */
    rtwCAPI_SetPath(dataMap->mmi, path);
    rtwCAPI_SetFullPath(dataMap->mmi, NULL);

    /* Set reference to submodels */
    rtwCAPI_SetChildMMIArray(dataMap->mmi, (NULL));
    rtwCAPI_SetChildMMIArrayLen(dataMap->mmi, 0);
  }

#ifdef __cplusplus

}
#endif
#endif                                 /* HOST_CAPI_BUILD */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
