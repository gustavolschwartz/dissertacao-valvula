/*
 * File: test.c
 *
 * Code generated for Simulink model 'test'.
 *
 * Model version                  : 1.37
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 09:59:02 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "test_capi.h"
#include "test.h"
#include "test_private.h"
#include "test_dt.h"
#define test_RegisterAddress_          (12.0)
#define test_SlaveAddress_             (54.0)

/* Block signals (auto storage) */
B_test_T test_B;

/* Block states (auto storage) */
DW_test_T test_DW;

/* External outputs (root outports fed by signals with auto storage) */
ExtY_test_T test_Y;

/* Real-time model */
RT_MODEL_test_T test_M_;
RT_MODEL_test_T *const test_M = &test_M_;
static void rate_monotonic_scheduler(void);

/*
 * Set which subrates need to run this base step (base rate always runs).
 * This function must be called prior to calling the model step function
 * in order to "remember" which rates need to run this base step.  The
 * buffering of events allows for overlapping preemption.
 */
void test_SetEventsForThisBaseStep(boolean_T *eventFlags)
{
  /* Task runs when its counter is zero, computed via rtmStepTask macro */
  eventFlags[1] = ((boolean_T)rtmStepTask(test_M, 1));
}

/*
 *   This function updates active task flag for each subrate
 * and rate transition flags for tasks that exchange data.
 * The function assumes rate-monotonic multitasking scheduler.
 * The function must be called at model base rate so that
 * the generated code self-manages all its subrates and rate
 * transition flags.
 */
static void rate_monotonic_scheduler(void)
{
  /* To ensure a deterministic data transfer between two rates,
   * data is transferred at the priority of a fast task and the frequency
   * of the slow task.  The following flags indicate when the data transfer
   * happens.  That is, a rate interaction flag is set true when both rates
   * will run, and false otherwise.
   */

  /* tid 0 shares data with slower tid rate: 1 */
  test_M->Timing.RateInteraction.TID0_1 = (test_M->Timing.TaskCounters.TID[1] ==
    0);

  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (test_M->Timing.TaskCounters.TID[1])++;
  if ((test_M->Timing.TaskCounters.TID[1]) > 499) {/* Sample time: [5.0s, 0.0s] */
    test_M->Timing.TaskCounters.TID[1] = 0;
  }
}

/* Model step function for TID0 */
void test_step0(void)                  /* Sample time: [0.01s, 0.0s] */
{
  uint8_T rawData[2];
  real_T rtb_IntegralGain;
  boolean_T rtb_NotEqual;
  real_T rtb_ZeroGain;
  int8_T rtb_DataTypeConv1_0;
  uint16_T q0;
  uint16_T qY;
  uint8_T tmp;

  {                                    /* Sample time: [0.01s, 0.0s] */
    rate_monotonic_scheduler();
  }

  /* Outport: '<Root>/Out1' */
  test_Y.Out1 = 0.0;

  /* RateTransition: '<Root>/Rate Transition' */
  if (test_M->Timing.RateInteraction.TID0_1) {
    test_B.In = test_DW.RateTransition_Buffer0;
  }

  /* End of RateTransition: '<Root>/Rate Transition' */

  /* Start for MATLABSystem: '<Root>/Sensor_Pos' */
  MW_i2cReadLoop((uint8_T)test_SlaveAddress_, 1.0, test_RegisterAddress_, 2U,
                 rawData);
  q0 = (uint16_T)rawData[0] << 8U;
  qY = q0 + /*MW:OvSatOk*/ rawData[1];
  if (qY < q0) {
    qY = MAX_uint16_T;
  }

  /* Gain: '<Root>/Gain3' incorporates:
   *  Bias: '<Root>/Bias1'
   *  DataTypeConversion: '<Root>/Data Type Conversion3'
   *  MATLABSystem: '<Root>/Sensor_Pos'
   *  MATLABSystem: '<Root>/Sensor_Pos'
   */
  test_B.Pos_Real = (real_T)test_P.Gain3_Gain * 7.62939453125E-6 * (real_T)
    ((int16_T)qY + test_P.Bias1_Bias);

  /* Sum: '<S2>/Sum' incorporates:
   *  DiscreteIntegrator: '<S2>/Integrator'
   *  Gain: '<S2>/Proportional Gain'
   *  Gain: '<S2>/Setpoint Weighting (Proportional)'
   *  Sum: '<S2>/Sum1'
   */
  rtb_IntegralGain = (test_P.B * test_B.In - test_B.Pos_Real) * test_P.P +
    test_DW.Integrator_DSTATE;

  /* Switch: '<S3>/Switch' incorporates:
   *  Constant: '<S14>/Constant'
   *  Constant: '<S15>/Constant'
   *  Constant: '<S16>/Constant'
   *  Constant: '<S3>/Constant'
   *  Logic: '<S3>/Logical Operator'
   *  Product: '<S3>/Product'
   *  RelationalOperator: '<S14>/Compare'
   *  RelationalOperator: '<S15>/Compare'
   *  RelationalOperator: '<S16>/Compare'
   *  Saturate: '<S2>/Saturate'
   */
  if ((test_B.In >= test_P.CompareToConstant1_const) && (test_B.Pos_Real >=
       test_P.CompareToConstant2_const)) {
    test_B.Switch = test_P.Constant_Value;
  } else {
    if (rtb_IntegralGain > test_P.DiscreteVarying2DOFPID2_UpperSa) {
      /* Saturate: '<S2>/Saturate' */
      rtb_ZeroGain = test_P.DiscreteVarying2DOFPID2_UpperSa;
    } else if (rtb_IntegralGain < test_P.DiscreteVarying2DOFPID2_LowerSa) {
      /* Saturate: '<S2>/Saturate' */
      rtb_ZeroGain = test_P.DiscreteVarying2DOFPID2_LowerSa;
    } else {
      /* Saturate: '<S2>/Saturate' */
      rtb_ZeroGain = rtb_IntegralGain;
    }

    test_B.Switch = (real_T)(test_B.In >= test_P.CompareToConstant_const) *
      rtb_ZeroGain;
  }

  /* End of Switch: '<S3>/Switch' */

  /* DataTypeConversion: '<S6>/Data Type Conversion' */
  if (test_B.Switch < 256.0) {
    if (test_B.Switch >= 0.0) {
      tmp = (uint8_T)test_B.Switch;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint8_T;
  }

  /* End of DataTypeConversion: '<S6>/Data Type Conversion' */

  /* S-Function (arduinoanalogoutput_sfcn): '<S6>/PWM' */
  MW_analogWrite(test_P.PWM_pinNumber, tmp);

  /* Gain: '<S13>/ZeroGain' */
  rtb_ZeroGain = test_P.ZeroGain_Gain * rtb_IntegralGain;

  /* DeadZone: '<S13>/DeadZone' */
  if (rtb_IntegralGain > test_P.DiscreteVarying2DOFPID2_UpperSa) {
    rtb_IntegralGain -= test_P.DiscreteVarying2DOFPID2_UpperSa;
  } else if (rtb_IntegralGain >= test_P.DiscreteVarying2DOFPID2_LowerSa) {
    rtb_IntegralGain = 0.0;
  } else {
    rtb_IntegralGain -= test_P.DiscreteVarying2DOFPID2_LowerSa;
  }

  /* End of DeadZone: '<S13>/DeadZone' */

  /* RelationalOperator: '<S13>/NotEqual' */
  rtb_NotEqual = (rtb_ZeroGain != rtb_IntegralGain);

  /* Signum: '<S13>/SignDeltaU' */
  if (rtb_IntegralGain < 0.0) {
    rtb_IntegralGain = -1.0;
  } else if (rtb_IntegralGain > 0.0) {
    rtb_IntegralGain = 1.0;
  } else if (rtb_IntegralGain == 0.0) {
    rtb_IntegralGain = 0.0;
  } else {
    rtb_IntegralGain = (rtNaN);
  }

  /* End of Signum: '<S13>/SignDeltaU' */

  /* DataTypeConversion: '<S13>/DataTypeConv1' */
  if (rtb_IntegralGain < 128.0) {
    rtb_DataTypeConv1_0 = (int8_T)rtb_IntegralGain;
  } else {
    rtb_DataTypeConv1_0 = MAX_int8_T;
  }

  /* Gain: '<S2>/Integral Gain' incorporates:
   *  Sum: '<S2>/Sum2'
   */
  rtb_IntegralGain = (test_B.In - test_B.Pos_Real) * test_P.I;

  /* Signum: '<S13>/SignPreIntegrator' */
  if (rtb_IntegralGain < 0.0) {
    /* DataTypeConversion: '<S13>/DataTypeConv2' */
    rtb_ZeroGain = -1.0;
  } else if (rtb_IntegralGain > 0.0) {
    /* DataTypeConversion: '<S13>/DataTypeConv2' */
    rtb_ZeroGain = 1.0;
  } else if (rtb_IntegralGain == 0.0) {
    /* DataTypeConversion: '<S13>/DataTypeConv2' */
    rtb_ZeroGain = 0.0;
  } else {
    /* DataTypeConversion: '<S13>/DataTypeConv2' */
    rtb_ZeroGain = (rtNaN);
  }

  /* End of Signum: '<S13>/SignPreIntegrator' */

  /* DataTypeConversion: '<S13>/DataTypeConv2' */
  if (rtIsNaN(rtb_ZeroGain)) {
    rtb_ZeroGain = 0.0;
  } else {
    rtb_ZeroGain = fmod(rtb_ZeroGain, 256.0);
  }

  /* Switch: '<S2>/Switch' incorporates:
   *  Constant: '<S2>/Constant'
   *  DataTypeConversion: '<S13>/DataTypeConv1'
   *  DataTypeConversion: '<S13>/DataTypeConv2'
   *  Logic: '<S13>/AND'
   *  RelationalOperator: '<S13>/Equal'
   */
  if (rtb_NotEqual && ((rtb_ZeroGain < 0.0 ? (int16_T)(int8_T)-(int8_T)(uint8_T)
                        -rtb_ZeroGain : (int16_T)(int8_T)(uint8_T)rtb_ZeroGain) ==
                       rtb_DataTypeConv1_0)) {
    rtb_IntegralGain = test_P.Constant_Value_k;
  }

  /* End of Switch: '<S2>/Switch' */

  /* Update for DiscreteIntegrator: '<S2>/Integrator' */
  test_DW.Integrator_DSTATE += test_P.Integrator_gainval * rtb_IntegralGain;

  /* External mode */
  rtExtModeUploadCheckTrigger(2);
  rtExtModeUpload(0, test_M->Timing.taskTime0);

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.01s, 0.0s] */
    if ((rtmGetTFinal(test_M)!=-1) &&
        !((rtmGetTFinal(test_M)-test_M->Timing.taskTime0) >
          test_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(test_M, "Simulation finished");
    }

    if (rtmGetStopRequested(test_M)) {
      rtmSetErrorStatus(test_M, "Simulation finished");
    }
  }

  /* Update absolute time */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  test_M->Timing.taskTime0 =
    (++test_M->Timing.clockTick0) * test_M->Timing.stepSize0;
}

/* Model step function for TID1 */
void test_step1(void)                  /* Sample time: [5.0s, 0.0s] */
{
  real_T rtb_Output;
  uint8_T rtb_FixPtSum1;

  /* Sum: '<S33>/FixPt Sum1' incorporates:
   *  Constant: '<S33>/FixPt Constant'
   *  UnitDelay: '<S32>/Output'
   */
  rtb_FixPtSum1 = (uint8_T)((uint16_T)test_DW.Output_DSTATE +
    test_P.FixPtConstant_Value);

  /* MultiPortSwitch: '<S11>/Output' incorporates:
   *  Constant: '<S11>/Vector'
   *  UnitDelay: '<S32>/Output'
   */
  rtb_Output = test_P.Teste3_OutValues[test_DW.Output_DSTATE];

  /* Update for RateTransition: '<Root>/Rate Transition' */
  test_DW.RateTransition_Buffer0 = rtb_Output;

  /* Switch: '<S34>/FixPt Switch' */
  if (rtb_FixPtSum1 > test_P.LimitedCounter_uplimit) {
    /* Update for UnitDelay: '<S32>/Output' incorporates:
     *  Constant: '<S34>/Constant'
     */
    test_DW.Output_DSTATE = test_P.Constant_Value_b;
  } else {
    /* Update for UnitDelay: '<S32>/Output' */
    test_DW.Output_DSTATE = rtb_FixPtSum1;
  }

  /* End of Switch: '<S34>/FixPt Switch' */
  rtExtModeUpload(1, ((test_M->Timing.clockTick1) * 5.0));

  /* Update absolute time */
  /* The "clockTick1" counts the number of times the code of this task has
   * been executed. The resolution of this integer timer is 5.0, which is the step size
   * of the task. Size of "clockTick1" ensures timer will not overflow during the
   * application lifespan selected.
   */
  test_M->Timing.clockTick1++;
}

/* Model initialize function */
void test_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)test_M, 0,
                sizeof(RT_MODEL_test_T));
  rtmSetTFinal(test_M, 36.0);
  test_M->Timing.stepSize0 = 0.01;

  /* External mode info */
  test_M->Sizes.checksums[0] = (4132955378U);
  test_M->Sizes.checksums[1] = (3494056472U);
  test_M->Sizes.checksums[2] = (1338565159U);
  test_M->Sizes.checksums[3] = (1204804192U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[5];
    test_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(test_M->extModeInfo,
      &test_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(test_M->extModeInfo, test_M->Sizes.checksums);
    rteiSetTPtr(test_M->extModeInfo, rtmGetTPtr(test_M));
  }

  /* block I/O */
  (void) memset(((void *) &test_B), 0,
                sizeof(B_test_T));

  /* states (dwork) */
  (void) memset((void *)&test_DW, 0,
                sizeof(DW_test_T));

  /* external outputs */
  test_Y.Out1 = 0.0;

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    test_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Initialize DataMapInfo substructure containing ModelMap for C API */
  test_InitializeDataMapInfo();

  /* Start for RateTransition: '<Root>/Rate Transition' */
  test_B.In = test_P.RateTransition_InitialCondition;

  /* Start for MATLABSystem: '<Root>/Sensor_Pos' */
  test_DW.obj.isInitialized = 0L;
  test_DW.obj.isInitialized = 1L;

  /* Start for S-Function (arduinoanalogoutput_sfcn): '<S6>/PWM' */
  MW_pinModeOutput(test_P.PWM_pinNumber);

  /* InitializeConditions for RateTransition: '<Root>/Rate Transition' */
  test_DW.RateTransition_Buffer0 = test_P.RateTransition_InitialCondition;

  /* InitializeConditions for DiscreteIntegrator: '<S2>/Integrator' */
  test_DW.Integrator_DSTATE = test_P.Integrator_IC;

  /* InitializeConditions for UnitDelay: '<S32>/Output' */
  test_DW.Output_DSTATE = test_P.Output_InitialCondition;
}

/* Model terminate function */
void test_terminate(void)
{
  /* Start for MATLABSystem: '<Root>/Sensor_Pos' */
  if (test_DW.obj.isInitialized == 1L) {
    test_DW.obj.isInitialized = 2L;
  }

  /* End of Start for MATLABSystem: '<Root>/Sensor_Pos' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
