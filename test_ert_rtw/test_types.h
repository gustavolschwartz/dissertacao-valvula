/*
 * File: test_types.h
 *
 * Code generated for Simulink model 'test'.
 *
 * Model version                  : 1.37
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 09:59:02 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_test_types_h_
#define RTW_HEADER_test_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#ifndef struct_tag_sMXuVOqiR8VrXITXlaWuyyE
#define struct_tag_sMXuVOqiR8VrXITXlaWuyyE

struct tag_sMXuVOqiR8VrXITXlaWuyyE
{
  real_T uint8;
  real_T uint16;
};

#endif                                 /*struct_tag_sMXuVOqiR8VrXITXlaWuyyE*/

#ifndef typedef_sMXuVOqiR8VrXITXlaWuyyE_test_T
#define typedef_sMXuVOqiR8VrXITXlaWuyyE_test_T

typedef struct tag_sMXuVOqiR8VrXITXlaWuyyE sMXuVOqiR8VrXITXlaWuyyE_test_T;

#endif                                 /*typedef_sMXuVOqiR8VrXITXlaWuyyE_test_T*/

#ifndef typedef_codertarget_arduinobase_inter_T
#define typedef_codertarget_arduinobase_inter_T

typedef struct {
  int32_T isInitialized;
} codertarget_arduinobase_inter_T;

#endif                                 /*typedef_codertarget_arduinobase_inter_T*/

/* Parameters (auto storage) */
typedef struct P_test_T_ P_test_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_test_T RT_MODEL_test_T;

#endif                                 /* RTW_HEADER_test_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
