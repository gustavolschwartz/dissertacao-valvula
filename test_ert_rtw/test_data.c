/*
 * File: test_data.c
 *
 * Code generated for Simulink model 'test'.
 *
 * Model version                  : 1.37
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Mon Sep 14 09:59:02 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "test.h"
#include "test_private.h"

/* Block parameters (auto storage) */
P_test_T test_P = {
  /* Variable: B
   * Referenced by: '<S2>/Setpoint Weighting (Proportional)'
   */
  1.0737814937419827,

  /* Variable: I
   * Referenced by: '<S2>/Integral Gain'
   */
  45.378498008043039,

  /* Variable: P
   * Referenced by: '<S2>/Proportional Gain'
   */
  5.3512666807357965,

  /* Mask Parameter: DiscreteVarying2DOFPID2_LowerSa
   * Referenced by:
   *   '<S2>/Saturate'
   *   '<S13>/DeadZone'
   */
  0.0,

  /* Mask Parameter: Teste3_OutValues
   * Referenced by: '<S11>/Vector'
   */
  { 2.0, 33.0, 97.0, 89.0, 5.0, 7.0, 50.0 },

  /* Mask Parameter: DiscreteVarying2DOFPID2_UpperSa
   * Referenced by:
   *   '<S2>/Saturate'
   *   '<S13>/DeadZone'
   */
  255.0,

  /* Mask Parameter: CompareToConstant1_const
   * Referenced by: '<S15>/Constant'
   */
  98.0,

  /* Mask Parameter: CompareToConstant2_const
   * Referenced by: '<S16>/Constant'
   */
  98.0,

  /* Mask Parameter: CompareToConstant_const
   * Referenced by: '<S14>/Constant'
   */
  1.0,

  /* Mask Parameter: PWM_pinNumber
   * Referenced by: '<S6>/PWM'
   */
  3U,

  /* Mask Parameter: LimitedCounter_uplimit
   * Referenced by: '<S34>/FixPt Switch'
   */
  6U,

  /* Expression: 120
   * Referenced by: '<S3>/Constant'
   */
  120.0,

  /* Expression: 0
   * Referenced by: '<Root>/Rate Transition'
   */
  0.0,

  /* Computed Parameter: Integrator_gainval
   * Referenced by: '<S2>/Integrator'
   */
  0.01,

  /* Expression: InitialConditionForIntegrator
   * Referenced by: '<S2>/Integrator'
   */
  0.0,

  /* Expression: 0
   * Referenced by: '<S13>/ZeroGain'
   */
  0.0,

  /* Expression: 0
   * Referenced by: '<S2>/Constant'
   */
  0.0,

  /* Computed Parameter: Bias1_Bias
   * Referenced by: '<Root>/Bias1'
   */
  -1610,

  /* Computed Parameter: Gain3_Gain
   * Referenced by: '<Root>/Gain3'
   */
  16487,

  /* Computed Parameter: Constant_Value_b
   * Referenced by: '<S34>/Constant'
   */
  0U,

  /* Computed Parameter: FixPtConstant_Value
   * Referenced by: '<S33>/FixPt Constant'
   */
  1U,

  /* Computed Parameter: Output_InitialCondition
   * Referenced by: '<S32>/Output'
   */
  0U
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
