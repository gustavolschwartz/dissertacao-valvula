/*
 * test_dt.h
 *
 * Code generation for model "test".
 *
 * Model version              : 1.37
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Mon Sep 14 09:59:02 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(int16_T),
  sizeof(codertarget_arduinobase_inter_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "int16_T",
  "codertarget_arduinobase_inter_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&test_B.In), 0, 0, 3 }
  ,

  { (char_T *)(&test_DW.Integrator_DSTATE), 0, 0, 2 },

  { (char_T *)(&test_DW.Scope1_PWORK.LoggedData[0]), 11, 0, 5 },

  { (char_T *)(&test_DW.obj), 15, 0, 1 },

  { (char_T *)(&test_DW.Output_DSTATE), 3, 0, 1 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  5U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&test_P.B), 0, 0, 15 },

  { (char_T *)(&test_P.PWM_pinNumber), 7, 0, 1 },

  { (char_T *)(&test_P.LimitedCounter_uplimit), 3, 0, 1 },

  { (char_T *)(&test_P.Constant_Value), 0, 0, 6 },

  { (char_T *)(&test_P.Bias1_Bias), 4, 0, 2 },

  { (char_T *)(&test_P.Constant_Value_b), 3, 0, 3 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  6U,
  rtPTransitions
};

/* [EOF] test_dt.h */
