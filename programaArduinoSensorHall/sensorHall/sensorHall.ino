#include <Wire.h>
#include <AS5600.h>
#define AS5600Address 0x36
//#define AS5600Address 0b0110110
AS5600 encoder;
double output;
//double PWMOut =0;
int analogOut =0;
byte RAW_ANGLE_LSB =0;
byte RAW_ANGLE_HSB =0;

void setup() 
{
 pinMode(A4,OUTPUT/INPUT);
 pinMode(A5,OUTPUT/INPUT);
 Serial.begin(9600);
 Wire.begin();
 //deviceCalibration(); 
 //deviceConf();
}

void loop() 
{
  //deviceConf(); // funcao de configuracao do sensor
  //PWMOut = pulseIn(2,HIGH); //escreve valor de pwm
  //analogOut = analogRead(A0); // escreve valor analogico
  //Serial.println("oi");
  //deviceConf();
  output = encoder.getPosition(); // get the absolute position of the encoder
  Serial.println(output);
  /*
  Wire.beginTransmission(AS5600Address); // escolhe device
  Wire.write(0x0C); //Escolhe o RAW ANGLE register com HSB
  //Serial.println("agarrou1");
  Wire.endTransmission();
  Serial.println("agarrou2");
  delay(10);
  
  Wire.requestFrom(AS5600Address, 1);

  if(Wire.available() <=1) 
  {
    RAW_ANGLE_HSB = Wire.read();
  }
  Serial.write("RAW_ANGLE_HSB:");
  Serial.println(RAW_ANGLE_HSB);
  */
     
  
  /*if((analogOut > 100)and (PWMOut < 100))
  {
    Serial.println(analogOut);
      
  }
  else

  if((analogOut < 100) and (PWMOut > 100))
  {
    Serial.println(PWMOut);
  }*/

}


void deviceConf()
{
  Wire.beginTransmission(AS5600Address);
  Wire.write(0x08);
  //Wire.write(0b10100000); // CONFIGURACAO DE SAIDA PARA PWM
  //Wire.write(0b10010000); //CONFIGURACAO DE SAIDA PARA ANALOGICO 10- 90%
  Wire.write(0b10000000); //CONFIGURACAO DE SAIDA PARA ANALOGICO 0- 100%
  Wire.write(0b00000000);
  Wire.endTransmission();
}



void deviceCalibration()
{
  //Calibracao posicao inicial
  Wire.beginTransmission(AS5600Address); // escolhe device
  Wire.write(0x0C); //Escolhe o RAW ANGLE register com HSB
  Wire.requestFrom(AS5600Address,1);// requisita 1 byte do device
  RAW_ANGLE_HSB=Wire.read(); // lê byte mais significativo do RAW ANGLE register
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Address);
  Wire.write(0x01); //Escolhe o ZPOS register com HSB
  Wire.write(RAW_ANGLE_HSB);// Escreve valor no registrador
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Address); // escolhe device
  Wire.write(0x0D); //Escolhe o RAW ANGLE register com LSB
  Wire.requestFrom(AS5600Address,1);// requisita 1 byte do device
  RAW_ANGLE_LSB=Wire.read(); // lê byte menos significativo do RAW ANGLE register
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Address);
  Wire.write(0x02); //Escolhe o ZPOS register com LSB
  Wire.write(RAW_ANGLE_LSB);// Escreve valor no registrador
  Wire.endTransmission();
  Serial.write("Escreveu o valor minimo, 10s...");
  delay(10000);

  
  //Calibracao posicao maxima

  Wire.beginTransmission(AS5600Address); // escolhe device
  Wire.write(0x0C); //Escolhe o RAW ANGLE register com HSB
  Wire.requestFrom(AS5600Address,1);// requisita 1 byte do device
  RAW_ANGLE_HSB=Wire.read(); // lê byte mais significativo do RAW ANGLE register
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Address);
  Wire.write(0x03); //Escolhe o MPOS register com HSB
  Wire.write(RAW_ANGLE_HSB);// Escreve valor no registrador
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Address); // escolhe device
  Wire.write(0x0D); //Escolhe o RAW ANGLE register com LSB
  Wire.requestFrom(AS5600Address,1);// requisita 1 byte do device
  RAW_ANGLE_LSB=Wire.read(); // lê byte menos significativo do RAW ANGLE register
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Address);
  Wire.write(0x04); //Escolhe o MPOS register com LSB
  Wire.write(RAW_ANGLE_LSB);// Escreve valor no registrador
  Wire.endTransmission();
  delay(10);
  Serial.write("Escreveu valor maximo");
  delay(5000);
    
  
}
