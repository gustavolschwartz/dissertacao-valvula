#include <Wire.h>
#include <AS5600.h>


AS5600 encoder;
double output;
#define AS5600Address 0x36
byte RAW_ANGLE_LSB =0;
byte RAW_ANGLE_HSB =0;
//int x =0;

void setup() 
{
 pinMode(A4,OUTPUT);
 pinMode(A5,OUTPUT);
 Wire.begin();
 Serial.begin(9600);
 //deviceCalibration(); 

}

void loop() 
{
  //deviceConf(); // funcao de configuracao do sensor
  //PWMOut = pulseIn(2,HIGH); //escreve valor de pwm
  //analogOut = analogRead(A0); // escreve valor analogico
   output = encoder.getPosition(); // get the absolute position of the encoder
   Serial.write("output:");
   Serial.println(output,BIN);
   //deviceCalibration(); 

   // get HSB
  Wire.beginTransmission(AS5600Address); // escolhe device
  Wire.write(0x0C); //Escolhe o RAW ANGLE register com HSB
  Wire.endTransmission();
  delay(10);

  Wire.requestFrom(AS5600Address, 1);

  if(Wire.available() <=1) 
  {
    RAW_ANGLE_HSB = Wire.read();
  }
  Serial.write("RAW_ANGLE_HSB:");
  Serial.println(RAW_ANGLE_HSB);

  // get LSB
  
  Wire.requestFrom(AS5600Address, 1);

  Wire.beginTransmission(AS5600Address);
  Wire.write(0x0D);
  Wire.endTransmission();

  if(Wire.available() <=1) {
    RAW_ANGLE_LSB = Wire.read();
  }
  Serial.write("RAW_ANGLE_LSB:");
  Serial.println(RAW_ANGLE_LSB);
  int x = (RAW_ANGLE_LSB) + (RAW_ANGLE_HSB) * 256;
   Serial.write("x:");
  Serial.println(x);

}
/*
void deviceCalibration()
{
  //Calibracao posicao inicial
  Wire.beginTransmission(AS5600Adress); // escolhe device
  Wire.write(0x0C); //Escolhe o RAW ANGLE register com HSB
  Wire.requestFrom(AS5600Adress,1);// requisita 1 byte do device
  RAW_ANGLE_HSB=Wire.read(); // lê byte mais significativo do RAW ANGLE register
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Adress);
  Wire.write(0x01); //Escolhe o ZPOS register com HSB
  Wire.write(RAW_ANGLE_HSB);// Escreve valor no registrador
  Wire.endTransmission();
  delay(5);
  

  Wire.beginTransmission(AS5600Adress); // escolhe device
  Wire.write(0x0D); //Escolhe o RAW ANGLE register com LSB
  Wire.requestFrom(AS5600Adress,1);// requisita 1 byte do device
  RAW_ANGLE_LSB=Wire.read(); // lê byte menos significativo do RAW ANGLE register
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Adress);
  Wire.write(0x02); //Escolhe o ZPOS register com LSB
  Wire.write(RAW_ANGLE_LSB);// Escreve valor no registrador
  Wire.endTransmission();
  Serial.write("Escreveu o valor minimo, 10s...");
  delay(10000);

  
  //Calibracao posicao maxima

  Wire.beginTransmission(AS5600Adress); // escolhe device
  Wire.write(0x0C); //Escolhe o RAW ANGLE register com HSB
  Wire.requestFrom(AS5600Adress,1);// requisita 1 byte do device
  RAW_ANGLE_HSB=Wire.read(); // lê byte mais significativo do RAW ANGLE register
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Adress);
  Wire.write(0x03); //Escolhe o MPOS register com HSB
  Wire.write(RAW_ANGLE_HSB);// Escreve valor no registrador
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Adress); // escolhe device
  Wire.write(0x0D); //Escolhe o RAW ANGLE register com LSB
  Wire.requestFrom(AS5600Adress,1);// requisita 1 byte do device
  RAW_ANGLE_LSB=Wire.read(); // lê byte menos significativo do RAW ANGLE register
  Wire.endTransmission();
  delay(5);

  Wire.beginTransmission(AS5600Address);
  Wire.write(0x04); //Escolhe o MPOS register com LSB
  Wire.write(RAW_ANGLE_LSB);// Escreve valor no registrador
  Wire.endTransmission();
  delay(10);
  Serial.write("Escreveu valor maximo");
  delay(5000);
  
}*/
