    Repositório do projeto de dissertação de mestrado de Gustavo Lemos Schwartz para o Mestrado profissional em Sistemas Aplicados a Engenharia e Gestão (SAEG) do Instituto Federal Fluminense.

Antes de abrir os diagramas de blocos do Simulink, carregue o arquivo "dados_unificados.mat" para o Workspace do Matlab.

O algoritmo de produção do projeto a ser embarcado no Aduino é o arquivo "Valvula_arduino.slx".

Outros arquivos inclusos no repositório foram incluídos para testes e levantamento de curvas, por exemplo.

Os arquivos .m foram utilizados para otimização, como o arquivo "Otimizacao_simulink_PI.m" que foi utilizado para otimizar o controlador PI do modelo regressivo do diagrama de blocos "test.slx".

Mais detalhes sobre o andamento e desenvolvimento do projeto podem ser observados no histórico de commits deste repositório.
